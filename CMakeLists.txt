################################################################################
# Build configuration for NtupleToHist_MonoSWW
################################################################################

# Declare the name of the package:
atlas_subdir( MonoSWWTruthFramework_NtupleToHist )

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/xAODRootAccess
   TruthFramework_NtupleToHist
   Tools/PathResolver
   #Control/AthenaBaseComps
   #Control/AthAnalysisBaseComps	
   #Control/AthenaKernel
   #GaudiKernel
   Event/xAOD/xAODCore	
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODBase
   Event/xAOD/xAODTruth
   Event/xAOD/xAODJet
   Event/xAOD/xAODMissingET
   Event/xAOD/xAODBTagging
   #Event/xAOD/xAODBTaggingEfficiency
   Reconstruction/Jet/JetInterface
   PhysicsAnalysis/Interfaces/JetAnalysisInterfaces
   PhysicsAnalysis/DerivationFramework/DerivationFrameworkInterfaces	
   Reconstruction/MET/METInterface
   Reconstruction/MET/METUtilitiesLib
   )

find_package( ROOT COMPONENTS Core Tree MathCore Hist Physics TMVA XMLParser XMLIO)
find_package( FastJet )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )


atlas_install_generic( data/*
			       DESTINATION data PKGNAME_SUBDIR )

# Build a dictionary for the library:
atlas_add_root_dictionary( MonoSWWTruthFramework_NtupleToHist _dictionarySource
   ROOT_HEADERS Root/LinkDef.h
   EXTERNAL_PACKAGES ROOT )

# Build a shared library:
atlas_add_library( MonoSWWTruthFramework_NtupleToHist
   MonoSWWTruthFramework_NtupleToHist/*.h Root/*.h Root/*.cxx ${_dictionarySource}
   PUBLIC_HEADERS MonoSWWTruthFramework_NtupleToHist
   LINK_LIBRARIES ${ROOT_LIBRARIES} EventLoop xAODRootAccess PathResolver TruthFramework_NtupleToHist xAODCore xAODEventInfo xAODBase xAODTruth xAODJet xAODMissingET xAODBTagging JetInterface JetAnalysisInterfacesLib METInterface METUtilitiesLib)

# Build the executable(s) of the package.
atlas_add_executable( NtupleToHist_MonoSWW  util/makeHistograms_MonoSWW.cxx
                      INCLUDE_DIRS ${Boost_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} MonoSWWTruthFramework_NtupleToHist )
