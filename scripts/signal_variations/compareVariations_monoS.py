#!/usr/bin/env python
from commonUtils import *
import ROOT
from ROOT import *
import os
import sys
from reweight_hists import *
import yaml
from collections import defaultdict

# Example usage:
# ./compareVariations_monoS.py -a monoSWW -d hadded_hists -n Zjets
#
# To do fits for calculating a pTV-based reweighting function for Z+jets:
# ./compareVariations_monoS.py -a monoSWW -d hadded_hists -n Zjets -w


def getArgs():
  
  # Class to modify the argument parser behaviour such that it prints out the full help message if insufficient or problematic arguments are supplied
  class MyParser(ArgumentParser):
    def error(self, message):
      sys.stderr.write('error: %s\n' % message)
      self.print_help()
      sys.exit(2)
  
  parser = MyParser()
  parser.add_argument('-d',
                      '--hists_dir',
                      required=True,
                      help='Name of the directory containing the histogram root files')
  parser.add_argument('-n',
                      '--sample_name',
                      required=True,
                      help='Name of the sample to consider. Can be either a dsid or the name of a combined sample (eg. 363643 or Zjets)')
  parser.add_argument('-a',
                      '--analysis',
                      default='monoSWW',
                      help='Analysis to consider: options are monoSWW, monoHbb, and monoSbb. Defaults to monoSWW (the code is currently only guaranteed to support the monoSWW option)')
  parser.add_argument('-m',
                      '--MetBin',
                      default='3',
                      help='Specify MET bin: 0: 200-300, 1: 300-500, 2: 500-inf, 3: inclusive')
  parser.add_argument('-v',
                      '--variable',
                      default='mS_candidate',
                      help='Variable to be plotted')
  parser.add_argument('-t',
                      '--variableTitle',
                      default='Dark Higgs Candidate Mass [GeV]',
                      help='Title for variable to be plotted')
  parser.add_argument('-l',
                      '--logY',
                      help='Make plots with logarithmic y-axis',
                      action='store_true')
  parser.add_argument('-w',
                      '--reweight',
                      help='Calculate and save reweighted MGPy for inclusive MET distribution (overrides `MetBin`, `variable`, and `variableTitle` arguments)',
                      action='store_true')
  parser.add_argument('-r',
                      '--use_reweighted_mgpy',
                      help='Use the reweighted mgpy',
                      action='store_true')
  parser.add_argument('-c',
                      '--do_recast',
                      help='Run this script as part of the recast workflow',
                      action='store_true')
  return parser.parse_args()

def dict_maker():
  return defaultdict(dict_maker)

# Function to get all the arguments from the output of getArgs() and make sure they're all acceptable
def check_args(args):
  analysis = args.analysis
  hists_dir = args.hists_dir
  sample_name = args.sample_name
  logY = args.logY
  reweight = args.reweight
  use_reweighted_mgpy = args.use_reweighted_mgpy
  do_recast = args.do_recast

  MetBin = args.MetBin
  if MetBin == '0': ptv_str = '200_300ptv'
  elif MetBin == '1': ptv_str = '300_500ptv'
  elif MetBin == '2': ptv_str = '500ptv'
  elif MetBin == '3': ptv_str = 'allptv'
  else:
    print("ERROR: MetBin argument must be one of: 0, 1, 2, 3. Exiting.")
    exit()
  
  # Automatically use allptv for the met bin if the 'reweight' option was specified
  if reweight:
    print("WARNING: Reweight option specified, so automatically using inclusive 'allptv' pTV region.")
    ptv_str = 'allptv'

    if(use_reweighted_mgpy):
      print("ERROR: Cannot specify '--reweight' and '--use_reweighted_mgpy' simultaneously. Exiting.")
      exit()

  variable = args.variable
  variableTitle = args.variableTitle

  return analysis, hists_dir, sample_name, ptv_str, variable, variableTitle, logY, reweight, use_reweighted_mgpy, do_recast


# Get the variations for either signal or background
def set_variations(analysis, type):
  variations = []
  variation_names = []
  
  if type == "signal":
    if analysis == "monoSWW" or analysis == "monoHbb" or analysis == "MonoSbb":   # Note: may be different for monoHbb, not sure
      variations = ["MUF=0.5","MUF=2.0","MUR=0.5","MUR=0.5MUF=0.5","MUR=2.0","MUR=2.0MUF=2.0"]
      variation_names = ["#mu_{F}=0.5","#mu_{F}=2.0","#mu_{R}=0.5","#mu_{R}=0.5#mu_{F}=0.5","#mu_{R}=2.0","#mu_{R}=2.0#mu_{F}=2.0"]
    else:
      print("ERROR: analysis name %s not recognized! Returning empty lists for variations."%analysis)

  elif type == "background":
    if analysis == "monoSWW" or analysis == "monoHbb" or analysis == "MonoSbb":   # Note: may be different for monoHbb, not sure
      variations = ["MUR0.5_MUF0.5_PDF261000","MUR0.5_MUF1_PDF261000","MUR1_MUF0.5_PDF261000","MUR1_MUF2_PDF261000","MUR2_MUF1_PDF261000","MUR2_MUF2_PDF261000"]
      variation_names = ["#mu_{R}=0.5#mu_{F}=0.5", "#mu_{R}=0.5#mu_{F}=1", "#mu_{R}=1#mu_{F}=0.5", "#mu_{R}=1#mu_{F}=2", "#mu_{R}=2#mu_{F}=1", "#mu_{R}=2#mu_{F}=2"]

  else:
    print("ERROR: sample type must be either signal or background. Returning empty lists for variations.")

  return variations, variation_names

# Get the pdf variations for either signal or background
def set_pdf_variations(analysis, type, sample):
  pdf_variations = []
  pdf_variation_names = []
  
  if type == "signal":
    for i in range(1,101):
      if (analysis=="monoSbb" or analysis=="monoSWW"):
        pdf_variations.append("PDF=263000MemberID="+str(i))
        pdf_variation_names.append("PDF Var %d"%i)
      elif (analysis=="monoHbb"):
        pdf_variations.append("PDF=260000MemberID="+str(i))
        pdf_variation_names.append("PDF Var %d"%i)
      else:
        print("ERROR: analysis name %s not recognized! Returning empty lists for pdf variations."%analysis)

  elif type == "background":
    if analysis == "monoSWW" or analysis == "monoHbb" or analysis == "MonoSbb":   # Note: may be different for monoHbb, not sure
      if sample == "Wall":
        pdf_variations = ["MUR1_MUF1_PDF13000","MUR1_MUF1_PDF25300","MUR1_MUF1_PDF261098"]
        pdf_variation_names = ["PDF Var 13000", "PDF Var 25300", "PDF Var 261098"]
      
      if sample == "Zall":
        pdf_variations = ["MUR1_MUF1_PDF13000","MUR1_MUF1_PDF25300"]
        pdf_variation_names = ["PDF Var 13000", "PDF Var 25300"]
  else:
    print("ERROR: sample type must be either signal or background. Returning empty lists for pdf variations.")

  return pdf_variations, pdf_variation_names

# Function to collect all histogram variations from the input file
def get_hist_variations(infile, sample, ptv_str, variations, n_leptons, lepton_region_label, type, variable):
  dir = infile.Get("WeightVariations")
  hist_variations = []
  hist_variations_err = []
  
  ngroup, xbins = get_rebin_info(variable, sample)
  #if variable == "mS_candidate": ngroup = 4

  for var in variations:
    if isinstance(xbins, int): hist_root = dir.Get("hist_%dLep_%s_%s_%s_%s_%s"%(n_leptons, sample, ptv_str, lepton_region_label, variable, var)).Rebin(ngroup)
    else: hist_root = dir.Get("hist_%dLep_%s_%s_%s_%s_%s"%(n_leptons, sample, ptv_str, lepton_region_label, variable, var)).Rebin(ngroup, "", xbins)
    hist_numpy = hist2array(hist_root)       # Convert to numpy array
    hist_variations.append(hist_numpy)
  
    # Get the errors
    hist_err = np.zeros(len(hist_numpy))
    for i in range(hist_root.GetNbinsX()):
      hist_err[i] = hist_root.GetBinError(i+1)
    hist_variations_err.append(hist_err)

  return hist_variations, hist_variations_err


def get_max_rel_diff(hist_nominal, hist_variations):
  max_rel_diff = 0.    # Maximum relative difference for all variations

  # Loop through all variations
  for iVar in range(len(hist_variations)):
    hist = hist_variations[iVar]
    
    # Calculate the relative difference in scale between the given and nominal hists
    rel_diff_scale = 1. - np.sum(hist)/np.sum(hist_nominal)
    if (abs(rel_diff_scale)>max_rel_diff):
      max_rel_diff=abs(rel_diff_scale)

  return max_rel_diff

# Function to get histogram variations relative to nominal
def get_variations_ratio(hist_nominal, hist_variations, hist_nominal_err = None, hist_variations_err = None):
  hist_variations_ratio = []
  hist_variations_ratio_err = []

  # Loop through all variations
  for iVar in range(len(hist_variations)):
    hist = hist_variations[iVar]
    
    # Calculate the relative deviation from the nominal histogram
    hist_ratio = hist / hist_nominal - 1  #(var-nominal)/nominal = rel. difference
    hist_ratio[np.isnan(hist_ratio)] = 0.    # If any hist_nominal bins are zero in the above expression, the corresponding hist_ratio bin will be set to NaN. Reset these 'divide-by-zero' NaNs to 0.
    hist_ratio[np.isinf(hist_ratio)] = 0.
    hist_variations_ratio.append(hist_ratio)
  
    # If errors are input for the nominal and variation histos, propagate their error to the ratio
    if hist_nominal_err is not None and hist_variations_err is not None:
      hist_ratio_err = (hist_ratio+1) * np.sqrt( (hist_variations_err[iVar] / hist)**2 + (hist_nominal_err / hist_nominal)**2 )
      hist_ratio_err[np.isnan(hist_ratio_err)] = 0
      hist_ratio_err[np.isinf(hist_ratio_err)] = 0
      hist_variations_ratio_err.append(hist_ratio_err)

  if hist_nominal_err is not None and hist_variations_err is not None: return hist_variations_ratio, hist_variations_ratio_err
  else: return hist_variations_ratio


# Function to calculate the relevant pdf variation info standard deviation relative to nominal
def collect_pdf_error(hist_nominal, hist_pdf_variations):
  
  print "Evaluation error PDF using standard deviation"
  central_value = 0.
  error_pdf = 0.
  sum_integrals = 0.
  sum_integrals_sq = 0.
  n_variations = 1.0*len(hist_pdf_variations)
    
  for hist in hist_pdf_variations:
    sum_integrals += np.sum(hist)
    sum_integrals_sq += np.sum(hist)**2
  
  central_value = sum_integrals/n_variations   # just the mean
  mean_of_sq = sum_integrals_sq/n_variations
  
  print "the central value of the pdfs set: ", central_value
  print "should be similar to nominal value: ", np.sum(hist_nominal)
  
  error_pdf = sqrt((mean_of_sq-central_value**2)*n_variations/(n_variations-1))    #Using standard deviation approach:https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PdfRecommendations#Standard_deviation
  #Need to use always the central value: https://arxiv.org/pdf/1510.03865.pdf
  
  nom_value = np.sum(hist_nominal)
  round_error_pdf = round(error_pdf*100./nom_value,1)     #relative percentage error

  print("round error pdf: %f"%round_error_pdf)
  return round_error_pdf

# Function to write out the relevant scale variation info (maximum relative difference) to output files
def write_scale_info(max_rel_diff, index_category, outfile_scale, outfile_all):
  round_max_rel_diff = round(max_rel_diff*100,1)
  file_output = "2 "+str(index_category)+" "+str(round_max_rel_diff)+"\n"
  outfile_scale.write(file_output)
  outfile_all.write(file_output)

# Function to write out the relevant pdf variation info (relative difference from standard deviation) to output files
def write_pdf_info(round_error_pdf, index_category, outfile_pdf, outfile_all):
  file_output = "3 "+str(index_category)+" "+str(round_error_pdf)+"\n"
  outfile_pdf.write(file_output)
  outfile_all.write(file_output)

# Class for drawing variations
class DrawVariations:
  def __init__(self, category, hist_nominal, hist_nominal_err, bin_edges, sample_type, n_leptons, variable, variableTitle, logY):

    # Create the colours to be used for plotting
    self.colors = make_hist_colors()
    
    # Create the template root histogram
    self.n_bins = len(bin_edges)-1
    self.x_low = np.min(bin_edges)
    self.x_high = np.max(bin_edges)
    self.bin_edges = bin_edges
    
    # Collect variables from class instantiation arguments
    self.hist_nominal = hist_nominal
    self.hist_nominal_err = hist_nominal_err
    self.hist_nominal_norm = hist_nominal/np.sum(hist_nominal)
    self.hist_nominal_norm_err = hist_nominal_err/np.sum(hist_nominal)
    
    self.category = category
    self.sample_type = sample_type
    self.n_leptons = n_leptons
    self.variable = variable
    self.variableTitle = variableTitle
    self.logY = logY
    if self.logY: self.logY_str = "_logY"
    else: self.logY_str = ""
    
    # Convert the nominal array to a root TH1F histogram for plotting, and also assign the correct errors to each bin
    self.hist_nominal_root = ROOT.TH1F("nominal_hist", "nominal_hist", self.n_bins, self.bin_edges)
    _ = array2hist(hist_nominal, self.hist_nominal_root)   # Convert from numpy array to root hist
    
    for i in range(self.hist_nominal_root.GetNbinsX()):
      self.hist_nominal_root.SetBinError(i+1, hist_nominal_err[i])
    
    # Also convert the normalized nominal array to a root TH1F histogram
    self.hist_nominal_normed_root = ROOT.TH1F("nominal_hist_norm", "nominal_hist_norm", self.n_bins, self.bin_edges)
    _ = array2hist(hist_nominal/np.sum(hist_nominal), self.hist_nominal_normed_root)   # Convert from numpy array to root hist
    
    for i in range(self.hist_nominal_normed_root.GetNbinsX()):
      self.hist_nominal_normed_root.SetBinError(i+1, self.hist_nominal_norm_err[i])
    
    # Set some drawing info for the nominal hist
    self.hist_nominal_root.SetLineColor(kBlack)
    self.hist_nominal_root.GetYaxis().SetTitleFont(42)
    self.hist_nominal_root.GetYaxis().SetTitleSize(0.12)
    self.hist_nominal_root.GetYaxis().SetTitleOffset(1.72)
  
    self.hist_nominal_normed_root.SetLineColor(kBlack)
    self.hist_nominal_normed_root.GetYaxis().SetTitleFont(42)
    self.hist_nominal_normed_root.GetYaxis().SetTitleSize(0.12)
    self.hist_nominal_normed_root.GetYaxis().SetTitleOffset(1.72)

  # Function for plotting variations
  def plot_variations(self, variation_names, hist_variations_orig, hist_variations_orig_err, label_str, ptv_str, sample_name, variation_type, plotShape=False, nEvents=0, nEvents_mgpy=0, reweight=False, use_reweighted_mgpy = False, reco=False):

    # Normalize everything to unit integral if we're just plotting shape comparisons
    if plotShape:
      hist_nominal = self.hist_nominal_norm
      hist_nominal_root = self.hist_nominal_normed_root
      hist_nominal_err = self.hist_nominal_norm_err
    
      hist_variations = []
      hist_variations_err = []
      for iVar in range(len(hist_variations_orig)):
        hist_variations.append(hist_variations_orig[iVar] / np.sum(hist_variations_orig[iVar]))
        hist_variations_err.append(hist_variations_orig_err[iVar] / np.sum(hist_variations_orig[iVar]))
    
    else:
      hist_nominal = self.hist_nominal
      hist_nominal_err = self.hist_nominal_err
      hist_nominal_root = self.hist_nominal_root
      hist_variations = hist_variations_orig
      hist_variations_err = hist_variations_orig_err
  
    # Calculate the ratio of variations to nominal for each variation and bin
    hist_variations_ratio, hist_variations_ratio_err = get_variations_ratio(hist_nominal, hist_variations, hist_nominal_err, hist_variations_err)
    
    # Make it an actual ratio rather than a relative difference if doing reweighting
    if reweight:
      for iVar in range(len(hist_variations_ratio)):
        hist_variations_ratio[iVar] += 1
  
    n_variations = len(hist_variations)
    
    # Convert all variation hists from numpy arrays to root hists
    hist_variations_root = []
    hist_variations_ratio_root = []
    for iVar in range(len(hist_variations)):
      # Variations
      hist_variation_root = ROOT.TH1F("var_hist_%d"%iVar, "var_hist_%d"%iVar, self.n_bins, self.bin_edges)
      _ = array2hist(hist_variations[iVar], hist_variation_root)
      hist_variations_root.append(hist_variation_root)
      
      # Relative errors for variations
      hist_variation_ratio_root = ROOT.TH1F("var_ratio_hist_%d"%iVar, "var_ratio_hist_%d"%iVar, self.n_bins, self.bin_edges)
      _ = array2hist(hist_variations_ratio[iVar], hist_variation_ratio_root)
      hist_variations_ratio_root.append(hist_variation_ratio_root)
      
      for i in range(hist_variation_ratio_root.GetNbinsX()):
        hist_variation_ratio_root.SetBinError(i+1, hist_variations_ratio_err[iVar][i])
    
      if reweight:

        # Reweight the hists in all categories
        for category in ["Merged", "Intermediate", "Resolved", "Inclusive"]:
          reweight_hist(hist_variations_ratio_root[iVar], label_str.split("_")[0], category, self.n_leptons, self.variable)

    print(" ****** Plotting %s variations ******"%variation_type)
    
    # Set up the canvases and cd into the pad for the main plot
    c1, pad1, pad2 = setup_canvas_ratio("c_scale", self.category, self.logY)
    pad1.cd()
    
    pad1_lower, pad1_upper, pad2_ylim = get_ylim(hist_nominal, hist_variations, self.logY)
    
    hist_nominal_root.GetYaxis().SetRangeUser(pad1_lower, pad1_upper)
    hist_nominal_root.Draw("hist")

    if variation_type=="2point":
      if reco: legend = setup_legend(0.58, 0.75, 0.92, 0.9)
      else: legend = setup_legend(0.65, 0.75, 0.95, 0.9)
    else: legend = setup_legend(0.60, 0.60, 0.95, 0.9)
    if variation_type == "2point":
      if reco: legend.AddEntry(hist_nominal_root, "Sherpa (Reco)", "l")
      else: legend.AddEntry(hist_nominal_root, "Sherpa", "l")
    else: legend.AddEntry(hist_nominal_root, "nominal", "l")
    
    for iVar in range(n_variations):
      hist_variations_root[iVar].GetYaxis().SetRangeUser(pad1_lower, pad1_upper)
      hist = hist_variations_root[iVar]
      hist.SetLineColor(self.colors[iVar])
      hist.Draw("histsame")
      if self.sample_type == "signal" and iVar%22==0 and variation_type=="pdf":    # Reduce legend frequency for signal pdf variations because there are tons of different pdfs
        legend.AddEntry(hist, variation_names[iVar], "l")
      elif self.sample_type == "background" or variation_type != "pdf":
        if reco: legend.AddEntry(hist, variation_names[iVar] + " (Reco)", "l")
        else: legend.AddEntry(hist, variation_names[iVar], "l")

    # Add some info text to the plot
    if variation_type == "2point": t = make_plot_text(self.category, self.n_leptons, sample_name, ptv_str, 0.045, 0.58, 0.5, 0.9, 0.6)
    else: t = make_plot_text(self.category, self.n_leptons, sample_name, ptv_str, 0.045, 0.58, 0.48, 0.9, 0.58)
    t.Draw()
    
    if variation_type == "2point" and not plotShape:
      # Add some info text to the plot about the sherpa and mgpy8 acceptances
      t_acc = ROOT.TPaveText(0.52, 0.35, 0.9, 0.45, "NDC,NB")   # NDC: use normalized coords rather than basing them on the x and y values on the axes
      accRatio = np.sum(hist_variations[0])/np.sum(hist_nominal)
      accRatio_statUnc = accRatio*np.sqrt(1./nEvents + 1./nEvents_mgpy)
      
      t_acc.AddText("MGPy/Sh Acc.: %.3f+/-%.3f"%(accRatio, accRatio_statUnc))
      t_acc.SetTextSize(0.045)
      t_acc.SetFillStyle(3000)      # Transparent fill
      t_acc.Draw()
    
    if plotShape:
      t_shape = ROOT.TPaveText(0.58, 0.31, 0.9, 0.41, "NDC,NB")   # NDC: use normalized coords rather than basing them on the x and y values on the axes
      t_shape.AddText("Normed to Unit Sum")
      t_shape.SetTextSize(0.045)
      t_shape.SetFillStyle(3000)      # Transparent fill
      t_shape.Draw()
    
    if use_reweighted_mgpy and self.category != "Intermediate" and self.category != "Resolved":
      t_reweighted = ROOT.TPaveText(0.58, 0.25, 0.9, 0.35, "NDC,NB")   # NDC: use normalized coords rather than basing them on the x and y values on the axes
      t_reweighted.AddText("MET-Reweighted")
      t_reweighted.SetTextSize(0.045)
      t_reweighted.SetFillStyle(3000)      # Transparent fill
      t_reweighted.Draw()

    pad2.cd()

    for iVar in range(n_variations):
      #if plotShape: hist_variations_ratio_root[iVar] = setup_axes(hist_variations_ratio_root[iVar], np.max(np.absolute(hist_variations_ratio))*1.2)
      
      # Dictionary of y-limits for the ratio plot, depending on the region, variable, and sample
      ylim_dict = {
        "0Lep.Zjets.MET": 1,
        "0Lep.Zjets.mS_candidate": 0.5,
        "1Lep.Zjets.MET_muInv": 1.5,
        "1Lep.Zjets.mS_candidate": 1.5,
        "2Lep.Zjets.pTV": 1.5,
        "2Lep.Zjets.mS_candidate": 0.5,
        "0Lep.Wjets.MET": 1,
        "0Lep.Wjets.mS_candidate": 1,
        "1Lep.Wjets.MET_muInv": 1.5,
        "1Lep.Wjets.mS_candidate": 1,
        "2Lep.Wjets.pTV": 1,
        "2Lep.Wjets.mS_candidate": 1.5
      }
      
      ylim_default = 1
      try: ylim = ylim_dict["%dLep.%s.%s"%(self.n_leptons, sample_name, self.variable)]
      except KeyError: ylim = ylim_default
      if self.sample_type == "signal": ylim = 0.25
      
      hist_variations_ratio_root[iVar] = setup_axes(hist_variations_ratio_root[iVar], ylim, reweight)
      hist_variations_ratio_root[iVar].GetXaxis().SetTitle(self.variableTitle)
      if reweight: hist_variations_ratio_root[iVar].GetYaxis().SetTitle("Ratio")
      else: hist_variations_ratio_root[iVar].GetYaxis().SetTitle("Rel. Diff.")
      hist_variations_ratio_root[iVar].SetLineColor(self.colors[iVar])
      hist_variations_ratio_root[iVar].SetMarkerSize(1);
      if variation_type == "2point": hist_variations_ratio_root[iVar].Draw("histsameE")
      else: hist_variations_ratio_root[iVar].Draw("histsame")

    # Draw the legend in the main plot
    pad1.cd()
    legend.Draw()
    
    c1.Update()
    
    if plotShape: shape_str = "_shape"
    else: shape_str = ""
    if use_reweighted_mgpy: mgpy_str = ""
    else: mgpy_str = "_no_reweighting"
    c1.SaveAs("./all_plots/%s_variations_%s_%s_%dLep_%s%s%s%s.pdf"%(variation_type, label_str, self.category, self.n_leptons, self.variable, shape_str, self.logY_str, mgpy_str))


# Function to determine plot ranges
def get_ylim(hist_nominal, hist_variations, logY):
  
  n_variations = len(hist_variations)
  hist_max = np.max(hist_nominal)
  hist_min = np.min(hist_nominal[hist_nominal>0])
  ratio_max = 0
  for iVar in range(n_variations):
    hist_max_var = np.max(hist_variations[iVar])
    hist_min_var = np.min(hist_variations[iVar][hist_variations[iVar]>0])
    if hist_max_var > hist_max: hist_max = hist_max_var
    
    rel_diff_abs = np.absolute(1-hist_variations[iVar]/hist_nominal)
    rel_diff_abs[np.isnan(rel_diff_abs)|np.isinf(rel_diff_abs)] = 0
    ratio_max_var = np.max(rel_diff_abs)
    if ratio_max_var > ratio_max: ratio_max = ratio_max_var
  
  if logY:
    pad1_upper = 10*hist_max
    pad1_lower = max(0.1*hist_min, 1e-5)
  else:
    pad1_upper = 1.2*hist_max
    pad1_lower = 0
  pad2_ylim = 1.2*ratio_max

  return pad1_lower, pad1_upper, pad2_ylim


# Function to apply a fix for the fact that the signal variations are buggy in that:
# for scale variations, the branch labelled MUF=MUR=0.5 variation is actually nominal, and the branch labelled 'Nominal' is actually MUF=MUR=0.5
def fix_signal_variation_bug(hist_nominal, hist_variations):
  hist_nominal_orig = np.copy(hist_nominal)
  hist_nominal = np.copy(hist_variations[3])
  hist_variations[3] = hist_nominal_orig
  return hist_nominal, hist_variations


# Function to specify the details of histogram rebinning, if needed
def get_rebin_info(variable, sample):
  ngroup = 1
  xbins=0
  if variable == "MET":
    if sample.split('_')[0][0]=="Z":
      xbins_low = np.linspace(0, 1080, 28)
      xbins_mid = np.linspace(1120, 1360, 3)
      xbins_high = np.linspace(1480, 2000, 3)
      xbins = (np.concatenate((xbins_low, xbins_mid, xbins_high)))
      ngroup = len(xbins)-1

    if sample.split('_')[0][0]=="W":
      xbins_low = np.linspace(0, 640, 17)
      xbins_mid = np.linspace(680, 1160, 5)
      xbins_high = np.linspace(1280, 2000, 2)
      xbins = (np.concatenate((xbins_low, xbins_mid, xbins_high)))
      ngroup = len(xbins)-1

  if variable == "MET_muInv":
    if sample.split('_')[0][0]=="Z":
      xbins_low = np.linspace(0, 520, 14)
      xbins_mid = np.linspace(560, 1160, 6)
      xbins_high = np.linspace(1280, 2000, 2)
      xbins = (np.concatenate((xbins_low, xbins_mid, xbins_high)))
      ngroup = len(xbins)-1
    
    if sample.split('_')[0][0]=="W":
      xbins_low = np.linspace(0, 1080, 28)
      xbins_mid = np.linspace(1120, 1360, 3)
      xbins_high = np.linspace(1480, 2000, 3)
      xbins = (np.concatenate((xbins_low, xbins_mid, xbins_high)))
      ngroup = len(xbins)-1

  if variable == "pTV":
    if sample.split('_')[0][0]=="Z":
      xbins_low = np.linspace(0, 820, 27)
      xbins_mid = np.linspace(860, 1100, 3)
      xbins_high = np.linspace(1220, 2000, 4)
      xbins = (np.concatenate((xbins_low, xbins_mid, xbins_high)))
      ngroup = len(xbins)-1
    
    if sample.split('_')[0][0]=="W":
      xbins_low = np.linspace(0, 240, 7)
      xbins_mid = np.linspace(280, 360, 2)
      xbins_high = np.asarray([2000]) #np.linspace(440, 2000, 2)
      xbins = (np.concatenate((xbins_low, xbins_mid, xbins_high)))
      ngroup = len(xbins)-1

  # Uncomment to change the mS_candidate mass binning to 20 GeV (5 GeV by default)
  """
  if variable == "mS_candidate":
    xbins = np.linspace(0, 400, 21)
    ngroup = len(xbins)-1
  """

  return ngroup, xbins

# Function to collect the nominal histogram for a given dataset
def collect_nominal(hists_dir, sample, n_leptons, analysis, category, ptv_str, lepton_region_label, variable):
  
  ngroup, xbins = get_rebin_info(variable, sample)
  #if variable == "mS_candidate": ngroup = 4

#  elif variable == "MET_muInv": bins_rebin = 16
#  elif variable == "mS_candidate": bins_rebin = 4
#  elif variable == "pTV": bins_rebin = 2

  sample_type, sample_root = get_sample_info(sample, analysis)

  infile, base_filename = get_infile(hists_dir, sample, category, n_leptons, analysis)
  
  # Check if there was an issue opening the file, and if so return all 0's.
  if infile == 0: return 0, 0, 0, 0

  # Make sure the nominal event hist exists and is of type TH1F
  if not isinstance(infile.Get("hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_str, lepton_region_label, variable)), ROOT.TH1):
    print("---------> File %s/%s does not contain the nominal histogram. Skipping."%(hists_dir, base_filename))
    return 0, 0, 0, 0

  # Collect the nominal event histogram and convert it to a numpy array
  if isinstance(xbins, int): hist_root = infile.Get("hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_str, lepton_region_label, variable)).Rebin(ngroup)
  else: hist_root = infile.Get("hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_str, lepton_region_label, variable)).Rebin(ngroup, "", xbins)
  nEvents = hist_root.GetEntries()
  hist_nominal, edges = hist2array(hist_root, return_edges=True)
#hist_nominal, edges = hist2array(infile.Get("hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_str, lepton_region_label, variable)), return_edges=True)
  edges = edges[0]
  
  # Get the number of events in each bin
  hist_nominal_err = np.zeros(len(hist_nominal))
  for i in range(hist_root.GetNbinsX()):
    hist_nominal_err[i] = hist_root.GetBinError(i+1)

  return hist_nominal, hist_nominal_err, edges, nEvents


# Function to collect variations for a given dataset
def collect_variations(hists_dir, hist_nominal, sample, n_leptons, analysis, category, ptv_str, lepton_region_label, variable):

  sample_type, sample_root = get_sample_info(sample, analysis)
  
  print("Full Sample Name: %s"%sample)
  print("Sample Name for Reading ROOT files: %s"%sample_root)
  print("Sample type: %s"%sample_type)
  
  # Get variations
  variations, variation_names = set_variations(analysis, sample_type)
  pdf_variations, pdf_variation_names = set_pdf_variations(analysis, sample_type, sample_root)
  
  # Delete the 0th pdf variation because it's actually nominal (this is a bug in the signal samples)
  if sample_type == "signal":
    del pdf_variations[0]
    del pdf_variation_names[0]
  
  infile, base_filename = get_infile(hists_dir, sample, category, n_leptons, analysis)

  # Check if there was an issue opening the file, and if so return all 0's.
  if infile == 0: return 0, 0, 0, 0, 0, 0
  
  # Make sure the nominal event hist exists and is of type TH1F
  print("hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_str, lepton_region_label, variable))
  if not isinstance(infile.Get("hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_str, lepton_region_label, variable)), ROOT.TH1F):
    print("---------> File %s does not contain the nominal histogram. Skipping."%base_filename)
    return 0, 0, 0, 0, 0, 0

  dir = infile.Get("WeightVariations")
  
  # Collect variations
  hist_variations, hist_variations_err = get_hist_variations(infile, sample_root, ptv_str, variations, n_leptons, lepton_region_label, "var", variable)
  hist_pdf_variations, hist_pdf_variations_err = get_hist_variations(infile, sample_root, ptv_str, pdf_variations, n_leptons, lepton_region_label, "pdf", variable)

  return hist_variations, hist_variations_err, variation_names, hist_pdf_variations, hist_pdf_variations_err, pdf_variation_names


# Function to calculate and save relative error for the pdf and scale variations
def evaluate_norm_variations(hist_nominal, hist_variations, hist_pdf_variations, analysis, category, label_str):
  
  index_category=get_index_category(analysis, category)
  
  outfile_scale = open("./output_files/signal_uncertainties_%s_%s_scale.txt"%(label_str, category), "w")
  outfile_pdf = open("./output_files/signal_uncertainties_%s_%s_pdf.txt"%(label_str, category), "w")
  outfile_all = open("./output_files/sys_mc16_13TeV_%s_%s_%s.txt"%(label_str, analysis, category), "w")
  
  print " ****** Evaluating scale variations ******"
  
  hist_variations_ratio = get_variations_ratio(hist_nominal, hist_variations)
  
  max_rel_diff = get_max_rel_diff(hist_nominal, hist_variations)
  
  # Write out the scale relative error info to output files
  write_scale_info(max_rel_diff, index_category, outfile_scale, outfile_all)
  
  print " ****** Evaluating scale variations ******"
  
  hist_pdf_variations_ratio = get_variations_ratio(hist_nominal, hist_pdf_variations)
  
  # Calculate the standard deviation of pdf variations, relative to nominal values
  round_error_pdf = collect_pdf_error(hist_nominal, hist_pdf_variations)
  
  # Write out the pdf relative error info to output files
  write_pdf_info(round_error_pdf, index_category, outfile_pdf, outfile_all)
  
  outfile_scale.close()
  outfile_pdf.close()
  outfile_all.close()

# Function to evaluate the acceptance double ratios for all variations
def evaluate_acceptance_double_ratios(hist_nominal_regionA, hist_nominal_regionB, hist_variations_regionA, hist_variations_regionB, nEvents_regionA, nEvents_mgpy_regionA, nEvents_regionB, nEvents_mgpy_regionB, varType):

  # Make sure the two lists of variations have the same length
  len_varsA = len(hist_variations_regionA)
  len_varsB = len(hist_variations_regionB)
  if len_varsA != len_varsB:
    print("ERROR: Lists of variation hists supplied to evaluate_acceptance_double_ratios() have different lengths (%d, %d). Exiting."%(len_varsA, len_varsB))
    exit()

  doubleRatios = []
  doubleRatios_statUnc = []

  for iVar in range(len_varsA):
    N_nom_regionA = np.sum(hist_nominal_regionA)
    N_nom_regionB = np.sum(hist_nominal_regionB)
    N_var_regionA = np.sum(hist_variations_regionA[iVar])
    N_var_regionB = np.sum(hist_variations_regionB[iVar])
    doubleRatio = ( N_var_regionB/N_var_regionA ) / (N_nom_regionB/N_nom_regionA)
    doubleRatios.append(doubleRatio-1.)
    if varType=="gen": doubleRatios_statUnc.append(doubleRatio*np.sqrt(1./nEvents_regionA + 1./nEvents_mgpy_regionA + 1./nEvents_regionB + 1./nEvents_mgpy_regionB))
    else: doubleRatios_statUnc.append(doubleRatio*np.sqrt(1./nEvents_regionA + 1./nEvents_regionB))

  return np.asarray(doubleRatios), np.asarray(doubleRatios_statUnc)

def evaluate_acceptance_single_ratios(hist_nominal, hist_variations, nEvents, nEvents_mgpy, varType):
  singleRatios = []
  singleRatios_statUnc = []

  for iVar in range(len(hist_variations)):
    N_nom = np.sum(hist_nominal)
    N_var = np.sum(hist_variations[iVar])
    singleRatio = N_var/N_nom
    singleRatios.append(singleRatio-1.)
    if varType=="gen": singleRatios_statUnc.append(singleRatio*np.sqrt(1./nEvents + 1/nEvents_mgpy))
    else: singleRatios_statUnc.append(singleRatio*np.sqrt(1./nEvents))

  return np.asarray(singleRatios), np.asarray(singleRatios_statUnc)

# Function to evaluate the shape systematic for a given set of variations
def evaluate_shape_unc(hist_nominal, hist_variations, bin_edges, n_leptons, category, variable, label_str, type):

  # Normalize the two histograms
  hist_nominal_norm = hist_nominal / np.sum(hist_nominal)
  hist_variations_norm = []
  n_variations = len(hist_variations)
  hist_shapeUncs_arr = np.zeros((n_variations, len(hist_variations[0])))
  for iVar in range(n_variations):
    hist_variations_norm.append(hist_variations[iVar] / np.sum(hist_variations[iVar]))

  hist_shapeUncs = get_variations_ratio(hist_nominal_norm, hist_variations_norm)
  
  for iVar in range(n_variations):
    hist_shapeUncs_arr[iVar, :] = hist_shapeUncs[iVar]

  # Find the envelope of upward fluctuations
  hist_shapeUncs_up = np.max(hist_shapeUncs_arr, axis=0)
  hist_shapeUncs_up[hist_shapeUncs_up < 0] = 0 # If the maximum fluctuation in a bin is negative, set the 'upward' fluctuation to 0.

  # Find the envelope of downward fluctuations
  hist_shapeUncs_down = np.min(hist_shapeUncs_arr, axis=0)
  hist_shapeUncs_down[hist_shapeUncs_down > 0] = 0 # If the minimum fluctuation in a bin is positive, set the 'downward' fluctuation to 0.

  # Create the hist to write to
  if not os.path.exists("ShapeHists_%s"%type):
    os.makedirs("ShapeHists_%s"%type)
  
  outHistFile = ROOT.TFile.Open("ShapeHists_%s/output%dLep_%s_%s_%s.root"%(type, n_leptons, category, label_str, variable) ,"RECREATE")
  outHistFile.cd()

  # If it's a 2-point variation, combine the upward and downward variations into a single histogram
  if type=="2point":
    hist_shapeUncs_combined = hist_shapeUncs_up + hist_shapeUncs_down
    hist_combined = ROOT.TH1F("sys", "sys", len(bin_edges)-1, bin_edges)
    _ = array2hist(hist_shapeUncs_combined, hist_combined)
    hist_combined.Write()

  # For non-2point variations, save both the up and down systematics to a ROOT file in separate branches
  else:
    hist_up = ROOT.TH1F("sys_up", "sys_up", len(bin_edges)-1, bin_edges)
    _ = array2hist(hist_shapeUncs_up, hist_up)
    hist_up.Write()

    hist_down = ROOT.TH1F("sys_down", "sys_down", len(bin_edges)-1, bin_edges)
    _ = array2hist(hist_shapeUncs_down, hist_down)
    hist_down.Write()

  outHistFile.Close()

  return hist_shapeUncs

# Function to get a ROOT-style polynomial fit function for a given ratio histogram
def get_ratio_fit_poly(hist_ratio_root, x_low, x_high, n_bins, label, n_leptons, order='7'):

  polyFuncs_dict = {
  "1": "[0] + [1]*x",
  "2": "[0] + [1]*x + [2]*x**2",
  "3": "[0] + [1]*x + [2]*x**2 + [3]*x**3",
  "7": "[0] + [1]*x + [2]*x**2 + [3]*x**3 + [4]*x**4 + [5]*x**5 + [6]*x**6+ [7]*x**7",
}

  # Fit to a 7th-order polynomial
  fitfunc = polyFuncs_dict[order] # polynomial of specified order
  func = TF1('func', fitfunc , x_low, x_high)
  func.SetLineColor(kBlue)
  func.SetLineWidth(4)
  
  fit = hist_ratio_root.Fit('func', "S", "", x_low, x_high)
  
  # Print out some fitting info
  print("NDF: %f. Chi2: %f"%(fit.Ndf(), fit.Chi2()))
  print("chi2/ndf = %f"%(fit.Chi2()/fit.Ndf()))

  return func, fit.Chi2(), fit.Ndf()


# Function fit a cubic spline to a given ratio histogram
def get_ratio_fit_spline(hist_ratio_root, label, n_leptons):
  spline = ROOT.TSpline3(hist_ratio_root)
  spline.SetLineColor(kBlue)
  spline.SetLineWidth(2)
  
  return spline


# Dictionary to contain the nominal and variation hists for the three categories in the SR
class CategoryHists:
  def __init__(self):
    self.Hists_dict = {
      'Merged_nominal': None,
      'Merged_pdf_list': None,
      'Merged_scale_list': None,
      'Merged_gen_list': None,
      'Merged_nEvents': None,
      'Merged_nEvents_mgpy': None,
      'Intermediate_nominal': None,
      'Intermediate_nominal_reco': None,
      'Intermediate_pdf_list': None,
      'Intermediate_scale_list': None,
      'Intermediate_gen_list': None,
      'Intermediate_gen_list_reco': None,
      'Intermediate_nEvents': None,
      'Intermediate_nEvents_reco': None,
      'Intermediate_nEvents_mgpy': None,
      'Intermediate_nEvents_mgpy_reco': None,
      'Resolved_nominal': None,
      'Resolved_nominal_reco': None,
      'Resolved_pdf_list': None,
      'Resolved_scale_list': None,
      'Resolved_gen_list': None,
      'Resolved_gen_list_reco': None,
      'Resolved_nEvents': None,
      'Resolved_nEvents_mgpy': None,
      'Resolved_nEvents_reco': None,
      'Resolved_nEvents_mgpy_reco': None,
      'Inclusive_nominal': None,
      'Inclusive_pdf_list': None,
      'Inclusive_scale_list': None,
      'Inclusive_gen_list': None,
      'Inclusive_nEvents': None,
      'Inclusive_nEvents_mgpy': None,
    }

  # Fill all but the generator keys in Hists_dict for the given category
  def fill_hists(self, nominal, scale_list, pdf_list, gen_list, nEvents, nEvents_mgpy, category_str):
    self.Hists_dict["%s_nominal"%category_str] = nominal
    self.Hists_dict["%s_pdf_list"%category_str] = pdf_list
    self.Hists_dict["%s_scale_list"%category_str] = scale_list
    self.Hists_dict["%s_gen_list"%category_str] = gen_list
    self.Hists_dict["%s_nEvents"%category_str] = nEvents
    self.Hists_dict["%s_nEvents_mgpy"%category_str] = nEvents_mgpy

  def fill_hists_reco(self, nominal_reco, gen_list_reco, nEvents_reco, nEvents_mgpy_reco, category_str):
    self.Hists_dict["%s_nominal_reco"%category_str] = nominal_reco
    self.Hists_dict["%s_gen_list_reco"%category_str] = gen_list_reco
    self.Hists_dict["%s_nEvents_reco"%category_str] = nEvents_reco
    self.Hists_dict["%s_nEvents_mgpy_reco"%category_str] = nEvents_mgpy_reco

# Dictionary to contain the CategoryHists classes for the three analysis regions
RegionHists_dict = {
  'SR': CategoryHists(),
  'CR1L': CategoryHists(),
  'CR2L': CategoryHists()
}

# Dictionary to contain the CategoryHists classes for the two generators
GeneratorHists_dict = {
  'Sh': CategoryHists(),
  'MGPy': CategoryHists()
}

def main():
  categories = ["Merged", "Intermediate", "Resolved", "Inclusive"]
  draw_plots = True
  variables_reweight = ["MET", "MET_muInv", "pTV"]
  variableTitles_reweight = ["MET [GeV]", "MET (muInv) [GeV]", "pTV [GeV]"]
  args = getArgs()
  analysis, hists_dir, sample_name, ptv_str, variable, variableTitle, logY, reweight, use_reweighted_mgpy, do_recast = check_args(args)
  variable_init = variable
  
  # Make the Sherpa and MGPy8 versions of the sample name
  sample_type, sample_root = get_sample_info(sample_name, analysis)
  
  reweighted_MGPy_str = ""
  if sample_type == "background" and not use_reweighted_mgpy: reweighted_MGPy_str = "no_reweighting"
  
  if sample_type == "background": sample_name_sherpa = sample_name + "_Sh"
  else: sample_name_sherpa = sample_name
  sample_name_mgpy = sample_name + "_MGPy"
  if use_reweighted_mgpy: sample_name_mgpy += "_reweighted"
  
  if sample_type == "signal":
    varTypes = ["pdf", "scale"]
    all_lep = [0]
    all_regions = ["SR"]
  else:
    varTypes = ["pdf", "scale", "gen"]
    all_lep = [0, 1, 2]
    all_regions = ["SR", "CR1L", "CR2L"]
  
  # Loop over all regions
  for n_leptons in all_lep:
    
    if reweight:
      variable = variables_reweight[n_leptons]
      variableTitle = variableTitles_reweight[n_leptons]

    # Plot pTV 'aliases' in each region if pTV specified as the plotting variable
    if variable_init=="pTV":
      variable = variables_reweight[n_leptons]
      variableTitle = variableTitles_reweight[n_leptons]

    # Make the lepton region label for reading hists from the root files
    lepton_region_label = make_lepton_region_label(n_leptons)
  
    # Make the label for output files
    label = make_output_label(sample_name, ptv_str)
    if sample_type == "signal" and int(sample_name) >= 310773 and int(sample_name) <= 312661: label = "ww_" + label
    if sample_type == "signal" and int(sample_name) >= 312903 and int(sample_name) <= 312927: label = "zz_" + label

    met_str = reformat_ptv_str(ptv_str)   # Some outputs require the ptv bin to be specified at met
    if sample_type == "signal":
      sample_name_str = get_sample_name(sample_name)  # Get name of signal sample if input is signal
      if sample_type == "signal" and int(sample_name) >= 310773 and int(sample_name) <= 312661: sample_name_str = "ww_" + sample_name_str
      if sample_type == "signal" and int(sample_name) >= 312903 and int(sample_name) <= 312927: sample_name_str = "zz_" + sample_name_str
    else: sample_name_str = sample_name

    if do_recast: label = "recast_%s"%ptv_str
  
    f_sumWeights = open("output_files/sumWeights_%s_%dLep%s.txt"%(label, n_leptons, reweighted_MGPy_str), "write")

    print("Going to draw all variations for %s %s_Lep samples of type %s: scale + pdf + gen"%(analysis, n_leptons, label))

    # Loop over all the analysis categories
    for category in categories:
      
      if (sample_name == "Zjets" or sample_name == "Wjets") and (category == "Intermediate" or category == "Resolved") and variable == "mS_candidate": doReco_2pt_shape = False
      else: doReco_2pt_shape = False
      
      print("Handling category %s"%category)
  
      # Collect nominal histogram and scale+pdf variations for first (and perhaps only) sample name
      hist_nominal, hist_nominal_err, bin_edges, nEvents = collect_nominal(hists_dir, sample_name_sherpa, n_leptons, analysis, category, ptv_str, lepton_region_label, variable)
      
      # Collect the nominal reco-level samples if we're using these for the shape systematics
      if doReco_2pt_shape: hist_nominal_reco, hist_nominal_err_reco, bin_edges_reco, nEvents_reco = collect_nominal(hists_dir + "/RecoLevel", sample_name_sherpa, n_leptons, analysis, category, ptv_str, lepton_region_label, variable)
      
      hist_variations, hist_variations_err, variation_names, hist_pdf_variations, hist_pdf_variations_err, pdf_variation_names = collect_variations(hists_dir, hist_nominal, sample_name_sherpa, n_leptons, analysis, category, ptv_str, lepton_region_label, variable)
    
      sumWeights_sh = np.sum(hist_nominal)
      unc_sumWeights_sh = sumWeights_sh / np.sqrt(nEvents)
      f_sumWeights.write("%s Sherpa: %.2f +/- %.2f\n"%(category, sumWeights_sh, unc_sumWeights_sh))
      
      if not isinstance(hist_nominal, int):
        
        # Fix a known bug in the scale variations, where the nominal histogram is actually one of the variations, and vice versa
        hist_nominal, hist_variations = fix_signal_variation_bug(hist_nominal, hist_variations)
        
        # Evaluate and save shape systematics for the scale and pdf variations
        evaluate_shape_unc(hist_nominal, hist_variations, bin_edges, n_leptons, category, variable, label, "scale")
        evaluate_shape_unc(hist_nominal, hist_pdf_variations, bin_edges, n_leptons, category, variable, label, "pdf")
        
        # Draw variations for the given sample
        draw_variations = DrawVariations(category, hist_nominal, hist_nominal_err, bin_edges, sample_type, n_leptons, variable, variableTitle, logY)
  
        if draw_plots:
          draw_variations.plot_variations(variation_names, hist_variations, hist_variations_err, label, ptv_str, sample_name, "scale", use_reweighted_mgpy=use_reweighted_mgpy)
          draw_variations.plot_variations(variation_names, hist_variations, hist_variations_err, label, ptv_str, sample_name, "scale", use_reweighted_mgpy=use_reweighted_mgpy, plotShape=True)   # Shape variations
          draw_variations.plot_variations(pdf_variation_names, hist_pdf_variations, hist_pdf_variations_err, label, ptv_str, sample_name, "pdf", use_reweighted_mgpy=use_reweighted_mgpy)
          draw_variations.plot_variations(pdf_variation_names, hist_pdf_variations, hist_pdf_variations_err, label, ptv_str, sample_name, "pdf", use_reweighted_mgpy=use_reweighted_mgpy, plotShape=True)   # Shape variations
      
       #if sample_type == "signal": evaluate_norm_variations(hist_nominal, hist_variations, hist_pdf_variations, analysis, category, label)
          
        hist_nominal_mgpy = None
        nEvents_mgpy = None
          
        # Do stuff with the alternative sample if doing background
        if sample_type == "background":
          # Collect the nominal variation with the alternative generator
          hist_nominal_mgpy, hist_nominal_err_mgpy, bin_edges, nEvents_mgpy = collect_nominal(hists_dir, sample_name_mgpy, n_leptons, analysis, category, ptv_str, lepton_region_label, variable)
          
          # Collect the reco-level samples with the alternative generator if we're using these for the shape systematics
          if doReco_2pt_shape: hist_nominal_mgpy_reco, hist_nominal_err_mgpy_reco, bin_edges_reco, nEvents_mgpy_reco = collect_nominal(hists_dir + "/RecoLevel", sample_name_mgpy, n_leptons, analysis, category, ptv_str, lepton_region_label, variable)
          
          if not isinstance(hist_nominal_mgpy, int):
        
            # Calculate and save the sum of weights for both generators
            sumWeights_mgpy = np.sum(hist_nominal_mgpy)
            unc_sumWeights_mgpy = sumWeights_mgpy / np.sqrt(nEvents_mgpy)
            f_sumWeights.write("%s MGPy: %.2f +/- %.2f\n"%(category, sumWeights_mgpy, unc_sumWeights_mgpy))
            f_sumWeights.write("---> Diff: %.2f +/- %.2f\n\n"%(sumWeights_sh-sumWeights_mgpy, np.sqrt(unc_sumWeights_sh**2 + unc_sumWeights_mgpy**2)))
        
            # Plot 2-point variations
            if not ( isinstance(hist_nominal, int) ):
              if reweight and category=="Inclusive": draw_variations.plot_variations(["MGPy8"], [hist_nominal_mgpy], [hist_nominal_err_mgpy], label, ptv_str, sample_name, "2point", plotShape=False, nEvents=nEvents, nEvents_mgpy=nEvents_mgpy, reweight=True, use_reweighted_mgpy=False)
              else: draw_variations.plot_variations(["MGPy8"], [hist_nominal_mgpy], [hist_nominal_err_mgpy], label, ptv_str, sample_name, "2point", plotShape=False, nEvents=nEvents, nEvents_mgpy=nEvents_mgpy, reweight=False, use_reweighted_mgpy=use_reweighted_mgpy)
              
              if doReco_2pt_shape: draw_variations.plot_variations(["MGPy8"], [hist_nominal_mgpy_reco], [hist_nominal_err_mgpy_reco], label, ptv_str, sample_name, "2point", plotShape=True, nEvents=nEvents_reco, nEvents_mgpy=nEvents_mgpy_reco, reweight=False, use_reweighted_mgpy=use_reweighted_mgpy, reco=True)   # Shape variations only, with reco-level hists
              else: draw_variations.plot_variations(["MGPy8"], [hist_nominal_mgpy], [hist_nominal_err_mgpy], label, ptv_str, sample_name, "2point", plotShape=True, nEvents=nEvents, nEvents_mgpy=nEvents_mgpy, reweight=False, use_reweighted_mgpy=use_reweighted_mgpy)  # Shape variations only
              bin_edges_nonzero = bin_edges
        
              # Calculate and save the 2-point shape systematic
              if doReco_2pt_shape: evaluate_shape_unc(hist_nominal_reco, [hist_nominal_mgpy_reco], bin_edges_reco, n_leptons, category, variable, label, "2point")
              else: evaluate_shape_unc(hist_nominal, [hist_nominal_mgpy], bin_edges, n_leptons, category, variable, label, "2point")
            else:
              # Get the name of the background file that's empty
              if analysis == "monoSWW": filename = "%s/output_%dLep_%s_%s.root"%(hists_dir, n_leptons, category, sample_name_mgpy)
              else: filename = "%s/output_%dLep_%s.root"%(hist_dir, n_leptons, sample_name_mgpy)
              path, base_filename = os.path.split(filename)
              
              # Report that we're exiting because this background file is empty
              print("The sample %s is empty. Cannot proceed with double ratio estimation."%base_filename)
      
          else:
            # Get the name of the background file that's empty
            if analysis == "monoSWW": filename = "%s/output_%dLep_%s_%s.root"%(hists_dir, n_leptons, category, sample_name_sherpa)
            else: filename = "%s/output_%dLep_%s.root"%(hist_dir, n_leptons, sample_name_sherpa)
            path, base_filename = os.path.split(filename)
          
            # Report that we're exiting because this background file is empty
            print("The background sample %s is empty. Cannot proceed with double ratio estimation."%base_filename)
      
        # Fill the region hists dictionary with the nominal hist, and all variations
        RegionHists_dict[lepton_region_label].fill_hists(hist_nominal, hist_variations, hist_pdf_variations, [hist_nominal_mgpy], nEvents, nEvents_mgpy, category)

      print("\n\n\n")
        
  f_sumWeights.close()

  # Evaluate all scale, pdf, and generator acceptance double-ratios of background sample
  if reweight == False:
      
    # Calculate and save the acceptance ratio between Sherpa and MGPy8 for each region
    SR_dict = {}
    SR_dict[sample_name_str] = {}
    f_sr_all = open("output_files/singleRatios_%s%s.txt"%(label, reweighted_MGPy_str), "w")
    f_sr_statunc_all = open("output_files/singleRatios_statunc_%s%s.txt"%(label, reweighted_MGPy_str), "w")
  
    f_sr_all.write("%15s%12s%12s%12s%12s%12s%12s%12s\n"%("Comparison", "pdf up", "pdf down", "pdf", "scale up", "scale down", "scale", "2pt"))
    f_sr_statunc_all.write("%15s%12s%12s%12s%12s%12s%12s%12s\n"%("Comparison", "pdf up", "pdf down", "pdf", "scale up", "scale down", "scale", "2pt"))
    
    for region in all_regions:
      for category in categories:
        iVarType=0
        for varType in varTypes:
          print("--------> Evaluating single ratio for %s %s with variation type %s"%(region, category, varType))
        
          f_sr_all.write("%15s"%("%s (%s)"%(region, category[0:4])))
          f_sr_statunc_all.write("%15s"%("%s (%s)"%(region, category[0:4])))
      
          try:
            hist_nom = RegionHists_dict[region].Hists_dict["%s_nominal"%category]
            hist_gen = RegionHists_dict[region].Hists_dict["%s_gen_list"%category][0]
            nEvents = RegionHists_dict[region].Hists_dict["%s_nEvents"%category]
            nEvents_mgpy = RegionHists_dict[region].Hists_dict["%s_nEvents_mgpy"%category]
          
          except TypeError:
            print("No data for %s %s, skipping"%(region, category))
            continue
    
          try:
            singleRatios, singleRatios_statUnc = evaluate_acceptance_single_ratios(RegionHists_dict[region].Hists_dict["%s_nominal"%category], RegionHists_dict[region].Hists_dict["%s_%s_list"%(category, varType)], RegionHists_dict[region].Hists_dict["%s_nEvents"%category], RegionHists_dict[region].Hists_dict["%s_nEvents_mgpy"%category], varType)
          
            sr_max = np.max(np.abs(singleRatios))   # Max of all (up and down) fluctuations
            sr_up = max(0, np.max(singleRatios))    # Max of upward fluctuations
            sr_down = min(0, np.min(singleRatios))  # Max of downward fluctuations
          
            f_out = open("output_files/singleRatio_%s_%s_%s_%s%s.txt"%(label, region, varType, category, reweighted_MGPy_str), "w")
            f_out.write("\nMax: %.2f%% +/- %.2f%%"%(100*sr_max, 100*singleRatios_statUnc[np.abs(singleRatios) == sr_max]))
            f_out.write("\nUp: %.6f%%"%(100*sr_up))
            f_out.write("\nDown: %.6f%%"%(100*sr_down))
            f_out.close()
          
            # Calculate the stat unc of the absolute maximum double ratio systematic
            statunc_max = singleRatios_statUnc[np.abs(singleRatios) == sr_max]
            
            # For 'gen' (i.e. 2-point) systematic, just take the absolute value of the double ratio systematic, since there's only one variation (so we won't get a 2-sided variation)
            if iVarType==0: SR_dict[sample_name_str]["%s_(%s)_%s"%(region, category[0:4], met_str)] = {}
            if varType=="gen":
              SR_dict[sample_name_str]["%s_(%s)_%s"%(region, category[0:4], met_str)][varType] = str(sr_max)
              f_sr_all.write("%12.7f"%(sr_max))
              
              # Make sure we actually got back a number, otherwise set the stat unc to 0
              if len(statunc_max) > 0: f_sr_statunc_all.write("%12.7f"%(statunc_max))
              else: f_sr_statunc_all.write("%12.7f"%(0))
            else:
              #SR_dict[sample_name_str]["SR/" + CR + "_(%s)_%s"%(category[0:4], met_str)][varType] = str(dr_max)
              SR_dict[sample_name_str]["%s_(%s)_%s"%(region, category[0:4], met_str)][varType + " down"] = str(sr_down)
              SR_dict[sample_name_str]["%s_(%s)_%s"%(region, category[0:4], met_str)][varType + " up"] = str(sr_up)
              f_sr_all.write("%12.7f%12.7f%12.7f"%(sr_up, sr_down, sr_max))
              
              # Calculate the stat unc on the up and down ratio systematics
              statunc_up = singleRatios_statUnc[singleRatios == sr_up]
              statunc_down = singleRatios_statUnc[singleRatios == sr_down]
              
              if len(statunc_up) > 0: f_sr_statunc_all.write("%12.7f"%(statunc_up))
              else: f_sr_statunc_all.write("%12.7f"%(0))
              
              if len(statunc_down) > 0: f_sr_statunc_all.write("%12.7f"%(statunc_down))
              else: f_sr_statunc_all.write("%12.7f"%(0))
              
              f_sr_statunc_all.write("%12.7f"%(statunc_max))
      
          except ZeroDivisionError:
            print("No events for one or both generators in %s %s, skipping "%(region, category))
            continue

          iVarType+=1
            
        f_sr_all.write("\n")
        f_sr_statunc_all.write("\n")
    
    f_sr_all.close()
    f_sr_statunc_all.close()
    print(SR_dict)
    with open("output_files/%s%s_singleratio.yml"%(label, reweighted_MGPy_str), 'w') as outfile:
      yaml.dump(SR_dict, outfile, default_flow_style=False)
  
    print("\n\n")
    
    # Evaluate double-ratios between signal and control regions
    f_dr_all = open("output_files/doubleRatios_%s%s.txt"%(label, reweighted_MGPy_str), "w")
    f_dr_statunc_all = open("output_files/doubleRatios_statunc_%s%s.txt"%(label, reweighted_MGPy_str), "w")

    f_dr_all.write("%15s%12s%12s%12s%12s%12s%12s%12s\n"%("Comparison", "pdf up", "pdf down", "pdf", "scale up", "scale down", "scale", "2pt"))
    f_dr_statunc_all.write("%15s%12s%12s%12s%12s%12s%12s%12s\n"%("Comparison", "pdf up", "pdf down", "pdf", "scale up", "scale down", "scale", "2pt"))


    DR_dict = {}
    DR_dict[sample_name_str] = {}
    if sample_type == "background":
      for CR in ["CR1L", "CR2L"]:
        for category in categories:
          # Make sure there's actually data for both regions
          try:
            hist_gen_SR = RegionHists_dict['SR'].Hists_dict["%s_gen_list"%category][0]
            hist_gen_CR = RegionHists_dict[CR].Hists_dict["%s_gen_list"%category][0]
          except TypeError:
            print("No data for %s %s, skipping"%(region, category))
            continue
          
          # Make sure there's at least one event in both regions
          if RegionHists_dict[CR].Hists_dict["%s_nEvents"%category] == 0 or RegionHists_dict[CR].Hists_dict["%s_nEvents_mgpy"%category] == 0 or RegionHists_dict['SR'].Hists_dict["%s_nEvents"%category] == 0 or RegionHists_dict['SR'].Hists_dict["%s_nEvents_mgpy"%category] == 0:
            print("No data for one of the regions in %s %s, skipping"%(region, category))
            continue
      
          f_dr_all.write("%15s"%("SR/" + CR + " (%s)"%category[0:4]))
          f_dr_statunc_all.write("%15s"%("SR/" + CR + " (%s)"%category[0:4]))
          
          iVarType=0
          for varType in ["pdf", "scale", "gen"]:
            print("--------> Evaluating double ratio between SR and %s for category %s and variation type %s"%(CR, category, varType))
            
            doubleRatios, doubleRatios_statUnc = evaluate_acceptance_double_ratios(RegionHists_dict[CR].Hists_dict["%s_nominal"%category], RegionHists_dict['SR'].Hists_dict["%s_nominal"%category], RegionHists_dict[CR].Hists_dict["%s_%s_list"%(category, varType)], RegionHists_dict['SR'].Hists_dict["%s_%s_list"%(category, varType)], RegionHists_dict[CR].Hists_dict["%s_nEvents"%category], RegionHists_dict[CR].Hists_dict["%s_nEvents_mgpy"%category], RegionHists_dict['SR'].Hists_dict["%s_nEvents"%category], RegionHists_dict['SR'].Hists_dict["%s_nEvents_mgpy"%category], varType)
            
            
            dr_max = np.max(np.abs(doubleRatios))   # Max of all (up and down) fluctuations
            dr_up = max(0, np.max(doubleRatios))    # Max of upward fluctuations
            dr_down = min(0, np.min(doubleRatios))  # Max of downward fluctuations
            
            f_out = open("output_files/doubleRatio_%s_SRvs%s_%s_%s%s.txt"%(label, CR, varType, category, reweighted_MGPy_str), "w")
            f_out.write("\nMax: %.2f%% +/- %.2f%%"%(100*dr_max, 100*doubleRatios_statUnc[np.abs(doubleRatios) == dr_max]))
            f_out.write("\nUp: %.6f%%"%(100*dr_up))
            f_out.write("\nDown: %.6f%%"%(100*dr_down))
            f_out.close()
            
            # Calculate the stat unc of the absolute maximum double ratio systematic
            statunc_max = doubleRatios_statUnc[np.abs(doubleRatios) == dr_max]
            
            # For 'gen' (i.e. 2-point) systematic, just take the absolute value of the double ratio systematic, since there's only one variation (so we won't get a 2-sided variation)
            if iVarType==0: DR_dict[sample_name_str]["SR/" + CR + "_(%s)_%s"%(category[0:4], met_str)] = {}
            if varType=="gen":
              DR_dict[sample_name_str]["SR/" + CR + "_(%s)_%s"%(category[0:4], met_str)][varType] = str(dr_max)
              f_dr_all.write("%12.7f"%(dr_max))
              
              # Make sure we actually got back a number, otherwise set the stat unc to 0
              if len(statunc_max) > 0: f_dr_statunc_all.write("%12.7f"%(statunc_max))
              else: f_dr_statunc_all.write("%12.7f"%(0))
            else:
              #DR_dict[sample_name_str]["SR/" + CR + "_(%s)_%s"%(category[0:4], met_str)][varType] = str(dr_max)
              DR_dict[sample_name_str]["SR/" + CR + "_(%s)_%s"%(category[0:4], met_str)][varType + " down"] = str(dr_down)
              DR_dict[sample_name_str]["SR/" + CR + "_(%s)_%s"%(category[0:4], met_str)][varType + " up"] = str(dr_up)
              f_dr_all.write("%12.7f%12.7f%12.7f"%(dr_up, dr_down, dr_max))
              
              # Calculate the stat unc on the up and down ratio systematics
              statunc_up = doubleRatios_statUnc[doubleRatios == dr_up]
              statunc_down = doubleRatios_statUnc[doubleRatios == dr_down]
                
              if len(statunc_up) > 0: f_dr_statunc_all.write("%12.7f"%(statunc_up))
              else: f_dr_statunc_all.write("%12.7f"%(0))
                
              if len(statunc_down) > 0: f_dr_statunc_all.write("%12.7f"%(statunc_down))
              else: f_dr_statunc_all.write("%12.7f"%(0))
                
              f_dr_statunc_all.write("%12.7f"%(statunc_max))
              
              iVarType+=1
    
          f_dr_all.write("\n")
          f_dr_statunc_all.write("\n")
    
      print("\n\n")

    # Evaluate double-ratios between the different categories
    N_categories = 3
    for region in all_regions:
      for iCat1 in range(N_categories):
        for iCat2 in range(iCat1+1, N_categories):
          categoryA = categories[iCat1]
          categoryB = categories[iCat2]
          
          # Make sure there's actually data for both regions
          noData = False
          for category in [categoryA, categoryB]:
            try:
              hist_gen = RegionHists_dict[region].Hists_dict["%s_gen_list"%category][0]
            except TypeError:
              print("No data for %s %s, skipping"%(region, category))
              noData = True
        
        # Make sure there's at least one event in both regions
          for category in [categoryA, categoryB]:
            if RegionHists_dict[region].Hists_dict["%s_nEvents"%category] == 0:
              print("No data for one of the regions in %s %s, skipping"%(region, category))
              noData = True
        
          if noData: continue
        
          f_dr_all.write("%15s"%(categoryB[0:3] + "/" + categoryA[0:3] + " (%s)"%region))
          f_dr_statunc_all.write("%15s"%(categoryB[0:3] + "/" + categoryA[0:3] + " (%s)"%region))
          
          iVarType=0
          for varType in varTypes:
            print("--------> Evaluating double ratio between %s and %s in the %s for variation type %s"%(categoryA, categoryB, region, varType))
          
            doubleRatios, doubleRatios_statUnc = evaluate_acceptance_double_ratios(RegionHists_dict[region].Hists_dict["%s_nominal"%categoryA], RegionHists_dict[region].Hists_dict["%s_nominal"%categoryB], RegionHists_dict[region].Hists_dict["%s_%s_list"%(categoryA, varType)], RegionHists_dict[region].Hists_dict["%s_%s_list"%(categoryB, varType)], RegionHists_dict[region].Hists_dict["%s_nEvents"%(categoryA)], RegionHists_dict[region].Hists_dict["%s_nEvents_mgpy"%(categoryA)], RegionHists_dict[region].Hists_dict["%s_nEvents"%(categoryB)], RegionHists_dict[region].Hists_dict["%s_nEvents_mgpy"%(categoryB)], varType)
            
            dr_max = np.max(np.abs(doubleRatios))   # Max of all (up and down) fluctuations
            dr_up = max(0, np.max(doubleRatios))    # Max of upward fluctuations
            dr_down = min(0, np.min(doubleRatios))  # Max of downward fluctuations

            f_out = open("output_files/doubleRatio_%s_%s_%svs%s_%s%s.txt"%(label, region, categoryA, categoryB, varType, reweighted_MGPy_str), "w")
            f_out.write("\nMax: %.2f%% +/- %.2f%%"%(100*dr_max, 100*doubleRatios_statUnc[np.abs(doubleRatios) == dr_max]))
            f_out.write("\nUp: %.6f%%"%(100*dr_up))
            f_out.write("\nDown: %.6f%%"%(100*dr_down))
            f_out.close()
            
            # Calculate the stat unc of the absolute maximum double ratio systematic
            statunc_max = doubleRatios_statUnc[np.abs(doubleRatios) == dr_max]
            
            if varType=="gen":
              if iVarType==0: DR_dict[sample_name_str][categoryB[0:3] + "/" + categoryA[0:3] + "_(%s)_%s"%(region, met_str)] = {}
              DR_dict[sample_name_str][categoryB[0:3] + "/" + categoryA[0:3] + "_(%s)_%s"%(region, met_str)][varType] = str(dr_max)
              f_dr_all.write("%12.7f"%(dr_max))
              
              # Make sure we actually got back a number, otherwise set the stat unc to 0
              if len(statunc_max) > 0: f_dr_statunc_all.write("%12.7f"%(statunc_max))
              else: f_dr_statunc_all.write("%12.7f"%(0))
            else:
              if iVarType==0: DR_dict[sample_name_str][categoryB[0:3] + "/" + categoryA[0:3] + "_(%s)_%s"%(region, met_str)] = {}
              #DR_dict[sample_name_str][categoryB[0:3] + "/" + categoryA[0:3] + "_(%s)_%s"%(region, met_str)][varType] = str(dr_max)
              DR_dict[sample_name_str][categoryB[0:3] + "/" + categoryA[0:3] + "_(%s)_%s"%(region, met_str)][varType + " down"] = str(dr_down)
              DR_dict[sample_name_str][categoryB[0:3] + "/" + categoryA[0:3] + "_(%s)_%s"%(region, met_str)][varType + " up"] = str(dr_up)
              f_dr_all.write("%12.7f%12.7f%12.7f"%(dr_up, dr_down, dr_max))

              # Calculate the stat unc on the up and down ratio systematics
              statunc_up = doubleRatios_statUnc[doubleRatios == dr_up]
              statunc_down = doubleRatios_statUnc[doubleRatios == dr_down]
    
              if len(statunc_up) > 0: f_dr_statunc_all.write("%12.7f"%(statunc_up))
              else: f_dr_statunc_all.write("%12.7f"%(0))
                
              if len(statunc_down) > 0: f_dr_statunc_all.write("%12.7f"%(statunc_down))
              else: f_dr_statunc_all.write("%12.7f"%(0))
          
              f_dr_statunc_all.write("%12.7f"%(statunc_max))
              iVarType+=1
                    
          f_dr_all.write("\n")
          f_dr_statunc_all.write("\n")
    f_dr_all.close()
    f_dr_statunc_all.close()
    with open("output_files/%s%s.yml"%(label, reweighted_MGPy_str), 'w') as outfile:
      yaml.dump(DR_dict, outfile, default_flow_style=False)

if __name__ == '__main__':
    main()
