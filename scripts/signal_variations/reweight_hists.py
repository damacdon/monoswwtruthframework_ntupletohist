from argparse import ArgumentParser
from pprint import pprint, pformat
import os
import csv
from math import sqrt, log
import logging
import ROOT
from ROOT import *
ROOT.gROOT.SetBatch(True)
from commonUtils import *

vars_dict = {
"SR": ["MET", "mS_candidate"],
"CR1L": ["MET", "mS_candidate", "pTV"],
"CR2L": ["MET", "mS_candidate", "pTV"],
}

pTV_regions_dict = {
"200_300ptv": [200, 300],
"300_500ptv": [300, 500],
"500ptv": [500, 9e20],
"allptv": [200, 9e20]
}

cutoff_dict = {
"Zjets_0Lep": 1600,
"Wjets_0Lep": 1100,
"Zjets_1Lep": 1200,
"Wjets_1Lep": 1500,
"Zjets_2Lep": 1870,
"Wjets_2Lep": 360,
}

# Function to perform the pTV-based reweighting for a given sample and pTV range
def fill_hist(outHistFile, myTree, hist, min_pTV, max_pTV, function, n_leptons, var):
  for entry in myTree:
  
    # Fill the dictionary of variables to be reweighted and saved to the output file
    entry_dict = {
    "MET": entry.MET,
    "mS_candidate": entry.mS_candidate,
    "pTV": None}
    
    # The variable used for pTV is region-dependent
    if n_leptons==0: entry_dict["pTV"] = entry.MET
    if n_leptons==1: entry_dict["pTV"] = entry.pTV
    if n_leptons==2: entry_dict["pTV"] = entry.pTV
    
    # Extract the reweighting function at the given pTV, as long as the pTV is within the specified range
    if entry_dict["pTV"] >= min_pTV and entry_dict["pTV"] <= max_pTV:
      if type=='function':
        reweight = function.Eval(entry_dict["pTV"])
        
        # Avoid extreme wrong weights where the fit function misbehaves
        if entry_dict["pTV"] > cutoff_dict["%s_%dLep"%(sample.split("_")[0], n_leptons)]:
          reweight = 1
    
      # Reweighting is bin-based by default
      else: reweight = function.GetBinContent(function.FindBin(entry_dict["pTV"]))
      
      # Reweight the event, unless the reweighting function is zero
      try: final_weight = entry.total_weight / reweight
      except ZeroDivisionError: final_weight = entry.total_weight
      hist.Fill(entry_dict[var],final_weight)

  # Write out the histogram to the output ROOT file, as long as it's nonempty
  if hist.GetEntries() > 0:
    outHistFile.cd()
    hist.Write()

# Function to run over events in the minitree created by NtupleToHist for the given sample, reweight each event based on its pTV using the input reweighting function (nominally bin-based reweighting), and create a reweighted histogram with the variables stored in the minitree
def reweight_hist(function, sample, category, n_leptons, ptv_var, type='bin_based'):
  
  print("--------> Reweighting category %s"%category)
  
  # Make sure we're getting the MGPy8 sample
  sample += "_MGPy"
  
  # Create the hist to write to
  outHistFile = ROOT.TFile.Open("hadded_hists/output_%dLep_%s_%s_reweighted.root"%(n_leptons, category, sample) ,"RECREATE")
  
  f_minitree = ROOT.TFile("hadded_hists/output_%dLep_%s_%s_tree.root"%(n_leptons, category, sample))
  myTree = f_minitree.Get("minitree")

  infile, base_filename = get_infile("hadded_hists", sample, category, n_leptons, "monoSWW")
  
  # Check if there was an issue opening the file, and if so return all 0's.
  if infile == 0:
    print("Issue opening file %s. Exiting from reweight_hist() with return code 0."%base_filename)
    return 0
  
  lepton_region_label = make_lepton_region_label(n_leptons)
  sample_type, sample_root = get_sample_info(sample, "monoSWW")

  for var in vars_dict[lepton_region_label]:
    
    # The MET_muInv is called pTV in the trees
    if n_leptons==1 and var=="pTV": var_hist = "MET_muInv"
    else: var_hist = var
    
    # Read the binning info from the existing histogram so the reweighted hist can be generated with the same binning
    if not isinstance(infile.Get("hist_%dLep_%s_allptv_%s_%s_Nominal"%(n_leptons, sample_root, lepton_region_label, var_hist)), ROOT.TH1F):
      print("File %s does not contain the nominal histogram. Exiting from reweight_hist() with return code 0."%base_filename)
      return 0
    hist_in = infile.Get("hist_%dLep_%s_allptv_%s_%s_Nominal"%(n_leptons, sample_root, lepton_region_label, var_hist))
    n_bins = hist_in.GetNbinsX()
    x_min = hist_in.GetBinLowEdge(1)
    x_max = hist_in.GetBinLowEdge(n_bins) + hist_in.GetBinWidth(0)

    # Reweight and fill the histogram for each ptv region used in the analysis
    for ptv_region, ptv_range in pTV_regions_dict.items():
      hist = ROOT.TH1F( "hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_region, lepton_region_label, var_hist), "hist_%dLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample_root, ptv_region, lepton_region_label, var_hist), n_bins, x_min, x_max)
  
      fill_hist(outHistFile, myTree, hist, ptv_range[0], ptv_range[1], function, n_leptons, var)

  outHistFile.Close()
