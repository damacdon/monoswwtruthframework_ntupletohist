#!/bin/bash
echo "running the script fetchAndMerge.sh for all the processes, lepton numbers and categories"
categories=( "Resolved" "Intermediate" "Merged" )
processes=( "Wmunu" "Wenu" "Wtaunu" "Znunu" "Zmumu" "Zee")
#processes=( "Znunu" )
fileTypes=("" "_tree")
gens=("MGPy" "Sh")

echo "Example usage: ./hadd_all_no_upload.sh /data/TruthLevel_hists hadded_hists"

INPUT_DIR=$1
OUTPUT_DIR=$2

for i1 in {0..2};
do
  for k3 in "${processes[@]}"
	do
    for j2 in "${categories[@]}"
		do
			echo "source fetchAndMerge.sh $k3 $i1 $j2 /data/ hadded_hists 1"
			source fetchAndMerge.sh $k3 $i1 $j2 $INPUT_DIR $OUTPUT_DIR 1
		done

    for type in "${fileTypes[@]}"
    do
      for gen in "${gens[@]}"
      do
        hadd ${OUTPUT_DIR}/output_${i1}Lep_Inclusive_${k3}_${gen}${type}.root ${OUTPUT_DIR}/output_${i1}Lep_Merged_${k3}_${gen}${type}.root ${OUTPUT_DIR}/output_${i1}Lep_Intermediate_${k3}_${gen}${type}.root ${OUTPUT_DIR}/output_${i1}Lep_Resolved_${k3}_${gen}${type}.root
      done
    done
	done
done
