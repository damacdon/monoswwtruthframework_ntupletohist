input_signals="file_list/inputlist_monoS_signals.txt"
selectedDIDS=()
INDIR=$1
MET_bins=(0 1 2 3)

# Remove any empty trees that will cause hadding issues from the input directory
./removeEmptyTrees.py -d $INPUT_DIR

while IFS= read line || [ -n "$line" ]
do
  first=${line:0:1}
  if [[ $first == "#" ]]; then
    continue
  fi
  arrIN=(${line//./ })
  echo ${arrIN[1]}
  ./hadd_signal.sh $INDIR ${arrIN[1]}
  for MET in ${MET_bins[@]}
  do
    ./compareVariations_monoS.py -d $INDIR -n ${arrIN[1]} -m $MET
  done
done < $input_signals

# Now combine all the yaml file containing scale DR systs for signals into a single yaml file
python combine_all_yml.py
