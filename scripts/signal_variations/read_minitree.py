from argparse import ArgumentParser
from pprint import pprint, pformat
import os
import csv
from math import sqrt, log
import logging
import ROOT
from ROOT import *
ROOT.gROOT.SetBatch(True)

# first we need to lead the weights. there can be computed from e.g. a fit function

functionfile = ROOT.TFile('fitfunc.root')
function = ROOT.TF1()
function = functionfile.Get("func")
print function
# f is the file to be re-weighted: we are re-weighting Madgraph (LO partons) to match Sherpa (NLO parton treatment)
# this has to be a MadGraph file!!
# f = ROOT.TFile("/afs/cern.ch/work/f/fnapolit/private/VHbbNtuple2Hist_monoSWW/run/output_0Lep_Resolved_366017_tree.root")
f = ROOT.TFile("/afs/cern.ch/work/f/fnapolit/private/VHbbNtuple2Hist_monoSWW/run/output_0Lep_Intermediate_366017_tree.root")
myTree = f.Get("minitree")
print myTree

c1_inclusive = TCanvas('c1_inclusive', '', 800,800) 
hist = ROOT.TH1F( 'mS_candidate', 'mS_candidate', 100, 0, 400 )

for entry in myTree:         
     # Now you have acess to the leaves/branches of each entry in the tree, e.g.
     # print  entry.mS_candidate, entry.MET, entry.total_weight
     reweight = function.Eval(entry.MET)
     # avoid extreme wrong weights where the fit function misbehaves
     if entry.MET > 1500 :
     	reweight = 1
     print "reweight", reweight
     final_weight = entry.total_weight * reweight
     hist.Fill(entry.mS_candidate,final_weight)

hist.Draw()
hist.SaveAs("histotest.root")
# c1_inclusive.SaveAs("test.root")
