#samples=("Znunu" "Zjets" "Wjets" "Vjets")
samples=("Zjets" "Wjets")
#samples=("Vjets")
vars=("mS_candidate" "pTV")
#vars=("mS_candidate")
titles=("Dark Higgs Candidate Mass [GeV]" "MET [GeV]")
#titles=("Dark Higgs Candidate Mass [GeV]")
MET_bins=(0 1 2 3)

for SAMPLE in ${samples[@]}
do
  for MET in ${MET_bins[@]}
  do
    for iVAR in ${!vars[*]}
    do
      ./compareVariations_monoS.py -a monoSWW -d hadded_hists -n $SAMPLE -m $MET -v ${vars[$iVAR]} -t "${titles[$iVAR]}" --use_reweighted_mgpy
      ./compareVariations_monoS.py -a monoSWW -d hadded_hists -n $SAMPLE -m $MET -v ${vars[$iVAR]} -t "${titles[$iVAR]}" --use_reweighted_mgpy --logY
    done
  done
done
