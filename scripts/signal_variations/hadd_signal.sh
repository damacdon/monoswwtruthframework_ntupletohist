#!/bin/bash
INPUT_DIR=$1
DSID=$2

types=("" "_tree")
categories=("Resolved" "Intermediate" "Merged")

for TYPE in "${types[@]}"
do
  commandhadd="hadd ${INPUT_DIR}/output_0Lep_Inclusive_${DSID}${TYPE}.root"
  for CATEGORY in "${categories[@]}"
  do
        if [ ! -f "${INPUT_DIR}/output_0Lep_${CATEGORY}_${DSID}${TYPE}.root" ]; then
          echo -e "${INPUT_DIR}/output_0Lep_${CATEGORY}_${DSID}${TYPE}.root \\e[0;41mDOES NOT\\e[0m exist"
          echo "Will not hadd this file"
        else
          commandhadd+=" ${INPUT_DIR}/output_0Lep_${CATEGORY}_${DSID}${TYPE}.root"
        fi
  done
  rm ${INPUT_DIR}/output_0Lep_Inclusive_${DSID}${TYPE}.root
  echo "executing" $commandhadd
  $commandhadd
done