#!/usr/bin/env python
from commonUtils import *
from ROOT import *

# Example usage:
# python plotHist.py -d /data/TruthLevel_hists -a monoSWW -v pTJ1 -t "Leading Jet pT [GeV]" -l 0 -f file_list/inputlist_monoS_Zjets.txt -m 3

def getArgs():
    parser = ArgumentParser()
    parser.add_argument('-d',
                        '--hist_dir',
                        required=True,
                        help='Directory containing the histograms with all variations produced by NtupleToHist.')
    parser.add_argument('-a',
                        '--analysis',
                        required=True,
                        help='monoHbb, monoSbb, or monoSWW')
    parser.add_argument('-v',
                        '--variable',
                        required=True,
                        help='Variable to be plotted')
    parser.add_argument('-t',
                        '--variableTitle',
                        default='',
                        help='Title for variable to be plotted')
    parser.add_argument('-l',
                        '--n_leptons',
                        default='0',
                        help='Number of leptons in the region to run over. Must be one of 0, 1, or 2.')
    parser.add_argument('-f',
                        '--dataset_file',
                        default='',
                        help='File containing the list of filenames to run over, in the format mc16_13TeV:mc16_13TeV.[dsid].[short_filename]. Cannot be used with the --dsid option.')
    parser.add_argument('-i',
                        '--dsid',
                        default='',
                        help='single dsid to run over. Cannot be used with the --datasetFile option.')
    parser.add_argument('-m',
                        '--MetBin',
                        default='3',
                        help='Specify MET bin: 0: 200-300, 1: 300-500, 2: 500-inf, 3: inclusive')
    return parser.parse_args()

# Function to get all the arguments from the output of getArgs() and make sure they're all acceptable
def check_args(args):
  hist_dir = args.hist_dir
  analysis = args.analysis
  variable = args.variable
  variableTitle = args.variableTitle
  if variableTitle == '': print("WARNING: Title variable not specified. X axis label will be left blank!")
  n_leptons = args.n_leptons
  if n_leptons != '0' and n_leptons != '1' and n_leptons != '2':
    print("ERROR: Number of leptons must be 0, 1, or 2.")
    exit()
  dataset_file = args.dataset_file
  dsid = args.dsid
  if dataset_file != '' and dsid != '':
    print("ERROR: Cannot specify both dsid and file containing datasets to run over. Please provide only 1 of the '--dataset_file' and '--dsid' arguments.")
    exit()
  if dataset_file == '' and dsid == '':
    print("ERROR: Must specify one of '--dataset_file' or '--dsid'.")
    exit()

  MetBin = args.MetBin
  if MetBin == '0': ptv_str = '200_300ptv'
  elif MetBin == '1': ptv_str = '300_500ptv'
  elif MetBin == '2': ptv_str = '500ptv'
  elif MetBin == '3': ptv_str = 'allptv'
  else:
    print("ERROR: MetBin argument must be one of: 0, 1, 2, 3.")
    exit()

  return hist_dir, analysis, variable, variableTitle, n_leptons, dataset_file, dsid, ptv_str

# Function to collect the nominal histogram for a given dsid
def collect_nominal_hist(dsid, hist_dir, n_leptons, analysis, region, ptv_str, lepton_region_label, variable):
  sample, generator, short_filename, sample_type = get_sample_info(dsid, analysis)
  print("Sample: %s"%sample)
  print("Short filename: %s"%short_filename)
  
  infile, base_filename = get_infile(hist_dir, dsid, region, n_leptons, analysis)
  
  # Check if there was an issue opening the file, and if so return all 0's.
  if infile == 0: return 0, 0
  
  # Make sure the nominal event hist exists and is of type TH1F
  print("hist_0Lep_%s_%s_%s_%s_Nominal"%(sample, ptv_str, lepton_region_label, variable))
  if not isinstance(infile.Get("hist_%sLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample, ptv_str, lepton_region_label, variable)), ROOT.TH1F):
    print("---------> File %s does not contain the nominal histogram. Skipping."%base_filename)
    return 0, 0
  
  # Collect the nominal event histogram and convert it to a numpy array
  hist_nominal, edges = hist2array(infile.Get("hist_%sLep_%s_%s_%s_%s_Nominal"%(n_leptons, sample, ptv_str, lepton_region_label, variable)), return_edges=True)
  edges = edges[0]

  return hist_nominal, edges

# Function to combine all the nominal histograms
def combine_nominal_hists(dsids, hist_dir, n_leptons, analysis, region, ptv_str, label_str, lepton_region_label, variable):
  
  # Add up the nominal hists
  isInitialized = False
  for dsid in dsids:
    print("Handling dsid %s"%dsid)
    hist_nominal, edges = collect_nominal_hist(dsid, hist_dir, n_leptons, analysis, region, ptv_str, lepton_region_label, variable)
    
    if not isinstance(hist_nominal, int):     # Make sure collect_variations didn't return all zeros
      if not isInitialized:
        hist_nominal_all = hist_nominal
        edges_all = edges
        isInitialized = True
      
      else:
        hist_nominal_all += hist_nominal

  # Check that at least one set of histograms was processed
  if not isInitialized:
    print("---------> No histograms were run over for this region.")
    return 0

  return hist_nominal_all, edges_all

def plot_nominal(region, hist_nominal, bin_edges, n_leptons, label_str, varTitle_str, dataset_file, ptv_str):
  
  # Create the template root histogram
  n_bins = len(bin_edges)-1
  x_low = np.min(bin_edges)
  x_high = np.max(bin_edges)
  

  hist_nominal_root = ROOT.TH1F("nominal_hist", "nominal_hist", n_bins, x_low, x_high)
  _ = array2hist(hist_nominal, hist_nominal_root)   # Convert from numpy array to root hist

  # Set some plotting details
  hist_nominal_root.SetLineColor(kBlack)
  hist_nominal_root.GetYaxis().SetTitleFont(42)
  hist_nominal_root.GetYaxis().SetTitleSize(0.12)
  hist_nominal_root.GetYaxis().SetTitleOffset(1.72)
  hist_nominal_root.GetXaxis().SetTitle(varTitle_str)

  # Set up the canvas
  c, pad = setup_canvas("c_pdf", region)
  pad.cd()

  # Draw the nominal histogram and save the canvas
  hist_nominal_root.Draw("hist")
  legend = setup_legend(0.65, 0.80, 0.89, 0.87)
  legend.AddEntry(hist_nominal_root,"nominal","l")
  legend.Draw()
  
  # Add some info text to the plot
  t = make_plot_text(region, n_leptons, dataset_file, ptv_str, 0.04, 0.15, 0.6, 0.5, 0.7)
  t.Draw()
  
  c.Update()
  c.SaveAs("./all_plots/nominal_hist_%s_%s_%sLep.pdf"%(label_str, region, n_leptons))


def main():
  regions = ["Merged", "Intermediate", "Resolved"]
  draw_plots = True
  draw_plots_dsid = False
  args = getArgs()
  hist_dir, analysis, variable, variableTitle, n_leptons, dataset_file, dsid, ptv_str = check_args(args)

  # Make the lepton region label for reading hists from the root files                                                                                                               
  lepton_region_label = make_lepton_region_label(n_leptons)

  # Make the label for output files                                                                                                                                                  
  if dataset_file != '':
    dsids = get_dsids(dataset_file)
    label = make_output_label(dataset_file, ptv_str)
  else:
    dsids = [dsid]
    label = "%s_%s"%(dsid, ptv_str)

  label += "_%s"%variable

  # Loop over all the analysis regions
  for region in regions:
    print("Handling region %s"%region)
    
    # Collect all the nominal his
    hist_nominal_all, edges = combine_nominal_hists(dsids, hist_dir, n_leptons, analysis, region, ptv_str, label, lepton_region_label, variable)

    plot_nominal(region, hist_nominal_all, edges, n_leptons, label, variableTitle, dataset_file, ptv_str)
    print("\n\n\n")

if __name__ == '__main__':
    main()
