# Date: 200110
# Purpose: Print the cutflow for an input histogram root file of the format produced by Ntuple2Hist
# Usage: python PrintCutflow.py [path to input histogram]

import ROOT
from ROOT import *
import sys

filename = sys.argv[1]
infile=ROOT.TFile.Open(filename)
dir = infile.Get("Cutflow")
cutflow_FullWeight = dir.Get("cutflow_FullWeight")
cutflow_Unweighted = dir.Get("cutflow_Unweighted")

nCuts = cutflow_Unweighted.GetNbinsX()

print("\n\nPrinting cutflow for input file %s"%filename)
passed_MET_Sig=False
for iCut in range(1, nCuts+1):
  label = cutflow_Unweighted.GetXaxis().GetBinLabel(iCut)
  nEvents = cutflow_FullWeight.GetBinContent(iCut)
  if label=='MET_Sig > 15' or label=='MET Significance':
    nEvents_MET_Sig = nEvents
    passed_MET_Sig = True

  if label!='':
    if passed_MET_Sig:
      print("%s: %d (%.2f%%)"%(label, nEvents, 100*nEvents/nEvents_MET_Sig))
    else: print("%s: %d"%(label, nEvents))


print("\n\n")
