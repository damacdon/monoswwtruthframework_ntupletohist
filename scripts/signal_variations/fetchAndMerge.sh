#!/bin/bash
echo "Example usage: source fetchAndMerge.sh Wmunu 0 Merged /eos/user/d/damacdon/TruthLevel_Hists_190902  /afs/cern.ch/work/f/fnapolit/private/output 1"
echo "Finding DIDS for $1"
echo "#Leptons selected: $2"
echo "Category selected: $3"
echo "input folder: $4"
echo "output folder: $5"
echo "hadding (yes = 1): $6"

for type in "${fileTypes[@]}"
do
  inputSh="file_list/inputlist_monoS_Vjet_Sh.txt"
  selectedDIDS=()
  fileTypes=("" "_tree")
  stop="1"
  while IFS= read line || [ -n "$line" ]
  do
    first=${line:0:1}
    # echo $first
    if [[ $first == "#" ]]; then
    continue
    fi
    if [[ "$line" != *"$1"* ]]; then
    continue
    fi
    # echo "$line"
    arrIN=(${line//./ })
    # echo ${arrIN[1]}
    selectedDIDS+=(${arrIN[1]})
  done < "$inputSh"

  commandhadd="hadd $5/output_$2Lep_$3_$1_Sh${type}.root"

  for i in "${selectedDIDS[@]}"
  do

    if [ ! -f "$4/output_$2Lep_$3_$i${type}.root" ]; then
        echo -e "$4/output_$2Lep_$3_$i${type}.root \\e[0;41mDOES NOT\\e[0m exist"
        echo "Will not hadd this file"
    else
      # echo $i " "
      commandhadd+=" $4/output_$2Lep_$3_$i${type}.root"
    fi
  done

  echo "executing" $commandhadd
  echo $6 stop
  if [[ $6 == "1" && $stop == "1" ]]; then
    echo "hadding"
    $commandhadd
  fi

  inputMGPy="file_list/inputlist_monoS_Vjet_MGPy.txt"
  selectedDIDS=()
  while IFS= read line || [ -n "$line" ]
  do
    first=${line:0:1}
    # echo $first
    if [[ $first == "#" ]]; then
    continue
    fi
    if [[ "$line" != *"$1"* ]]; then
    continue
    fi
    # echo "$line"
    arrIN=(${line//./ })
    # echo ${arrIN[1]}
    selectedDIDS+=(${arrIN[1]})
  done < "$inputMGPy"

  commandhadd="hadd $5/output_$2Lep_$3_$1_MGPy${type}.root"

  for i in "${selectedDIDS[@]}"
  do

    if [ ! -f "$4/output_$2Lep_$3_$i${type}.root" ]; then
        echo -e "$4/output_$2Lep_$3_$i${type}.root \\e[0;41mDOES NOT\\e[0m exist"
        echo "Will not hadd this file"
    else
      # echo $i " "
      commandhadd+=" $4/output_$2Lep_$3_$i${type}.root"
    fi
  done

  echo "executing" $commandhadd
  if [[ $6 == "1" && $stop == "1" ]]; then
    $commandhadd
  fi

done

