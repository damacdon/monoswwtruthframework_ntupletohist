"""
Date: 200305
Purpose: Combine all yaml files produced by compareVariations_monoS.py with scale DR systematics for signal.
"""

import yaml
import sys
import os

signal = sys.argv[1]

all_syst_dicts = {}

found_signal = False
for filename in os.listdir("output_files"):
  if signal in filename and ".yml" in filename:
    found_signal = True
    with open("output_files/%s"%filename) as infile:
      syst_dict = yaml.safe_load(infile)
      for key, value in syst_dict.items():
        if not key in all_syst_dicts: all_syst_dicts[key] = {}
        all_syst_dicts[key].update(value)

if found_signal==False:
  print("Error: couldn't find any yaml files for signal %s"%signal)

all_syst_dicts["recastsignal"] = all_syst_dicts.pop(signal)
with open("output_files/%s_recast_systs.yml"%signal, 'w') as outfile:
  yaml.dump(all_syst_dicts, outfile, default_flow_style=False)

