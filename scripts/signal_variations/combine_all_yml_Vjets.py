"""
Date: 200305
Purpose: Combine all yaml files produced by compareVariations_monoS.py with scale DR systematics for signal.
"""

import yaml
import sys
import os

all_syst_dicts = {}

for filename in os.listdir("output_files"):
  if "jets" in filename and "ptv.yml" in filename:# and "singleratio" in filename:
    with open("output_files/%s"%filename) as infile:
      syst_dict = yaml.safe_load(infile)
      for key, value in syst_dict.items():
        if not key in all_syst_dicts: all_syst_dicts[key] = {}
        all_syst_dicts[key].update(value)

#with open("output_files/Vjets_systs_singleratio.yml", 'w') as outfile:
with open("output_files/Vjets_systs.yml", 'w') as outfile:
  yaml.dump(all_syst_dicts, outfile, default_flow_style=False)

