#!/usr/bin/env python
#
# Script to delete any trees in the given directory that do not contain the mS_candidate branch (i.e. are empty)
#
# Example usage:
# To remove all empty trees in the directory /data/TruthLevel_hists:
# >> ./removeEmptyTrees.py -d /data/TruthLevel_hists

import ROOT
from ROOT import *
import glob
from argparse import ArgumentParser
import os

def getArgs():
  parser = ArgumentParser()
  parser.add_argument('-d',
                      '--hists_dir',
                      required=True,
                      help='Name of the directory containing the ttree root files to check')
  return parser.parse_args()


def main():

  args = getArgs()
  hists_dir = args.hists_dir

  for filepath in glob.iglob("%s/*_tree.root"%(hists_dir)):
    path, base_filename = os.path.split(filepath)
    infile = ROOT.TFile.Open(filepath)
    tree = infile.Get("minitree")
    try:
      branch = tree.mS_candidate
    except AttributeError:
      print("File %s has no minitree entries. Deleting."%base_filename)
      os.remove(filepath)


if __name__ == '__main__':
  main()




