#!/bin/bash
# Script to combine V+jets hists and minitrees in the input directory into combined Z+jets and W+jets samples (Eg. Zjets, Wjets, Zee, Wenu, etc.) in the output directory

echo "Example usage: ./combine_Vjets.sh /data/TruthLevel_hists hadded_hists"

INPUT_DIR=$1
OUTPUT_DIR=$2

# Remove any empty trees that will cause hadding issues from the input directory
./removeEmptyTrees.py -d $INPUT_DIR

# Create combined final state samples, eg. Zee, Wenu from input ntuples and minitrees located in INPUT_DIR
./hadd_all_no_upload.sh $INPUT_DIR $OUTPUT_DIR

# Create combined Z+jets and W+jets samples from final state samples produced above
./combineSamples.sh $OUTPUT_DIR $OUTPUT_DIR
