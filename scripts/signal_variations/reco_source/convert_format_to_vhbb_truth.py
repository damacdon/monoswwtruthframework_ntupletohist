#!/usr/bin/env python2.7
# Python script to convert the format of the mS_candidate info in the combined reco-level histos into something that can be readily consumed by compareVariations_monoS.py
# This can only be run after previously running combine_samples.sh
# Assume that the directories 'SR', 'CR1', and 'CR2' containing the original reco-level samples are located a at the same level as this script.

# Usage:
#     >> ./combine_samples.sh
#     >> ./convert_format_to_vhbb_truth.py

import ROOT

Regions = ["SR", "CR1", "CR2"]
Regions_truth = ["SR", "CR1L", "CR2L"]
Categories = ["Intermediate", "Resolved"]
mS_cand_vars = ["cand_scalar_tarcomb_m", "cand_scalar_wwresolved_m"]
Gens = ["Sh", "MGPy"]
Bkgs = ["Zjets", "Wjets"]
Bkg_root_info = ["Zall", "Wall"]
ptv_regions_reco = ["", "_200_300", "_300_500", "_500"]
ptv_regions_truth = ["allptv", "200_300ptv", "300_500ptv", "500ptv"]


for iRegion in range(len(Regions)):
  for gen in Gens:
    for iBkg in range(len(Bkgs)):
      infile = ROOT.TFile.Open("%s/%s_%s.root"%(Regions[iRegion], Bkgs[iBkg], gen))

      for iCategory in range(len(Categories)):
        outHistFile = ROOT.TFile.Open("output_%dLep_%s_%s_%s.root"%(iRegion, Categories[iCategory], Bkgs[iBkg], gen) ,"RECREATE")
        outHistFile.cd()
        for ipTV in range(len(ptv_regions_reco)):
          mS_candidate_hist = infile.Get("MonoS_Nominal/%s_%s%s/%s"%(Regions[iRegion], Categories[iCategory], ptv_regions_reco[ipTV], mS_cand_vars[iCategory]))
          name = "hist_%dLep_%s_%s_%s_mS_candidate_Nominal"%(iRegion, Bkg_root_info[iBkg], ptv_regions_truth[ipTV], Regions_truth[iRegion])
          mS_candidate_hist.SetNameTitle(name, name)
          mS_candidate_hist.Write()
      outHistFile.Close()
