#!/bin/bash
# Script to add '_reweighted.root' to the end of all reco-level hists that have been fully converted to a format readable by compareVariations_monoS.py. This makes it seamless for the hists to be read in regardless of whether the reweighted or unreweighted versions of the other truth-level hists are being used.
# Can only be run after copying the reformatted reco-level hists produced by combine_samples.sh and convert_format_to_vhbb_truth.py into a sub-dir 'RecoLevel' located inside the directory (eg. 'hadded_hists') containing all the truth-level hists.

# Example usage:
#     >> ./combine_samples.sh
#     >> ./convert_format_to_vhbb_truth.py
#     >> mkdir -p ../hadded_hists/RecoLevel
#     >> cp *.root ../hadded_hists/RecoLevel
#     >> ./copy_to_reweighted.sh ../hadded_hists/RecoLevel

if [ $# -gt 0 ]
then
  Reco_hist_dir=$1
else
  Reco_hist_dir=../hadded_hists/RecoLevel
fi

rm ${Reco_hist_dir}/*_reweighted.root

for file in ${Reco_hist_dir}/*.root
do
  filename_noext="${file%.*}"
  echo $filename_noext
  filename_reweighted=${filename_noext}_reweighted.root
  cp $file $filename_reweighted
done
