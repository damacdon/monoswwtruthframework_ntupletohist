#!/bin/bash
# Bash script to combine reco-level V+jet sub-samples with nominal and alternative generators provided by Philipp into Z+jets and W+jets samples
# Assume that the directories 'SR', 'CR1', and 'CR2' containing the original reco-level samples are located a at the same level as this script.

# Usage:
#     >>  ./combine_samples.sh

regions=(SR CR1 CR2)
Zjets_nom=(Zee_cl Zee_hf Zee_hpt Zee_l Zmumu_cl Zmumu_hf Zmumu_hpt Zmumu_l Znunu_cl Znunu_hf Znunu_hpt Znunu_l Ztautau_cl Ztautau_hf Ztautau_hpt Ztautau_l)
Wjets_nom=(Wenu_cl Wenu_hf Wenu_hpt Wenu_l Wmunu_cl Wmunu_hf Wmunu_hpt Wmunu_l Wtaunu_cl Wtaunu_hf Wtaunu_hpt Wtaunu_l)

Zjets_alt=(Zee Zmumu Znunu Ztautau)
Wjets_alt=(Wenu Wtaunu Wmunu)

# Combine the nominal samples
for REGION in "${regions[@]}"
do
  # Combine the nominal Z+jets and W+jets samples
  hadd_str_Zjets=""
  hadd_str_Wjets=""
  for SAMPLE in "${Zjets_nom[@]}"
  do
    hadd_str_Zjets="${hadd_str_Zjets} ${REGION}/nominal/${SAMPLE}.root"
  done
  hadd -f ${REGION}/Zjets_Sh.root ${hadd_str_Zjets}

  for SAMPLE in "${Wjets_nom[@]}"
  do
    hadd_str_Wjets="${hadd_str_Wjets} ${REGION}/nominal/${SAMPLE}.root"
  done
  hadd -f ${REGION}/Wjets_Sh.root ${hadd_str_Wjets}

  # Combine the mgpy8 Z+jets and W+jets samples
  hadd_str_Zjets=""
  hadd_str_Wjets=""
  for SAMPLE in "${Zjets_alt[@]}"
  do
    hadd_str_Zjets="${hadd_str_Zjets} ${REGION}/alternative/${SAMPLE}-sysMGPy8.root"
  done
  hadd -f ${REGION}/Zjets_MGPy.root ${hadd_str_Zjets}

  for SAMPLE in "${Wjets_alt[@]}"
  do
    hadd_str_Wjets="${hadd_str_Wjets} ${REGION}/alternative/${SAMPLE}-sysMGPy8.root"
  done
  hadd -f ${REGION}/Wjets_MGPy.root ${hadd_str_Wjets}
done
