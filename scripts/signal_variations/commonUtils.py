# Header module with functions common to other scripts

from argparse import ArgumentParser
import os
from math import sqrt, log
import ROOT
from ROOT import *
from root_numpy import hist2array, array2hist, fill_graph
import numpy as np
ROOT.gROOT.SetBatch(True)

# Setup the ATLAS plot style
ROOT.gStyle.SetOptStat(0)
gROOT.LoadMacro("AtlasStyle.C")
SetAtlasStyle()

# Function to determine whether a sample is signal or background
def get_sample_type(sample, analysis):
  if sample in ["ttbar", "qqWlvH125", "stopWt", "stopt", "stops", "Z", "W"]: sample_type = "background"
  else: sample_type = "signal"
  
  return sample_type

# Get all dsids to run over from the input dataset file
def get_dsids(dataset_file):
  f_in = open(dataset_file)
  
  dsids = []
  for line in f_in:
    words = line.split(".")
    if len(words) > 1:
      dsids.append(words[1])
  return dsids


def get_sample_name(dsid):
  # Open SampleInformation_13TeV.txt
  sample_info = open("../../data/SampleInformation_13TeV.txt")
  
  # Find the line beginning with the given dsid
  for line in sample_info:
    words = line.split()
    if len(words) > 0 and words[0]==dsid:
      sample_name = words[1]

  return sample_name

# Function to get the base name of the dataset file for naming the plot files
def make_output_label(sample_name, ptv_str):
  
  # Assume the sample is a signal if the sample name is a 6-digit integer
  if sample_name.isdigit() and len(sample_name) == 6: label = get_sample_name(sample_name)
  else: label = sample_name
  label += "_%s"%ptv_str

  return label

# Function to reformat the ptv string to match pgadow's formatting for the bkg samples
def reformat_ptv_str(ptv_str):
  bin = ptv_str.replace('ptv', '')
  return "met%s"%bin

# Function to determine the sample type based on the name of the sample
def get_sample_info(sample_name, analysis):

  # If the label is a 6-digit integer (presumably dsid), assume it's a signal. Otherwise, assume it's a background
  if sample_name.isdigit() and len(sample_name) == 6: sample_type = "signal"
  else: sample_type = "background"

#   Get the sample name for reading the histograms from the root file
  if sample_name.startswith('Z'): sample_root = 'Z'
  elif sample_name.startswith('W'): sample_root = 'W'
  elif sample_type == "signal": sample_root = get_sample_name(sample_name)
  else: sample_root = sample_name
  if sample_type == "background" and analysis == "monoSWW": sample_root += "all"

  return sample_type, sample_root

# Function to produce the colours to be used for the pdfs
def make_hist_colors():
  # Basic colours
  colors = [kRed, kBlue, kGreen, kYellow, kViolet, kCyan]
  colors_pdf = colors
  
  # Additional colours
  more_colors = [kMagenta, kBlue, kGreen, kYellow, kPink, kOrange, kViolet, kTeal, kBlack, kAzure, kRed]
  
  # Add in some shade variations on the additional colours
  for iRepeat in [1, 2, 3]:
    for color in more_colors:
      for i in range(1,5):
        colors_pdf.append(color+i)

  return colors_pdf

# Function to get the index corresponding to the given category for the specified analysis
def get_index_category(analysis, category):
  
  # Default -1 to be return if category not matched to any index
  index_category = -1
  
  # MonoSWW
  if analysis == "monoSWW":
    if category == "merged" or category == "Merged": index_category = 0
    elif category == "intermediate" or category == "Intermediate": index_category = 1
    elif category == "resolved" or category == "Resolved": index_category = 2
    elif category == "inclusive" or category == "Inclusive": index_category = 3
    else: print("ERROR: Category %s is not recognized for %s analysis - returning -1. Acceptable regions: merged, Merged, intermediate, Intermediate, resolved, Resolved, inclusive, Inclusive."%(category, analysis))
  
  # FIXME: MonoSbb and MonoHbb not yet implemented
  else:
    print("Category indices not yet set for monoSbb and monoHbb analyses :( Returning -1.")

  return index_category

# Set up basic canvas
def setup_canvas(canvas_name, category):
  # Make sure the canvas name is a string
  if not isinstance(canvas_name, str):
    print("ERROR: argument to setup_canvas_ratio() must be a string! Returning all 0's.")
    return 0, 0, 0
  
  # Create the main canvas
  c = TCanvas(canvas_name+str(category), '', 800,600)

  # Set up the pad
  pad = c.cd()
  #pad.SetLogy()
  pad.SetPad(0.0,0.0,1.0,1.0)
  pad.SetBorderMode(0)
  pad.SetBorderSize(2)
  pad.SetTickx(1)
  pad.SetTicky(1)
  pad.SetLeftMargin(0.13)
  pad.SetRightMargin(0.075)
  pad.SetTopMargin(0.07)
  pad.SetBottomMargin(0.18)
  pad.SetFrameBorderMode(0)

  return c, pad

# Set up canvas for plot with lower panel for ratio
def setup_canvas_ratio(canvas_name, category, logY):
  # Make sure the canvas name is a string
  if not isinstance(canvas_name, str):
    print("ERROR: argument to setup_canvas_ratio() must be a string! Returning all 0's.")
    return 0, 0, 0

  # Create the main canvas
  c = TCanvas(canvas_name+str(category), '', 800,800)

  # Create canvases for the main plot and the ratio underneath
  c.Divide(1,2)

  # Set up the pad for the main plot
  pad1 = c.cd(1)
  if logY: pad1.SetLogy()
  pad1.SetPad(0.0,0.3,1.0,1.0)
  pad1.SetBorderMode(0)
  pad1.SetBorderSize(2)
  pad1.SetTickx(1)
  pad1.SetTicky(1)
  pad1.SetLeftMargin(0.13)
  pad1.SetRightMargin(0.075)
  pad1.SetTopMargin(0.07)
  pad1.SetBottomMargin(0.012)
  pad1.SetFrameBorderMode(0)

  # Set up the pad for the ratio plot underneath the main plot
  pad2 = c.cd(2)
  pad2.SetPad(0.0,0.0,1.0,0.3)
  pad2.SetBorderMode(0)
  pad2.SetFillColor(0)
  pad2.SetBorderSize(2)
  pad2.SetTickx(1)
  pad2.SetTicky(1)
  pad2.SetLeftMargin(0.13)
  pad2.SetRightMargin(0.075)
  pad2.SetTopMargin(0.045)
  pad2.SetBottomMargin(0.4)
  pad2.SetFrameBorderMode(0)

  return c, pad1, pad2

# Setup axes for hist to plot
def setup_axes(hist, ylim=0.4, reweight=False):
  hist.GetYaxis().SetNdivisions(504)
  if reweight: hist.GetYaxis().SetRangeUser(1-ylim, 1+ylim)
  else: hist.GetYaxis().SetRangeUser(-ylim, ylim)
  hist.GetYaxis().SetTitleFont(43)
  hist.GetYaxis().SetLabelFont(42)
  hist.GetYaxis().SetLabelSize(0.12)
  hist.GetYaxis().SetTitleSize(28)
  hist.GetYaxis().SetTickLength(0.035)
  hist.GetYaxis().SetTitleOffset(1.72)
  hist.GetXaxis().SetLabelFont(42)
  hist.GetXaxis().SetLabelSize(0.12)
  hist.GetXaxis().SetTitleSize(28)
  hist.GetXaxis().SetTickLength(0.1)
  hist.GetXaxis().SetTitleOffset(3.5)
  hist.GetXaxis().SetTitleFont(43)
  return hist

# Set up legend for variations
def setup_legend(x_low, y_low, x_high, y_high):
  legend = TLegend(x_low, y_low, x_high, y_high)
  legend.SetFillStyle(0)
  legend.SetLineColor(0)
  legend.SetBorderSize(0)
  return legend

def get_infile(hists_dir, sample_name, category, n_leptons, analysis):
  # MonoSWW specifies the analysis category (Merged, Intermediate, or Resolved) in the filename
  if analysis == "monoSWW": filename = "%s/output_%dLep_%s_%s.root"%(hists_dir, n_leptons, category, sample_name)
  else: filename = "%s/output_%dLep_%s.root"%(hist_dir, n_leptons, sample_name)

  path, base_filename = os.path.split(filename)
  
  # Open the root file if it exists
  if os.path.exists(filename): infile = ROOT.TFile.Open(filename)
  else:
    print("---------> Filename %s does not exist. Skipping."%filename)
    return 0, base_filename
  
  return infile, base_filename

# Function to make a label for the region associated with the given number of leptons
def make_lepton_region_label(n_leptons):
  if n_leptons == 0: region_label = "SR"
  if n_leptons == 1: region_label = "CR1L"
  if n_leptons == 2: region_label = "CR2L"

  return region_label

# Function to make some written info about the plot to add to the canvas
def make_plot_text(category, n_leptons, sample_name, ptv_str, fontsize, xlow, ylow, xhigh, yhigh):
  lepton_region_label = make_lepton_region_label(n_leptons)

  # Set the ptv variable for the given number of leptons
  if n_leptons == 0: ptv_type_str = "MET"
  if n_leptons == 1: ptv_type_str = "MET (no \mu)"
  if n_leptons == 2: ptv_type_str = "(p_{T})_{ll}"
  
  if sample_name.isdigit(): sample_name = "DSID " + sample_name

  # Make a label indicating the ptv region
  if ptv_str == '200_300ptv': ptv_info_str = "200 GeV < %s < 300 GeV"%ptv_type_str
  if ptv_str == '300_500ptv': ptv_info_str = "300 GeV < %s < 500 GeV"%ptv_type_str
  if ptv_str == '500ptv': ptv_info_str = "%s > 500 GeV"%ptv_type_str
  if ptv_str == 'allptv': ptv_info_str = "%s-Inclusive"%ptv_type_str
  
  label_str = "%s (%s %s)%s"%(sample_name, category, lepton_region_label, ptv_info_str)
  line1_str = "%s (%s %s)"%(sample_name, category, lepton_region_label)
  line2_str = ptv_info_str
  
  t = ROOT.TPaveText(xlow, ylow, xhigh, yhigh, "NDC,NB")   # NDC: use normalized coords rather than basing them on the x and y values on the axes
  t.AddText(line1_str)
  t.AddText(line2_str)
  t.SetTextSize(fontsize)
  t.SetFillStyle(3000)      # Transparent fill

  return t

