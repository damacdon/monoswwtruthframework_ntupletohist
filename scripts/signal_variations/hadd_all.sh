#!/bin/bash
echo "running the script fetchAndMerge.sh for all the processes, lepton numbers and categories"
categories=( "Resolved" "Intermediate" "Merged" )
# categories=( "Resolved" )
processes=( "Wmunu" "Wenu" "Wtaunu" "Znunu" "Zmumu" "Zee")
# processes=( "Znunu" )
for i1 in {0..2};
do
	for j2 in "${categories[@]}"
	do
		for k3 in "${processes[@]}"
		do
			
			# the files can be optionally produced at the same time
			# echo "source fetchAndMerge.sh $k3 $i1 $j2 /eos/user/d/damacdon/TruthLevel_Hists_190902  /afs/cern.ch/work/f/fnapolit/private/output 1"
			# source fetchAndMerge.sh $k3 $i1 $j2 /eos/user/d/damacdon/TruthLevel_Hists_190905  /afs/cern.ch/work/f/fnapolit/private/output4 1

			# continue
			
			echo "output_${i1}Lep_${j2}_${k3}_Sh.root"
			if [ ! -f "/afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_Sh.root" ]; then
			    echo -e "/afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_Sh.root \\e[0;41mDOES NOT\\e[0m exist"
			    echo "Will not do anything"
				sed -i -e "s/.*${k3}_${j2}_${i1}L.*/<div class=\"notpresent ${k3}_${j2}_${i1}L\">  <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\">link<\/a> <\/div>/" /eos/user/f/fnapolit/www/TruthModelling/example.html
			    stop="0"
			elif [ ! -f "/afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_MGPy.root" ]; then
			    echo -e "/afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_MGPy.root \\e[0;41mDOES NOT\\e[0m exist"
			    echo "Will not do anything"
				sed -i -e "s/.*${k3}_${j2}_${i1}L.*/<div class=\"notpresent ${k3}_${j2}_${i1}L\">  <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\">link<\/a> <\/div>/" /eos/user/f/fnapolit/www/TruthModelling/example.html

			    stop="0"
			else
				for w4 in {0..3};
				do
					# python 2pointVariation.py -x /afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_MGPy.root -y /afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_Sh.root -m ${w4} -a monoSWW -v pTV
					python 2pointVariation.py -x /afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_MGPy.root -y /afs/cern.ch/work/f/fnapolit/private/output4/output_${i1}Lep_${j2}_${k3}_Sh.root -m ${w4} -a monoSWW 
					
					# <div class="box Wmunu_Resolved_0L">  <a href="https://www.cern.ch">link</a>  </div>

				done

				if [ "$?" != "0" ]; then
					echo "something went wrong"
					sed -i -e "s/.*${k3}_${j2}_${i1}L.*/<div class=\"notpresent ${k3}_${j2}_${i1}L\">  <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\">link<\/a> <\/div>/" /eos/user/f/fnapolit/www/TruthModelling/example.html
				else
					sed -i -e "s/.*${k3}_${j2}_${i1}L.*/<div class=\"box ${k3}_${j2}_${i1}L\">  <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\/2\_point\_comparison${i1}Lep\_${j2}\_${k3}\_200_300ptv.pdf\">METBIN 1 <\/a> <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\/2\_point\_comparison${i1}Lep\_${j2}\_${k3}\_300_500ptv.pdf\">METBIN 2 <\/a> <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\/2\_point\_comparison${i1}Lep\_${j2}\_${k3}\_500ptv.pdf\">METBIN 3 <\/a> <a href=\"https:\/\/fnapolit.web.cern.ch\/fnapolit\/TruthModelling\/eos_plots\/2\_point\_comparison${i1}Lep\_${j2}\_${k3}\_allptv.pdf\">INCLUSIVE<\/a> <\/div>/" /eos/user/f/fnapolit/www/TruthModelling/example.html

				fi

			fi
		done
	done
done
cp /eos/user/f/fnapolit/www/TruthModelling/all_plots/2_point*.pdf /eos/user/f/fnapolit/www/TruthModelling/eos_plots
