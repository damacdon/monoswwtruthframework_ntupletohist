#!/usr/bin/python

# this script is to make the 2 point variation: Acceptance double ratio MadGraphPy8 vs Sherpa

# Example usage:
# python acceptanceDoubleRatio.py -x ../../../../../output_DMM/Wmunu/output_1Lep_Merged_all_MGPy8.root -y ../../../../../output_DMM/Wmunu/output_1Lep_Merged_all_Sh.root -z ../../../../../output_DMM/Wmunu_Intermediate/output_1Lep_Intermediate_all_MGPy8.root -w ../../../../../output_DMM/Wmunu_Intermediate/output_1Lep_Intermediate_all_Sh.root

from argparse import ArgumentParser
from pprint import pprint, pformat
import os
import csv
from math import sqrt, log
import logging
import ROOT
from ROOT import *
ROOT.gROOT.SetBatch(True)

def getArgs():
    parser = ArgumentParser()
    parser.add_argument('-x',
                        '--inputFileMadGraphPy8_A',
                        default='',
                        help='output file of the NtupleToHist code containing the nominal histos with MadGraphPy8. Region A')
    parser.add_argument('-y',
                        '--inputFileSherpa_A',
                        default='',
                        help='output file of the NtupleToHist code containing the nominal histos with Sherpa. Region A')
    parser.add_argument('-z',
                        '--inputFileMadGraphPy8_B',
                        default='',
                        help='output file of the NtupleToHist code containing the nominal histos with MadGraphPy8. Region B')
    parser.add_argument('-w',
                        '--inputFileSherpa_B',
                        default='',
                        help='output file of the NtupleToHist code containing the nominal histos with Sherpa. Region B')
    parser.add_argument('-a',
                        '--analysis',
                        default='',
                        help='monoHbb, monoSbb, or monoSWW')
    return parser.parse_args()

# Function to extract the dataset ID from the filename
def get_dsid(filename, analysis):
  if(analysis=="monoSWW"): dsid = filename.split('_')[3]      # MonoSWW additionally specifies the recycling strategy region before the dsid
  else: dsid = filename.split('_')[2]
  return dsid

# Function to get to the line corresponding to a given dsid in the file SampleInformation_13TeV.txt containing the info for each sample
def get_sample_info(inFile, dsid):

  # Open SampleInformation_13TeV.txt
  sample_info = open("../../data/SampleInformation_13TeV.txt")
  if dsid == "all":
    return "","",""
  # Find the line beginning with the given dsid
  for line in sample_info:

    words = line.replace("\t"," ").split(" ")
    if words[0]==dsid:
      sample_name = words[1]
      generator = words[2]
      if len(words) == 4 : 
          short_filename = words[3]
      else:
          short_filename = ""
      return sample_name, generator, short_filename

  # Print an error if no line was found corresponding to the dsid
  print("ERROR! No line found in SampleInformation_13TeV.txt for DSID %s"%dsid)
  return "","",""

  exit()

# Get the pdf variations for either signal or background
def get_pdf_vars(analysis, type):
  pdf_variations = []
  
  if type == "signal":
    for i in range(1,101):
      if (analysis=="monoSbb" or analysis=="monoSWW"):
        pdf_variations.append("PDF=263000MemberID="+str(i))
      elif (analysis=="monoHbb"):
        pdf_variations.append("PDF=260000MemberID="+str(i))
      else:
        print "ERROR: wrong analysis name!!"

  elif type == "background":
    print("PDF variations not yet implemented for background :(")

  else:
    print("ERROR! sample type must be either signal or background")

  return pdf_variations

def resolve_names(filename):
    # first the analysis region
    analysisRegion = ""
    leptonnumber = ""
    if "0Lep" in filename:
        analysisRegion = "SR"
        leptonnumber = "0"
    elif "1Lep" in filename:
        analysisRegion = "CR1L"
        leptonnumber = "1"
    elif "2Lep" in filename:
        analysisRegion = "CR2L"
        leptonnumber = "2"
    # then the category 
    category = ""

    category = ""
    if "Resolved" in filename:
        category = "Resolved"
    elif "Intermediate" in filename:
        category = "Intermediate"
    elif "Merged" in filename:
        category = "Merged"
    # then the category 
    return analysisRegion, category, leptonnumber



def DrawVariations(inFileX, inFileY,inFileZ,inFileW, analysis):
    region="merged"
    index_region=0
    
    pathX, filenameX = os.path.split(inFileX)
    print "filenameX: ", filenameX
    
    pathY, filenameY = os.path.split(inFileY)
    print "filenameY: ", filenameY
    
    pathZ, filenameZ = os.path.split(inFileZ)
    print "filenameZ: ", filenameZ
    
    pathW, filenameW = os.path.split(inFileW)
    print "filenameW: ", filenameW
    
    analysisRegionX, categoryX, leptonnumberX = resolve_names(filenameX)
    analysisRegionY, categoryY, leptonnumberY = resolve_names(filenameY)
    analysisRegionZ, categoryZ, leptonnumberZ = resolve_names(filenameZ)
    analysisRegionW, categoryW, leptonnumberW = resolve_names(filenameW)

    if analysisRegionX != analysisRegionY:
        print "WARNING comparing two analysis Regions. "

    if categoryX != categoryY:
        print "WARNING comparing two categories. "

    if analysisRegionZ != analysisRegionW:
        print "WARNING comparing two analysis Regions. "

    if categoryZ != categoryW:
        print "WARNING comparing two categories. "

    nameX, extensionX = filenameX.split('.')
    dsidX = get_dsid(nameX, analysis)
    print "DSID X: ", dsidX
    
    nameY, extensionY = filenameY.split('.')
    dsidY = get_dsid(nameY, analysis)
    print "DSID Y: ", dsidY
    
    nameZ, extensionZ = filenameZ.split('.')
    dsidZ = get_dsid(nameZ, analysis)
    print "DSID Z: ", dsidZ
    
    nameW, extensionW = filenameW.split('.')
    dsidW = get_dsid(nameW, analysis)
    print "DSID W: ", dsidW
    


    sampleX, generatorX, short_filenameX = get_sample_info(inFileX, dsidX)
    print("Sample: %s"%sampleX)
    
    sampleY, generatorY, short_filenameY = get_sample_info(inFileY, dsidY)
    print("Sample: %s"%sampleY)
   
    sampleZ, generatorZ, short_filenameZ = get_sample_info(inFileZ, dsidZ)
    print("Sample: %s"%sampleZ)
    
    sampleW, generatorW, short_filenameW = get_sample_info(inFileW, dsidW)
    print("Sample: %s"%sampleW)
     

    ROOT.gStyle.SetOptStat(0)
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    
    colors = [kRed, kBlue, kGreen, kYellow, kViolet, kCyan]
    more_colors = [kMagenta, kBlue, kGreen, kYellow, kPink, kOrange, kViolet, kTeal, kBlack, kAzure]
    colors_pdf = []

    for color in more_colors:
        for i  in range(1,11):
            colors_pdf.append(color+i)

    infileX = ROOT.TFile.Open(inFileX)
    infileY = ROOT.TFile.Open(inFileY)
    infileZ = ROOT.TFile.Open(inFileZ)
    infileW = ROOT.TFile.Open(inFileW)
    print infileX
    print infileY
    print infileZ
    print infileW
    infileX.ls()
    # name can be changed
    outfile_acceptance = open("./output_files/acceptance_2point_uncertainties_"+str(dsidX)+"_"+str(dsidY)+".txt","w")

    # hist_nominal = infile.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(region)+"_mBB_"+str(label_region)+"_allcut_Nominal")  # Needs to be adapted
    histonameX = "hist_"+leptonnumberX+"Lep_Zall_300_500ptv_"+analysisRegionX+"_mS_candidate_Nominal"
    histonameY = "hist_"+leptonnumberY+"Lep_Zall_300_500ptv_"+analysisRegionY+"_mS_candidate_Nominal"
    histonameZ = "hist_"+leptonnumberZ+"Lep_Zall_300_500ptv_"+analysisRegionZ+"_mS_candidate_Nominal"
    histonameW = "hist_"+leptonnumberW+"Lep_Zall_300_500ptv_"+analysisRegionW+"_mS_candidate_Nominal"
    print histonameX
    print histonameY
    print histonameZ
    print histonameW
    histX = infileX.Get(histonameX)  # Needs to be adapted
    histY = infileY.Get(histonameY)  # Needs to be adapted
    histZ = infileZ.Get(histonameZ)  # Needs to be adapted
    histW = infileW.Get(histonameW)  # Needs to be adapted

    print histX
    print histY
    print histZ
    print histW

    N_X = histX.Integral()
    N_Y = histY.Integral()
    N_Z = histZ.Integral()
    N_W = histW.Integral()

    # X = ( N_Z / N_X ) /( N_W / N_Y )
    X = ( N_X / N_Y ) /( N_Z / N_W )

    print  N_X / N_Y, N_Z / N_W 
    print X

    outfile_acceptance.write("2 "+str(index_region)+" "+str(X)+"\n")    


def DrawVariations_InclusiveMET(inFile,sample,analysis):

    path, filename = os.path.split(inFile)
    print "filename: ", filename
    name, extension = filename.split('.')
    output, lepton, dsid = name.split('_')
    print "DSID: ", dsid
    
    ROOT.gStyle.SetOptStat(0)
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    
    variations = ["MUF=0.5","MUF=2.0","MUR=0.5","MUR=0.5MUF=0.5","MUR=2.0","MUR=2.0MUF=2.0"]
    variations_names = ["#mu_{F}=0.5","#mu_{F}=2.0","#mu_{R}=0.5","#mu_{R}=0.5#mu_{F}=0.5","#mu_{R}=2.0","#mu_{R}=2.0#mu_{F}=2.0"]
        
    colors = [kRed, kBlue, kGreen, kYellow, kViolet, kCyan]
    
    print " ******** INCLUSIVE MET PLOTS ******** "
    
    hist_nominal_inclusive = ROOT.TH1F()
    hist_nominal_inclusive.SetLineColor(kBlack)
    hist_nominal_inclusive = Merge_MET_bins(inFile,"Nominal",sample)
    # merge scale variations
    print " ****** SCALE ****** "
    c1_inclusive = TCanvas('c1_inclusive', '', 800,800) 
    c1_inclusive.Divide(1,2)
    pad1_inclusive = c1_inclusive.cd(1)
    pad1_inclusive.SetLogy()
    pad1_inclusive.SetPad(0.0,0.3,1.0,1.0)
    pad1_inclusive.SetBorderMode(0)
    pad1_inclusive.SetBorderSize(2)
    pad1_inclusive.SetTickx(1)
    pad1_inclusive.SetTicky(1)
    pad1_inclusive.SetLeftMargin(0.13)
    pad1_inclusive.SetRightMargin(0.075)
    pad1_inclusive.SetTopMargin(0.07)
    pad1_inclusive.SetBottomMargin(0.012)
    pad1_inclusive.SetFrameBorderMode(0)
    pad1_inclusive.cd()
    hist_nominal_inclusive.GetYaxis().SetTitleFont(42)
    hist_nominal_inclusive.GetYaxis().SetTitleSize(0.12)
    hist_nominal_inclusive.GetYaxis().SetTitleOffset(1.72)
    hist_nominal_inclusive.Draw("hist")
    i=0
    hist_variations_ratio_inclusive = []
    hist_variations_inclusive = []
    legend_inclusive = TLegend(0.60,0.60,0.9,0.9)
    legend_inclusive.SetFillStyle(0)
    legend_inclusive.SetLineColor(0)
    legend_inclusive.SetBorderSize(0)
    legend_inclusive.AddEntry(hist_nominal_inclusive,"nominal","l")
    for scale in variations:
        hist_scale_inclusive = ROOT.TH1F()
        hist_scale_inclusive = Merge_MET_bins(inFile,str(scale),sample)
        hist_variations_inclusive.append(hist_scale_inclusive)
        # for variation "scale" now I have the inclusive MET histogram
        hist_variations_inclusive[i].SetLineColor(colors[i])
        hist_variations_inclusive[i].Draw("histsame")
        legend_inclusive.AddEntry(hist_variations_inclusive[i],variations_names[i],"l")
        hist_clone_inclusive = hist_scale_inclusive.Clone("hist_clone")
        hist_clone_inclusive.Add(hist_nominal_inclusive,-1) #(var-nominal)/nominal = rel. difference
        hist_clone_inclusive.Divide(hist_nominal_inclusive)
        hist_clone_inclusive.SetLineColor(colors[i])
        hist_variations_ratio_inclusive.append(hist_clone_inclusive)
        i += 1
    legend_inclusive.Draw()    
    pad2_inclusive = c1_inclusive.cd(2)
    pad2_inclusive.SetPad(0.0,0.0,1.0,0.3)
    pad2_inclusive.SetBorderMode(0)
    pad2_inclusive.SetFillColor(0)
    pad2_inclusive.SetBorderSize(2)
    pad2_inclusive.SetTickx(1)
    pad2_inclusive.SetTicky(1)
    pad2_inclusive.SetLeftMargin(0.13)
    pad2_inclusive.SetRightMargin(0.075)
    pad2_inclusive.SetTopMargin(0.045)
    pad2_inclusive.SetBottomMargin(0.4)
    pad2_inclusive.SetFrameBorderMode(0)
    pad2_inclusive.cd()
    i=0
    for var in variations:
        hist_variations_ratio_inclusive[i].GetYaxis().SetNdivisions(504)
        hist_variations_ratio_inclusive[i].GetYaxis().SetRangeUser(-0.3,+0.3)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitleFont(43)
        hist_variations_ratio_inclusive[i].GetYaxis().SetLabelFont(42)
        hist_variations_ratio_inclusive[i].GetYaxis().SetLabelSize(0.12)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitleSize(28)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTickLength(0.035)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitleOffset(1.72)
        hist_variations_ratio_inclusive[i].GetXaxis().SetLabelFont(42)
        hist_variations_ratio_inclusive[i].GetXaxis().SetLabelSize(0.12)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitleSize(28)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTickLength(0.1)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitleOffset(3.5)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitleFont(43)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitle("Dark Higgs candidate mass [GeV]")
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitle("Rel. Diff.")
        hist_variations_ratio_inclusive[i].Draw("histsame") 
        i += 1
    c1_inclusive.Update() 
    c1_inclusive.SaveAs("./all_plots/scale_variations_"+str(dsid)+"_"+"inclusive_MET.pdf")

    pdf_variations = []
    
    for i in range(1,101):
        if (analysis=="monoSbb"):
            pdf_variations.append("PDF=263000MemberID="+str(i))
        elif (analysis=="monoHbb"):
            pdf_variations.append("PDF=260000MemberID="+str(i))
        else:
            print "ERROR: wrong analysis name!!"
        
    more_colors = [kMagenta, kBlue, kGreen, kYellow, kPink, kOrange, kViolet, kTeal, kBlack, kAzure]        
    colors_pdf = []
    
    for color in more_colors:
        for i  in range(1,11):
            colors_pdf.append(color+i)    

    print " ****** PDF ****** "
    c1_inclusive_pdf = TCanvas('c1_inclusive_pdf', '', 800,800) 
    c1_inclusive_pdf.Divide(1,2)
    pad1_inclusive_pdf = c1_inclusive_pdf.cd(1)
    pad1_inclusive_pdf.SetLogy()
    pad1_inclusive_pdf.SetPad(0.0,0.3,1.0,1.0)
    pad1_inclusive_pdf.SetBorderMode(0)
    pad1_inclusive_pdf.SetBorderSize(2)
    pad1_inclusive_pdf.SetTickx(1)
    pad1_inclusive_pdf.SetTicky(1)
    pad1_inclusive_pdf.SetLeftMargin(0.13)
    pad1_inclusive_pdf.SetRightMargin(0.075)
    pad1_inclusive_pdf.SetTopMargin(0.07)
    pad1_inclusive_pdf.SetBottomMargin(0.012)
    pad1_inclusive_pdf.SetFrameBorderMode(0)
    pad1_inclusive_pdf.cd()
    hist_nominal_inclusive.GetYaxis().SetTitleFont(42)
    hist_nominal_inclusive.GetYaxis().SetTitleSize(0.12)
    hist_nominal_inclusive.GetYaxis().SetTitleOffset(1.72)
    hist_nominal_inclusive.Draw("hist")
    i=0
    hist_pdf_ratio_inclusive = []
    hist_pdf_variations_inclusive = []
    hist_pdf_variations_ratio_inclusive = []
    legend_inclusive_pdf = TLegend(0.40,0.20,0.95,0.9)
    legend_inclusive_pdf.SetFillStyle(0)
    legend_inclusive_pdf.SetLineColor(0)
    legend_inclusive_pdf.SetBorderSize(0)
    legend_inclusive_pdf.AddEntry(hist_nominal_inclusive,"nominal","l")
    for pdf in pdf_variations:
        hist_pdf_inclusive = ROOT.TH1F()
        hist_pdf_inclusive = Merge_MET_bins(inFile,str(pdf),sample)
        hist_pdf_variations_inclusive.append(hist_pdf_inclusive)
        # for variation "pdf" now I have the inclusive MET histogram
        hist_pdf_variations_inclusive[i].SetLineColor(colors_pdf[i])
        hist_pdf_variations_inclusive[i].Draw("histsame")
        if (i%5==0):
            legend_inclusive_pdf.AddEntry(hist_pdf_variations_inclusive[i],str(pdf),"l")
        hist_clone_inclusive_pdf = hist_pdf_inclusive.Clone("hist_clone")
        hist_clone_inclusive_pdf.Add(hist_nominal_inclusive,-1) #(var-nominal)/nominal = rel. difference
        hist_clone_inclusive_pdf.Divide(hist_nominal_inclusive)
        hist_clone_inclusive_pdf.SetLineColor(colors_pdf[i])
        hist_pdf_variations_ratio_inclusive.append(hist_clone_inclusive_pdf)
        i += 1
    legend_inclusive_pdf.Draw()    
    pad2_inclusive_pdf = c1_inclusive_pdf.cd(2)
    pad2_inclusive_pdf.SetPad(0.0,0.0,1.0,0.3)
    pad2_inclusive_pdf.SetBorderMode(0)
    pad2_inclusive_pdf.SetFillColor(0)
    pad2_inclusive_pdf.SetBorderSize(2)
    pad2_inclusive_pdf.SetTickx(1)
    pad2_inclusive_pdf.SetTicky(1)
    pad2_inclusive_pdf.SetLeftMargin(0.13)
    pad2_inclusive_pdf.SetRightMargin(0.075)
    pad2_inclusive_pdf.SetTopMargin(0.045)
    pad2_inclusive_pdf.SetBottomMargin(0.4)
    pad2_inclusive_pdf.SetFrameBorderMode(0)
    pad2_inclusive_pdf.cd()
    i=0
    for var in pdf_variations:
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetNdivisions(504)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetRangeUser(-0.3,+0.3)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitleFont(43)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetLabelFont(42)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetLabelSize(0.12)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitleSize(28)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTickLength(0.035)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitleOffset(1.72)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetLabelFont(42)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetLabelSize(0.12)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitleSize(28)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTickLength(0.1)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitleOffset(3.5)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitleFont(43)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitle("Dark Higgs candidate mass [GeV]")
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitle("Rel. Diff.")
        hist_pdf_variations_ratio_inclusive[i].Draw("histsame") 
        i += 1
    c1_inclusive_pdf.Update() 
    c1_inclusive_pdf.SaveAs("./all_plots/pdf_variations_"+str(dsid)+"_"+"inclusive_MET.pdf")     

def Merge_MET_bins(inFile,variation,sample):
    regions = ["200_350ptv", "350_500ptv", "500ptv"]
    infile = ROOT.TFile.Open(inFile)
    hist = ROOT.TH1F()
    #get one MET bin
    if (variation=="Nominal"):
        hist = infile.Get("hist_0Lep_"+str(sample)+"_2tag2pjet_150_200ptv_mBB_res_allcut_"+str(variation))
    else:
        dir = infile.Get("WeightVariations")
        hist = dir.Get("hist_0Lep_"+str(sample)+"_2tag2pjet_150_200ptv_mBB_res_allcut_"+str(variation))
    hist_clone = ROOT.TH1F()
    hist_clone = hist.Clone("hist_clone")
    #add the other MET bins
    for r in regions:
        if (r == "200_350ptv") or (r == "350_500ptv"):
            label_region = "res"
            label_tag_jet = "_2tag2pjet_"
        elif (r == "500ptv"):
            label_region = "mer"
            label_tag_jet = "_2tag1pfatjet_0addtag_"
        hist_other_MET_bin = ROOT.TH1F()
        if (variation=="Nominal"):
            hist_other_MET_bin = infile.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(r)+"_mBB_"+str(label_region)+"_allcut_"+str(variation))
        else:
            dir = infile.Get("WeightVariations")
            hist_other_MET_bin = dir.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(r)+"_mBB_"+str(label_region)+"_allcut_"+str(variation))
        hist_clone.Add(hist_other_MET_bin)
        hist_clone.SetDirectory(0)
    return hist_clone 

    
def main():
    print "Going to draw all variations for mono-SWW signal samples: scale + pdf"
    regions = ["Merged", "Intermediate", "Resolved"]
    args = getArgs()
    inFileX = args.inputFileMadGraphPy8_A
    inFileZ = args.inputFileMadGraphPy8_B
    inFileY = args.inputFileSherpa_A
    inFileW = args.inputFileSherpa_B
    analysis = args.analysis
    DrawVariations(inFileX,inFileY,inFileZ,inFileW, analysis)
    #DrawVariations_InclusiveMET(inFile,sample,analysis)

if __name__ == '__main__':
    main()
