#!/bin/bash
# Date:     191106
# Purpose:  Script to run compareVariations_monoS.py with the reweighting option to reweight histograms for both V+jets backgrounds

bkgs=("Zjets" "Wjets")
#bkgs=("Wjets")

for bkg in "${bkgs[@]}"
do
  printf "\n\n--------> RUNNING REWEIGHTING FOR BACKGROUND %s\n\n" $bkg
  ./compareVariations_monoS.py -a monoSWW -d hadded_hists -n $bkg --reweight
done
