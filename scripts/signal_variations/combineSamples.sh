#!/bin/bash
echo "Example usage: source combineSamples.sh hadded_hists hadded_hists"

categories=( "Resolved" "Intermediate" "Merged" "Inclusive" )
processes_Wjets=("Wmunu" "Wenu")
processes_Zjets=("Znunu" "Zmumu" "Zee")
fileTypes=("" "_tree")

for type in "${fileTypes[@]}"
do
  # Hadd Sherpa Wjets processes
  for N_LEP in {0..2}
  do
    for CATEGORY in "${categories[@]}"
    do
      commandhadd="hadd $2/output_${N_LEP}Lep_${CATEGORY}_Wjets_Sh${type}.root"
      for WJET_PROCESS in "${processes_Wjets[@]}"
      do
        echo Handling Wjet process ${WJET_PROCESS}
        if [ ! -f "$1/output_${N_LEP}Lep_${CATEGORY}_${WJET_PROCESS}_Sh${type}.root" ]; then
          echo -e "$1/output_${N_LEP}Lep_${CATEGORY}_${WJET_PROCESS}_Sh${type}.root \\e[0;41mDOES NOT\\e[0m exist"
          echo "Will not hadd this file"
        else
          commandhadd+=" $1/output_${N_LEP}Lep_${CATEGORY}_${WJET_PROCESS}_Sh${type}.root"
        fi
      done
    echo "executing" $commandhadd
    $commandhadd
    done
  done

  # Hadd Sherpa Zjets processes
  for N_LEP in {0..2}
  do
    for CATEGORY in "${categories[@]}"
    do
      commandhadd="hadd $2/output_${N_LEP}Lep_${CATEGORY}_Zjets_Sh${type}.root"
      for ZJET_PROCESS in "${processes_Zjets[@]}"
      do
        echo Handling Zjet process ${ZJET_PROCESS}
        if [ ! -f "$1/output_${N_LEP}Lep_${CATEGORY}_${ZJET_PROCESS}_Sh${type}.root" ]; then
          echo -e "$1/output_${N_LEP}Lep_${CATEGORY}_${ZJET_PROCESS}_Sh${type}.root \\e[0;41mDOES NOT\\e[0m exist"
          echo "Will not hadd this file"
        else
          commandhadd+=" $1/output_${N_LEP}Lep_${CATEGORY}_${ZJET_PROCESS}_Sh${type}.root"
	fi
      done
    echo "executing" $commandhadd
    $commandhadd
    done
  done


  # Hadd MGPy Wjets processes
  for N_LEP in {0..2}
  do
    for CATEGORY in "${categories[@]}"
    do
      commandhadd="hadd $2/output_${N_LEP}Lep_${CATEGORY}_Wjets_MGPy${type}.root"
      for WJET_PROCESS in "${processes_Wjets[@]}"
      do
        echo Handling Wjet process ${WJET_PROCESS}
        if [ ! -f "$1/output_${N_LEP}Lep_${CATEGORY}_${WJET_PROCESS}_MGPy${type}.root" ]; then
          echo -e "$1/output_${N_LEP}Lep_${CATEGORY}_${WJET_PROCESS}_MGPy${type}.root \\e[0;41mDOES NOT\\e[0m exist"
          echo "Will not hadd this file"
        else
          commandhadd+=" $1/output_${N_LEP}Lep_${CATEGORY}_${WJET_PROCESS}_MGPy${type}.root"
	fi
      done
    echo "executing" $commandhadd
    $commandhadd
    done
  done


  # Hadd MGPy Zjets processes
  for N_LEP in {0..2}
  do
    for CATEGORY in "${categories[@]}"
    do
      commandhadd="hadd $2/output_${N_LEP}Lep_${CATEGORY}_Zjets_MGPy${type}.root"
      for ZJET_PROCESS in "${processes_Zjets[@]}"
      do
        echo Handling Zjet process ${ZJET_PROCESS}
        if [ ! -f "$1/output_${N_LEP}Lep_${CATEGORY}_${ZJET_PROCESS}_MGPy${type}.root" ]; then
          echo -e "$1/output_${N_LEP}Lep_${CATEGORY}_${ZJET_PROCESS}_MGPy${type}.root \\e[0;41mDOES NOT\\e[0m exist"
          echo "Will not hadd this file"
        else
          commandhadd+=" $1/output_${N_LEP}Lep_${CATEGORY}_${ZJET_PROCESS}_MGPy${type}.root"
	fi
      done
    echo "executing" $commandhadd
    $commandhadd
    done
  done
done
