#!/usr/bin/python

# this script is to make the 2 point variation: MadGraphPy8 vs Sherpa

# Example usage:
# python 2pointVariation.py -x ../../../../../output_DMM/Wmunu/output_1Lep_Merged_all_MGPy8.root -y ../../../../../output_DMM/Wmunu/output_1Lep_Merged_all_Sh.root -m 0 -a monoSWW

from argparse import ArgumentParser
from pprint import pprint, pformat
import os
import csv
from math import sqrt, log
import logging
import ROOT
from ROOT import *
ROOT.gROOT.SetBatch(True)

def getArgs():
    parser = ArgumentParser()
    parser.add_argument('-x',
                        '--inputFileMadGraphPy8',
                        default='',
                        help='output file of the NtupleToHist code containing the nominal histos with MadGraphPy8.')
    parser.add_argument('-y',
                        '--inputFileSherpa',
                        default='',
                        help='output file of the NtupleToHist code containing the nominal histos with Sherpa.')
    parser.add_argument('-m',
                        '--MetBin',
                        default='3',
                        help='Specify MET bin: 0: 200-300, 1: 300-500, 2: 500-inf, 3: inclusive')

    parser.add_argument('-v',
                        '--variable',
                        default='mS_candidate',
                        help='Specify MET bin: 0: 200-300, 1: 300-500, 2: 500-inf, 3: inclusive')


    parser.add_argument('-a',
                        '--analysis',
                        default='monoSWW',
                        help='monoHbb, monoSbb, or monoSWW')
    return parser.parse_args()

# Function to extract the dataset ID from the filename
def get_dsid(filename, analysis):
  if(analysis=="monoSWW"): dsid = filename.split('_')[3]      # MonoSWW additionally specifies the recycling strategy region before the dsid
  else: dsid = filename.split('_')[2]
  return dsid

# Function to get to the line corresponding to a given dsid in the file SampleInformation_13TeV.txt containing the info for each sample
def get_sample_info(inFile, dsid):

  # Open SampleInformation_13TeV.txt
  sample_info = open("../../data/SampleInformation_13TeV.txt")
  if dsid == "all":
    return "","",""
  # Find the line beginning with the given dsid
  for line in sample_info:

    words = line.replace("\t"," ").split(" ")
    if words[0]==dsid:
      sample_name = words[1]
      generator = words[2]
      if len(words) == 4 : 
          short_filename = words[3]
      else:
          short_filename = ""
      return sample_name, generator, short_filename

  # Print an error if no line was found corresponding to the dsid
  print("ERROR! No line found in SampleInformation_13TeV.txt for DSID %s"%dsid)
  # exit()
  return "","",""

# Get the pdf variations for either signal or background
def get_pdf_vars(analysis, type):
  pdf_variations = []
  
  if type == "signal":
    for i in range(1,101):
      if (analysis=="monoSbb" or analysis=="monoSWW"):
        pdf_variations.append("PDF=263000MemberID="+str(i))
      elif (analysis=="monoHbb"):
        pdf_variations.append("PDF=260000MemberID="+str(i))
      else:
        print "ERROR: wrong analysis name!!"

  elif type == "background":
    print("PDF variations not yet implemented for background :(")

  else:
    print("ERROR! sample type must be either signal or background")

  return pdf_variations

def resolve_names(filename):
    # first the analysis region
    analysisRegion = ""
    leptonnumber = ""
    if "0Lep" in filename:
        analysisRegion = "SR"
        leptonnumber = "0"
    elif "1Lep" in filename:
        analysisRegion = "CR1L"
        leptonnumber = "1"
    elif "2Lep" in filename:
        analysisRegion = "CR2L"
        leptonnumber = "2"
    # then the category 
    category = ""
    if "Resolved" in filename:
        category = "Resolved"
    elif "Intermediate" in filename:
        category = "Intermediate"
    elif "Merged" in filename:
        category = "Merged"
    # then the process
    process = ""
    pshort = "" # for flavour
    if "Znunu" in filename:
        process = "Znunu"
        pshort="Zall"
    elif "Zmumu" in filename:
        process = "Zmumu"
        pshort="Zall"
    elif "Zee" in filename:
        process = "Zee"
        pshort="Zall"
    elif "Wmunu" in filename:
        process = "Wmunu"
        pshort="Wall"
    elif "Wenu" in filename:
        process = "Wenu"
        pshort="Wall"
    elif "Wtaunu" in filename:
        process = "Wtaunu"
        pshort="Wall"

    return analysisRegion, category, leptonnumber, process, pshort



def DrawVariations(inFileX, inFileY, METBin,variable ,analysis):
    region="merged"
    regions = ["200_300ptv", "300_500ptv", "500ptv","allptv"]
    METBinStr=""
    if METBin == "0":
        METBinStr = "200_300ptv"
    if METBin == "1":
        METBinStr = "300_500ptv"
    if METBin == "2":
        METBinStr = "500ptv"
    if METBin == "3":
        METBinStr = "allptv"


    index_region=0
    
    pathX, filenameX = os.path.split(inFileX)
    print "filenameX: ", filenameX
    
    pathX, filenameY = os.path.split(inFileY)
    print "filenameY: ", filenameY
    
    analysisRegionX, categoryX, leptonnumberX, processX, pshortX = resolve_names(filenameX)
    analysisRegionY, categoryY, leptonnumberY, processY, pshortY = resolve_names(filenameY)

    if analysisRegionX != analysisRegionY:
        print "WARNING comparing two analysis Regions. OK if studying acceptance"

    if categoryX != categoryY:
        print "WARNING comparing two categories. OK if studying acceptance"

    nameX, extensionX = filenameX.split('.')
    dsidX = get_dsid(nameX, analysis)
    print "DSID X: ", dsidX
    
    nameY, extensionY = filenameY.split('.')
    dsidY = get_dsid(nameY, analysis)
    print "DSID Y: ", dsidY
    

    sampleX, generatorX, short_filenameX = get_sample_info(inFileX, dsidX)
    print("Sample: %s"%sampleX)
    
    sampleY, generatorY, short_filenameY = get_sample_info(inFileY, dsidY)
    print("Sample: %s"%sampleY)
    
    ROOT.gStyle.SetOptStat(0)
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    
    colors = [kRed, kBlue, kGreen, kYellow, kViolet, kCyan]
    more_colors = [kMagenta, kBlue, kGreen, kYellow, kPink, kOrange, kViolet, kTeal, kBlack, kAzure]
    colors_pdf = []

    for color in more_colors:
        for i  in range(1,11):
            colors_pdf.append(color+i)

    infileX = ROOT.TFile.Open(inFileX)
    infileY = ROOT.TFile.Open(inFileY)
    print infileX
    print infileY
    infileX.ls()
    # name can be changed
    outfile_acceptance = open("./output_files/acceptance_2point_uncertainties_"+str(dsidX)+"_"+str(dsidY)+".txt","w")

    # hist_nominal = infile.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(region)+"_mBB_"+str(label_region)+"_allcut_Nominal")  # Needs to be adapted
    # histonameX = "hist_"+leptonnumberX+"Lep_"+pshortX+"_150ptv_"+analysisRegionX+"_mS_candidate_Nominal"

    histonameX = "hist_"+leptonnumberX+"Lep_"+pshortX+"_"+METBinStr+"_"+analysisRegionX+"_"+variable+"_Nominal"
    infileY.cd("WeightVariations")
    # histonameXspecial= "WeightVariations/hist_0Lep_Zall_500ptv_SR_mS_candidate_MUR0.5_MUF1_PDF261000"
    print histonameX
    histX = infileX.Get(histonameX)  # Needs to be adapted

    print histX
    histX.Rebin(4)
    if variable == "MET" or variable == "pTV":
        histX.Rebin(4)

    # histX = ROOT.TH1F
    histX.SetLineColor(kBlack)
    #print "region: ", str(region)
    # dir = infile.Get("WeightVariations")
    print " ****** Evaluating scale variations ******"
    c1 = TCanvas('c_scale_'+str(region), '', 800,800) 
    c1.Divide(1,2)
    pad1 = c1.cd(1)
    pad1.SetLogy()
    pad1.SetPad(0.0,0.3,1.0,1.0)
    pad1.SetBorderMode(0)
    pad1.SetBorderSize(2)
    pad1.SetTickx(1)
    pad1.SetTicky(1)
    pad1.SetLeftMargin(0.13)
    pad1.SetRightMargin(0.075)
    pad1.SetTopMargin(0.07)
    pad1.SetBottomMargin(0.012)
    pad1.SetFrameBorderMode(0)
    pad1.cd()
    histX.GetYaxis().SetTitleFont(42)
    histX.GetYaxis().SetTitleSize(0.12)
    histX.GetYaxis().SetTitleOffset(1.72)
    histX.Draw("hist")
    histY = ROOT.TH1F()

    legend = TLegend(0.60,0.60,0.9,0.9)
    legend.SetHeader(str(leptonnumberX)+"Lep_"+str(categoryX)+"_"+str(processX)+"_"+METBinStr)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)
    legend.SetBorderSize(0)
    legend.AddEntry(histX,"MadGraphPy8","l")
    histY_variations_ratio = []
    max_rel_diff = 0.

    # print histonameXspecial
    infileY.ls()

    histY = infileY.Get(histonameX )    # Needs to be adapted
    print histY
    histY.Rebin(4)
    if variable == "MET" or variable == "pTV":
        histY.Rebin(4)

    histY.SetLineColor(colors[0])
    # now make shape comparison
    histX.Scale(1./histX.Integral())
    histY.Scale(1./histY.Integral())

    histY.Draw("histYsame")
    legend.AddEntry(histY,"Sherpa","l");
    histY_clone = histY.Clone("histY_clone")
    histY_clone.Divide(histX)
    histY_clone.SetLineColor(colors[0])
    histY_variations_ratio.append(histY_clone)

    outfile_acceptance.write("2 "+str(index_region)+" "+str(0)+"\n")    
    legend.Draw()
    pad2 = c1.cd(2)
    pad2.SetPad(0.0,0.0,1.0,0.3)
    pad2.SetBorderMode(0)
    pad2.SetFillColor(0)
    pad2.SetBorderSize(2)
    pad2.SetTickx(1)
    pad2.SetTicky(1)
    pad2.SetLeftMargin(0.13)
    pad2.SetRightMargin(0.075)
    pad2.SetTopMargin(0.045)
    pad2.SetBottomMargin(0.4)
    pad2.SetFrameBorderMode(0)
    pad2.cd()
    i=0
    for var in histY_variations_ratio:
        histY_variations_ratio[i].GetYaxis().SetNdivisions(504)
        histY_variations_ratio[i].GetYaxis().SetRangeUser(.5,1.5)
        histY_variations_ratio[i].GetYaxis().SetTitleFont(43)
        histY_variations_ratio[i].GetYaxis().SetLabelFont(42)
        histY_variations_ratio[i].GetYaxis().SetLabelSize(0.12)
        histY_variations_ratio[i].GetYaxis().SetTitleSize(28)
        histY_variations_ratio[i].GetYaxis().SetTickLength(0.035)
        histY_variations_ratio[i].GetYaxis().SetTitleOffset(1.72)
        histY_variations_ratio[i].GetXaxis().SetLabelFont(42)
        histY_variations_ratio[i].GetXaxis().SetLabelSize(0.12)
        histY_variations_ratio[i].GetXaxis().SetTitleSize(28)
        histY_variations_ratio[i].GetXaxis().SetTickLength(0.1)
        histY_variations_ratio[i].GetXaxis().SetTitleOffset(3.5)
        histY_variations_ratio[i].GetXaxis().SetTitleFont(43)
        if variable == "MET":
            histY_variations_ratio[i].GetXaxis().SetTitle("MET [GeV]")
        elif variable == "pTV":
            histY_variations_ratio[i].GetXaxis().SetTitle("p_{TV} [GeV]")
        else:
            histY_variations_ratio[i].GetXaxis().SetTitle("Dark Higgs candidate mass [GeV]")
        histY_variations_ratio[i].GetYaxis().SetTitle("Ratio")
        histY_variations_ratio[i].Draw("histsameE1") 
        i += 1
    line=ROOT.TLine(0,1,400,1);
    line.SetLineColor(kBlack);
    line.SetLineWidth(1);
    line.SetLineStyle(4);
    line.Draw("Same");
    c1.Update() 
    # c1.SaveAs("./all_plots/2_point_comparison"+str(dsidX)+"_"+str(dsidY)+"_"+str(region)+".pdf")
    c1.SaveAs("./all_plots/2_point_comparison"+str(leptonnumberX)+"Lep_"+str(categoryX)+"_"+str(processX)+"_"+METBinStr+"_"+variable+".pdf")

    histY_variations_ratio[0].Draw()
    twopointroot = ROOT.TFile("output_files/output"+str(leptonnumberX)+"Lep_"+str(categoryX)+"_"+str(processX)+"_"+METBinStr+"_"+variable+".root","UPDATE")
    histY_variations_ratio[0].Write()

    # print " ****** Evaluation of pdf uncertainties ****** "
    # c2 = TCanvas('c_pdf'+str(region), '', 800,800) 
    # c2.Divide(1,2)
    # pad1_pdf = c2.cd(1)
    # pad1_pdf.SetLogy()
    # pad1_pdf.SetPad(0.0,0.3,1.0,1.0)
    # pad1_pdf.SetBorderMode(0)
    # pad1_pdf.SetBorderSize(2)
    # pad1_pdf.SetTickx(1)
    # pad1_pdf.SetTicky(1)
    # pad1_pdf.SetLeftMargin(0.13)
    # pad1_pdf.SetRightMargin(0.075)
    # pad1_pdf.SetTopMargin(0.07)
    # pad1_pdf.SetBottomMargin(0.012)
    # pad1_pdf.SetFrameBorderMode(0)
    # pad1_pdf.cd()
    # histX.Draw("hist")
    # hist_pdf = ROOT.TH1F()
    # legend_pdf = TLegend(0.40,0.20,0.95,0.9)
    # legend_pdf.SetFillStyle(0)
    # legend_pdf.SetLineColor(0)
    # legend_pdf.SetBorderSize(0)
    # legend_pdf.AddEntry(histX,"nominal","l")
    # j=0
    # hist_pdf_variations_ratio = []
    # hist_pdf_variations = []
    # for pdf_var in pdf_variations:
    #     # hist_pdf = dir.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(region)+"_mBB_"+str(label_region)+"_allcut_"+str(pdf_var))
    #     if str(pdf_var) == str(pdf_variations[0]):
    #         continue
    #     print "hist_0Lep_%s_150ptv_SR_mS_candidate_%s"%(sample, pdf_var)
    #     hist_pdf = dir.Get("hist_0Lep_%s_150ptv_SR_mS_candidate_%s"%(sample, pdf_var))  # Needs to be adapted

    #     hist_pdf.SetLineColor(colors_pdf[j])
    #     hist_pdf.Draw("histsame")
    #     # if (j%5==0): 
    #         # legend_pdf.AddEntry(hist_pdf,str(pdf_var),"l")
    #     legend_pdf.AddEntry(hist_pdf,str(pdf_var),"l")
    #     hist_pdf_variations.append(hist_pdf)                        
    #     hist_clone_pdf = hist_pdf.Clone("hist_clone")
    #     hist_clone_pdf.Add(histX,-1) #(var-nominal)/nominal = rel. difference
    #     hist_clone_pdf.Divide(histX)
    #     hist_clone_pdf.SetLineColor(colors_pdf[j])
    #     hist_pdf_variations_ratio.append(hist_clone_pdf)
    #     j += 1
    # legend_pdf.Draw()
    # pad2_pdf = c2.cd(2)
    # pad2_pdf.SetPad(0.0,0.0,1.0,0.3)
    # pad2_pdf.SetBorderMode(0)
    # pad2_pdf.SetFillColor(0)
    # pad2_pdf.SetBorderSize(2)
    # pad2_pdf.SetTickx(1)
    # pad2_pdf.SetTicky(1)
    # pad2_pdf.SetLeftMargin(0.13)
    # pad2_pdf.SetRightMargin(0.075)
    # pad2_pdf.SetTopMargin(0.045)
    # pad2_pdf.SetBottomMargin(0.4)
    # pad2_pdf.SetFrameBorderMode(0)
    # pad2_pdf.cd()
    # j=0
    # for var_pdf in hist_pdf_variations_ratio:
    #     hist_pdf_variations_ratio[j].GetYaxis().SetNdivisions(504)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetRangeUser(-0.3,+0.3)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetTitleFont(43)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetLabelFont(42)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetLabelSize(0.12)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetTitleSize(28)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetTickLength(0.035)
    #     hist_pdf_variations_ratio[j].GetYaxis().SetTitleOffset(1.72)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetLabelFont(42)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetLabelSize(0.12)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetTitleSize(28)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetTickLength(0.1)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetTitleOffset(3.5)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetTitleFont(43)
    #     hist_pdf_variations_ratio[j].GetXaxis().SetTitle("Dark Higgs candidate mass [GeV]")
    #     hist_pdf_variations_ratio[j].GetYaxis().SetTitle("Rel. Diff.")
    #     hist_pdf_variations_ratio[j].Draw("histsame")
    #     j += 1
    # c2.Update() 
    # c2.SaveAs("./all_plots/pdf_variations"+str(dsid)+"_"+str(region)+".pdf")

    # print "Evaluation error PDF using standard deviation"    

    # central_value = 0.
    # error_pdf = 0.
    # sum_integrals = 0.
    # sum_integrals_sq = 0.

    # for hist in hist_pdf_variations:
    #     sum_integrals += hist.Integral()
    #     sum_integrals_sq += hist.Integral()*hist.Integral()

    # central_value = sum_integrals/100. #just the mean
    # mean_of_sq = sum_integrals_sq/100.

    # print "the central value of the pdfs set: ", central_value 
    # print "should be similar to nominal value: ", histX.Integral()

    # error_pdf = sqrt((mean_of_sq-central_value*central_value)*100/99.)#Using standard deviation approach:https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PdfRecommendations#Standard_deviation
    # #Need to use always the central value: https://arxiv.org/pdf/1510.03865.pdf

    # nom_value = histX.Integral()

    # round_error_pdf = round(error_pdf*100./nom_value,1)#relative percentage error

    # #print "met bin: " + str(region) + " relative error pdf: ", round_error_pdf

    # outfile_pdf.write("3 "+str(index_region)+" "+str(round_error_pdf)+"\n")    
    # index_region+=1

    # outfile_scale.close()
    # outfile_pdf.close()
    # filenames = ["./output_files/signal_uncertainties_"+str(dsid)+"_scale.txt","./output_files/signal_uncertainties_"+str(dsid)+"_pdf.txt"]

    # # with open('./output_files/sys_mc16_13TeV.'+str(dsid)+'.MadGraphPy8EG_A14NNP23LO_'+str(analysis)+"_"+masses[str(dsid)]+'.txt', 'w') as outfile:
    # with open('./output_files/sys_mc16_13TeV.'+str(dsid)+'.MadGraphPy8EG_A14NNP23LO_'+str(analysis)+'.txt', 'w') as outfile:
    #     for fname in filenames:
    #         with open(fname) as infile:
    #             outfile.write(infile.read())

def DrawVariations_InclusiveMET(inFile,sample,analysis):

    path, filename = os.path.split(inFile)
    print "filename: ", filename
    name, extension = filename.split('.')
    output, lepton, dsid = name.split('_')
    print "DSID: ", dsid
    
    ROOT.gStyle.SetOptStat(0)
    gROOT.LoadMacro("AtlasStyle.C")
    SetAtlasStyle()
    
    variations = ["MUF=0.5","MUF=2.0","MUR=0.5","MUR=0.5MUF=0.5","MUR=2.0","MUR=2.0MUF=2.0"]
    variations_names = ["#mu_{F}=0.5","#mu_{F}=2.0","#mu_{R}=0.5","#mu_{R}=0.5#mu_{F}=0.5","#mu_{R}=2.0","#mu_{R}=2.0#mu_{F}=2.0"]
        
    colors = [kRed, kBlue, kGreen, kYellow, kViolet, kCyan]
    
    print " ******** INCLUSIVE MET PLOTS ******** "
    
    hist_nominal_inclusive = ROOT.TH1F()
    hist_nominal_inclusive.SetLineColor(kBlack)
    hist_nominal_inclusive = Merge_MET_bins(inFile,"Nominal",sample)
    # merge scale variations
    print " ****** SCALE ****** "
    c1_inclusive = TCanvas('c1_inclusive', '', 800,800) 
    c1_inclusive.Divide(1,2)
    pad1_inclusive = c1_inclusive.cd(1)
    pad1_inclusive.SetLogy()
    pad1_inclusive.SetPad(0.0,0.3,1.0,1.0)
    pad1_inclusive.SetBorderMode(0)
    pad1_inclusive.SetBorderSize(2)
    pad1_inclusive.SetTickx(1)
    pad1_inclusive.SetTicky(1)
    pad1_inclusive.SetLeftMargin(0.13)
    pad1_inclusive.SetRightMargin(0.075)
    pad1_inclusive.SetTopMargin(0.07)
    pad1_inclusive.SetBottomMargin(0.012)
    pad1_inclusive.SetFrameBorderMode(0)
    pad1_inclusive.cd()
    hist_nominal_inclusive.GetYaxis().SetTitleFont(42)
    hist_nominal_inclusive.GetYaxis().SetTitleSize(0.12)
    hist_nominal_inclusive.GetYaxis().SetTitleOffset(1.72)
    hist_nominal_inclusive.Draw("hist")
    i=0
    hist_variations_ratio_inclusive = []
    hist_variations_inclusive = []
    legend_inclusive = TLegend(0.60,0.60,0.9,0.9)
    legend_inclusive.SetFillStyle(0)
    legend_inclusive.SetLineColor(0)
    legend_inclusive.SetBorderSize(0)
    legend_inclusive.AddEntry(hist_nominal_inclusive,"nominal","l")
    for scale in variations:
        hist_scale_inclusive = ROOT.TH1F()
        hist_scale_inclusive = Merge_MET_bins(inFile,str(scale),sample)
        hist_variations_inclusive.append(hist_scale_inclusive)
        # for variation "scale" now I have the inclusive MET histogram
        hist_variations_inclusive[i].SetLineColor(colors[i])
        hist_variations_inclusive[i].Draw("histsame")
        legend_inclusive.AddEntry(hist_variations_inclusive[i],variations_names[i],"l")
        hist_clone_inclusive = hist_scale_inclusive.Clone("hist_clone")
        hist_clone_inclusive.Add(hist_nominal_inclusive,-1) #(var-nominal)/nominal = rel. difference
        hist_clone_inclusive.Divide(hist_nominal_inclusive)
        hist_clone_inclusive.SetLineColor(colors[i])
        hist_variations_ratio_inclusive.append(hist_clone_inclusive)
        i += 1
    legend_inclusive.Draw()    
    pad2_inclusive = c1_inclusive.cd(2)
    pad2_inclusive.SetPad(0.0,0.0,1.0,0.3)
    pad2_inclusive.SetBorderMode(0)
    pad2_inclusive.SetFillColor(0)
    pad2_inclusive.SetBorderSize(2)
    pad2_inclusive.SetTickx(1)
    pad2_inclusive.SetTicky(1)
    pad2_inclusive.SetLeftMargin(0.13)
    pad2_inclusive.SetRightMargin(0.075)
    pad2_inclusive.SetTopMargin(0.045)
    pad2_inclusive.SetBottomMargin(0.4)
    pad2_inclusive.SetFrameBorderMode(0)
    pad2_inclusive.cd()
    i=0
    for var in variations:
        hist_variations_ratio_inclusive[i].GetYaxis().SetNdivisions(504)
        hist_variations_ratio_inclusive[i].GetYaxis().SetRangeUser(-0.3,+0.3)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitleFont(43)
        hist_variations_ratio_inclusive[i].GetYaxis().SetLabelFont(42)
        hist_variations_ratio_inclusive[i].GetYaxis().SetLabelSize(0.12)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitleSize(28)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTickLength(0.035)
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitleOffset(1.72)
        hist_variations_ratio_inclusive[i].GetXaxis().SetLabelFont(42)
        hist_variations_ratio_inclusive[i].GetXaxis().SetLabelSize(0.12)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitleSize(28)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTickLength(0.1)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitleOffset(3.5)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitleFont(43)
        hist_variations_ratio_inclusive[i].GetXaxis().SetTitle("Dark Higgs candidate mass [GeV]")
        hist_variations_ratio_inclusive[i].GetYaxis().SetTitle("Rel. Diff.")
        hist_variations_ratio_inclusive[i].Draw("histsame") 
        i += 1
    c1_inclusive.Update() 
    c1_inclusive.SaveAs("./all_plots/scale_variations_"+str(dsid)+"_"+"inclusive_MET.pdf")

    pdf_variations = []
    
    for i in range(1,101):
        if (analysis=="monoSbb"):
            pdf_variations.append("PDF=263000MemberID="+str(i))
        elif (analysis=="monoHbb"):
            pdf_variations.append("PDF=260000MemberID="+str(i))
        else:
            print "ERROR: wrong analysis name!!"
        
    more_colors = [kMagenta, kBlue, kGreen, kYellow, kPink, kOrange, kViolet, kTeal, kBlack, kAzure]        
    colors_pdf = []
    
    for color in more_colors:
        for i  in range(1,11):
            colors_pdf.append(color+i)    

    print " ****** PDF ****** "
    c1_inclusive_pdf = TCanvas('c1_inclusive_pdf', '', 800,800) 
    c1_inclusive_pdf.Divide(1,2)
    pad1_inclusive_pdf = c1_inclusive_pdf.cd(1)
    pad1_inclusive_pdf.SetLogy()
    pad1_inclusive_pdf.SetPad(0.0,0.3,1.0,1.0)
    pad1_inclusive_pdf.SetBorderMode(0)
    pad1_inclusive_pdf.SetBorderSize(2)
    pad1_inclusive_pdf.SetTickx(1)
    pad1_inclusive_pdf.SetTicky(1)
    pad1_inclusive_pdf.SetLeftMargin(0.13)
    pad1_inclusive_pdf.SetRightMargin(0.075)
    pad1_inclusive_pdf.SetTopMargin(0.07)
    pad1_inclusive_pdf.SetBottomMargin(0.012)
    pad1_inclusive_pdf.SetFrameBorderMode(0)
    pad1_inclusive_pdf.cd()
    hist_nominal_inclusive.GetYaxis().SetTitleFont(42)
    hist_nominal_inclusive.GetYaxis().SetTitleSize(0.12)
    hist_nominal_inclusive.GetYaxis().SetTitleOffset(1.72)
    hist_nominal_inclusive.Draw("hist")
    i=0
    hist_pdf_ratio_inclusive = []
    hist_pdf_variations_inclusive = []
    hist_pdf_variations_ratio_inclusive = []
    legend_inclusive_pdf = TLegend(0.40,0.20,0.95,0.9)
    legend_inclusive_pdf.SetFillStyle(0)
    legend_inclusive_pdf.SetLineColor(0)
    legend_inclusive_pdf.SetBorderSize(0)
    legend_inclusive_pdf.AddEntry(hist_nominal_inclusive,"nominal","l")
    for pdf in pdf_variations:
        hist_pdf_inclusive = ROOT.TH1F()
        hist_pdf_inclusive = Merge_MET_bins(inFile,str(pdf),sample)
        hist_pdf_variations_inclusive.append(hist_pdf_inclusive)
        # for variation "pdf" now I have the inclusive MET histogram
        hist_pdf_variations_inclusive[i].SetLineColor(colors_pdf[i])
        hist_pdf_variations_inclusive[i].Draw("histsame")
        if (i%5==0):
            legend_inclusive_pdf.AddEntry(hist_pdf_variations_inclusive[i],str(pdf),"l")
        hist_clone_inclusive_pdf = hist_pdf_inclusive.Clone("hist_clone")
        hist_clone_inclusive_pdf.Add(hist_nominal_inclusive,-1) #(var-nominal)/nominal = rel. difference
        hist_clone_inclusive_pdf.Divide(hist_nominal_inclusive)
        hist_clone_inclusive_pdf.SetLineColor(colors_pdf[i])
        hist_pdf_variations_ratio_inclusive.append(hist_clone_inclusive_pdf)
        i += 1
    legend_inclusive_pdf.Draw()    
    pad2_inclusive_pdf = c1_inclusive_pdf.cd(2)
    pad2_inclusive_pdf.SetPad(0.0,0.0,1.0,0.3)
    pad2_inclusive_pdf.SetBorderMode(0)
    pad2_inclusive_pdf.SetFillColor(0)
    pad2_inclusive_pdf.SetBorderSize(2)
    pad2_inclusive_pdf.SetTickx(1)
    pad2_inclusive_pdf.SetTicky(1)
    pad2_inclusive_pdf.SetLeftMargin(0.13)
    pad2_inclusive_pdf.SetRightMargin(0.075)
    pad2_inclusive_pdf.SetTopMargin(0.045)
    pad2_inclusive_pdf.SetBottomMargin(0.4)
    pad2_inclusive_pdf.SetFrameBorderMode(0)
    pad2_inclusive_pdf.cd()
    i=0
    for var in pdf_variations:
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetNdivisions(504)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetRangeUser(-0.3,+0.3)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitleFont(43)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetLabelFont(42)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetLabelSize(0.12)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitleSize(28)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTickLength(0.035)
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitleOffset(1.72)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetLabelFont(42)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetLabelSize(0.12)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitleSize(28)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTickLength(0.1)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitleOffset(3.5)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitleFont(43)
        hist_pdf_variations_ratio_inclusive[i].GetXaxis().SetTitle("Dark Higgs candidate mass [GeV]")
        hist_pdf_variations_ratio_inclusive[i].GetYaxis().SetTitle("Rel. Diff.")
        hist_pdf_variations_ratio_inclusive[i].Draw("histsame") 
        i += 1
    c1_inclusive_pdf.Update() 
    c1_inclusive_pdf.SaveAs("./all_plots/pdf_variations_"+str(dsid)+"_"+"inclusive_MET.pdf")     

def Merge_MET_bins(inFile,variation,sample):
    regions = ["200_350ptv", "350_500ptv", "500ptv"]
    infile = ROOT.TFile.Open(inFile)
    hist = ROOT.TH1F()
    #get one MET bin
    if (variation=="Nominal"):
        hist = infile.Get("hist_0Lep_"+str(sample)+"_2tag2pjet_150_200ptv_mBB_res_allcut_"+str(variation))
    else:
        dir = infile.Get("WeightVariations")
        hist = dir.Get("hist_0Lep_"+str(sample)+"_2tag2pjet_150_200ptv_mBB_res_allcut_"+str(variation))
    hist_clone = ROOT.TH1F()
    hist_clone = hist.Clone("hist_clone")
    #add the other MET bins
    for r in regions:
        if (r == "200_350ptv") or (r == "350_500ptv"):
            label_region = "res"
            label_tag_jet = "_2tag2pjet_"
        elif (r == "500ptv"):
            label_region = "mer"
            label_tag_jet = "_2tag1pfatjet_0addtag_"
        hist_other_MET_bin = ROOT.TH1F()
        if (variation=="Nominal"):
            hist_other_MET_bin = infile.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(r)+"_mBB_"+str(label_region)+"_allcut_"+str(variation))
        else:
            dir = infile.Get("WeightVariations")
            hist_other_MET_bin = dir.Get("hist_0Lep_"+str(sample)+str(label_tag_jet)+str(r)+"_mBB_"+str(label_region)+"_allcut_"+str(variation))
        hist_clone.Add(hist_other_MET_bin)
        hist_clone.SetDirectory(0)
    return hist_clone 

    
def main():
    print "Going to draw all variations for mono-SWW signal samples: scale + pdf"
    regions = ["Merged", "Intermediate", "Resolved"]
    args = getArgs()
    inFileX = args.inputFileMadGraphPy8
    inFileY = args.inputFileSherpa
    METBin = args.MetBin
    analysis = args.analysis
    variable = args.variable
    DrawVariations(inFileX,inFileY,METBin,variable, analysis)
    #DrawVariations_InclusiveMET(inFile,sample,analysis)

if __name__ == '__main__':
    main()
