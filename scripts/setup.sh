setupATLAS
cd build
asetup --restore
if [ -f $AtlasProject_PLATFORM/setup.sh ] ; then
  source $AtlasProject_PLATFORM/setup.sh
else 
  platforms=($(find . -type d -name 'x*' -printf '%f\n'))
  if  (( ${#platforms[@]} == 1 )) ; then
    # source ${platforms[1]}/setup.sh
    source */setup.sh
  else 
    echo "Several platforms have been detected."
    echo "Go in build/ locate the PLATFORM that you want to use and do"
    echo 'source $PLATFORM/setup.sh'
  fi
fi
cd ../run

