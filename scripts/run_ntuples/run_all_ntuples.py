#!/usr/bin/env python
# Run with, eg. ./run_all_ntuples.py -i ~/data/MonoSWW/TruthLevel_ntuples -o ~/data/MonoSWW/TruthLevel_hists

import argparse
import os
from os import path
import json
import subprocess

# Read in user arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--inputpath", required=True,
                help='path to directory containing containers produced by VHbbTruthFramework-21')
ap.add_argument("-o", "--outputpath", required=False, default="hists", 
                help="path to directory to contain organized symlinks to files (defaults to hists)")
ap.add_argument("-c", "--config_file", required=False, default="../../data/inputConfig_config.json",
                help="path to input json config file (defaults to ../../data/inputConfig_config.json)")
ap.add_argument("-d", "--dsid", required=False, default="-1",
                help="dsid to run over")
ap.add_argument("-w", "--overwrite", required=False, default="True", 
                help="overwrite existing output files if they already exist. Set to either 'True' or 'False'. Defaults to 'True'")
args = vars(ap.parse_args())
dsid_user=int(args["dsid"])

# Generators
MadGraphPy8 = ["MadGraphPy8EG", "MadGraphPythia8EvtGen", "MGPy8EG"]
Sherpa = ["Sherpa_221", "Sh_221"]
Generators = [MadGraphPy8, Sherpa]
Generators_str = ["MadGraphPy8", "Sherpa"]

# Open the input json file
config_file = open(args["config_file"])
config = json.load(config_file)


# Loop through each container in the data directory, and decide which sub-directory it belongs to
for filename in os.listdir(args["inputpath"]):
    if filename.endswith("_Lep"):
        # Determine the DSID
        dsid = filename.split(".")[3]
        if dsid_user > 0 and int(dsid) != dsid_user: continue 

        """
        # Determine which sample and generator the DSID belongs to
        file_sample = "dummy_sample"
        file_generator = "dummy_generator"
        for sample in config["samples"]: 
            if dsid in config["samples"][sample]: 
                file_sample = sample
                short_name = config["samples"][sample][dsid]
                for iGen in range(len(Generators)):
                    for sub_gen in Generators[iGen]:
                        if(sub_gen in short_name): file_generator = Generators_str[iGen]
        """
    else: continue

    """
    # Make sure the filename got matched to a generator
    if file_generator == "dummy_gen":
        print("ERROR! Filename %s does not match any of the available generators!\nAvailable generators: "%filename)
        for gen in Generators: print("\t%s"%gen)
        continue 

    # Make sure the filename got matched to a sample                                                                                                                  
    if file_sample == "dummy_sample":     
        print("ERROR! Filename %s does match any of the available samples. "%filename)
        continue             

    # Make a symlink to the file in the appropriate sub-directory
    #try: os.symlink("%s/%s"%(args["inputpath"], filename), "%s/%s/%s/%s"%(args["outputpath"], file_generator, file_sample, filename))
    #except OSError: continue
    """

    # Run NtupleToHist on the dsid, and output the results to the appropriate directory
    analysis_channel = filename.split("_")[-2]
    apply_sherpa = 1
    #output_path = "%s/%s/%s"%(args["outputpath"], file_generator, file_sample)
    output_path = args["outputpath"]

    # If overwrite option set to false, check if the output file already exists, and skip if so
    if args["overwrite"]=="False":
        skip_file=True
        for region in ["Merged", "Intermediate", "Resolved"]:
            if path.exists("%s/output_%sLep_%s_%s.root"%(output_path, analysis_channel, region, dsid)) == False: skip_file=False
        if skip_file: 
            print("Skipping file %s because corresponding output path exists and overwrite is set to 'False'"%filename)
            continue

    print("Calling subprocess for:\n\nNtupleToHist_MonoSWW %s %s %s %s %s"%(analysis_channel, args["inputpath"], dsid, output_path, str(apply_sherpa)))
    subprocess.call(["NtupleToHist_MonoSWW", analysis_channel, args["inputpath"], dsid, output_path, str(apply_sherpa)])
    
