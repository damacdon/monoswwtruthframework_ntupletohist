#ifndef generateHistogram_0lep_H
#define generateHistogram_0lep_H

#include <map>
#include <string>
#include <vector>

#include "ReadTruthTree_MonoSWW.h"
#include "TChain.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TString.h"

#include "generateHistogram_MonoSWW.h"

class generateHistogram_0lep : public generateHistogram {
 public:
  //Bit flag event selection
  static void EventSelection(ReadTruthTree &readNtuple,
                             EventObjects &PhysicsObjects,
                             unsigned long &eventFlag, bool Debug = false, int Region = 0);

  //Build the objects for the event
  static bool BuildEvent(ReadTruthTree &readNtuple,
                         EventObjects &PhysicsObjects, btagTool &BtagTool,
                         bool Debug = false, int Region = 0);

  // Select events for histogram plotting
  void Selection0Lepton();

  // Enumerated values for 0-lepton cutflow
  enum ZeroLeptonCutFlow {
    Input_events,
    METgreaterthan200GeV,
    //dPhi_MET_MPT,
    //Exactly2or3jets,
    TauVeto,
    MinDPhiMETJets,
    MET_Sig,
    mS_window,
    AnalysisRegion,
    //SumJetPt,
    LeadJetPt_greaterthan45GeV,
    //dPhiBB_lessthan140,
    DPhiMETDijetResolved_greaterthan120,
  };

  virtual void FillHistograms() override;

  void FillCutFlow0lep(const unsigned long int flag, float eventWeight,
                       float btagWeight);

  generateHistogram_0lep();
  ~generateHistogram_0lep();

 private:
};

#endif
