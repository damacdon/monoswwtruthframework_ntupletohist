//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct 25 16:20:48 2017 by ROOT version 5.34/36
// from TTree ReadTruthTree/
// found on file: ntuple.root
//////////////////////////////////////////////////////////

#ifndef ReadTruthTree_h
#define ReadTruthTree_h

#include <TChain.h>
#include <TChainElement.h>
#include <TFile.h>
#include <TH1F.h>
#include <TKey.h>
#include <TROOT.h>
#include <iostream>

// Header file for the classes stored in the TTree if any.
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class ReadTruthTree {
 public:
  TChain *fChain;  //!pointer to the analyzed TTree or TChain
  Int_t fCurrent;  //!current Tree number in a TChain

  // Declaration of leaf types
  Int_t Event_Number;
  Float_t Event_Weight;
  std::vector<float> *Event_Weights;
  std::vector<float> *Jet_Pt;
  std::vector<float> *Jet_Eta;
  std::vector<float> *Jet_Phi;
  std::vector<float> *Jet_Mass;
  std::vector<int> *Jet_HadronConeExclTruthLabelID;
  std::vector<float> *ForwardJet_Pt;
  std::vector<float> *ForwardJet_Eta;
  std::vector<float> *ForwardJet_Phi;
  std::vector<float> *ForwardJet_Mass;
  std::vector<float> *LargeRJet_Pt;
  std::vector<float> *LargeRJet_Eta;
  std::vector<float> *LargeRJet_Phi;
  std::vector<float> *LargeRJet_Mass;
  std::vector<float> *ReclusteredJet_Pt;
  std::vector<float> *ReclusteredJet_Eta;
  std::vector<float> *ReclusteredJet_Phi;
  std::vector<float> *ReclusteredJet_Mass;
  Float_t MET;
  Float_t MET_Phi;
  Float_t MPT;
  Float_t MPT_Phi;
  Double_t MET_Sig;
  Double_t MET_Sig_muInvis;
  Int_t nSignalJet;
  Int_t nForwardJet;
  Int_t nLargeRJet;
  Int_t nReclusteredJet;
  Int_t NSignalTaus;
  std::vector<float> *Truth_Boson_Pt;
  std::vector<float> *Truth_Boson_Eta;
  std::vector<float> *Truth_Boson_Phi;
  std::vector<float> *Truth_Boson_Mass;
  // 1 Lepton specific leaves
  std::vector<float> *Lepton_Pt;
  std::vector<float> *Lepton_Eta;
  std::vector<float> *Lepton_Phi;
  std::vector<float> *Lepton_Mass;
  std::vector<int> *Lepton_ID;

  // List of branches
  TBranch *b_Event_Number;                    //!
  TBranch *b_Event_Weight;                    //!
  TBranch *b_Event_Weights;                   //!
  TBranch *b_Jet_Pt;                          //!
  TBranch *b_Jet_Eta;                         //!
  TBranch *b_Jet_Phi;                         //!
  TBranch *b_Jet_Mass;                        //!
  TBranch *b_Jet_HadronConeExclTruthLabelID;  //!
  TBranch *b_ForwardJet_Pt;                   //!
  TBranch *b_ForwardJet_Eta;                  //!
  TBranch *b_ForwardJet_Phi;                  //!
  TBranch *b_ForwardJet_Mass;                 //!
  TBranch *b_LargeRJet_Pt;                    //!                                                                                                                                   
  TBranch *b_LargeRJet_Eta;                   //!                                                                                                                                   
  TBranch *b_LargeRJet_Phi;                   //!                                                                                                                                   
  TBranch *b_LargeRJet_Mass;                  //! 
  TBranch *b_ReclusteredJet_Pt;               //!                                                                                                                                   
  TBranch *b_ReclusteredJet_Eta;              //!                                                                                                                                   
  TBranch *b_ReclusteredJet_Phi;              //!                                                                                                                                   
  TBranch *b_ReclusteredJet_Mass;             //!
  TBranch *b_MET;                             //!
  TBranch *b_MET_Phi;                         //!
  TBranch *b_MPT;                             //!
  TBranch *b_MPT_Phi;                         //!
  TBranch *b_MET_Sig;                         //!
  TBranch *b_MET_Sig_muInvis;                 //!
  TBranch *b_nSignalJet;                      //!
  TBranch *b_nForwardJet;                     //!
  TBranch *b_nLargeRJet;                      //!
  TBranch *b_nReclusteredJet;                 //!
  TBranch *b_NSignalTaus;                     //!
  TBranch *b_Truth_Boson_Pt;                  //!
  TBranch *b_Truth_Boson_Eta;                 //!
  TBranch *b_Truth_Boson_Phi;                 //!
  TBranch *b_Truth_Boson_Mass;                //!
  // 1 Lepton specific branches
  TBranch *b_Lepton_Pt;    //!
  TBranch *b_Lepton_Eta;   //!
  TBranch *b_Lepton_Phi;   //!
  TBranch *b_Lepton_Mass;  //!
  TBranch *b_Lepton_ID;    //!

  //ReadTruthTree(TTree *tree = 0, unsigned int AnalysisChannel = 0);
  ReadTruthTree(TChain *chain = 0, unsigned int AnalysisChannel = 0);
  virtual ~ReadTruthTree();
  virtual Int_t Cut(Long64_t entry);
  virtual Int_t GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  //virtual void Init(TTree *tree, unsigned int AnalysisChannel);
  virtual void Init(TChain *chain, unsigned int AnalysisChannel);
  virtual void Loop();
  virtual Bool_t Notify();
  virtual void Show(Long64_t entry = -1);
  virtual void Init_WeightMap(unsigned int AnalysisChannel);

  //Event weights map
  std::map<TString, float *> Event_Weights_Map;
};

#endif

#ifdef ReadTruthTree_cxx
ReadTruthTree::ReadTruthTree(TChain *chain, unsigned int AnalysisChannel)
    : fChain(0) {
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  /*
     if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ntuple.root");
        if (!f || !f->IsOpen()) {
           f = new TFile("ntuple.root");
        }
        f->GetObject("ReadTruthTree",tree);

     }
  */
  Init(chain, AnalysisChannel);
  Init_WeightMap(AnalysisChannel);
  std::cout << "Initialised weight map." << std::endl;
}

ReadTruthTree::~ReadTruthTree() {
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t ReadTruthTree::GetEntry(Long64_t entry) {
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t ReadTruthTree::LoadTree(Long64_t entry) {
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void ReadTruthTree::Init(TChain *chain, unsigned int AnalysisChannel) {
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  Event_Weights = 0;
  Jet_Pt = 0;
  Jet_Eta = 0;
  Jet_Phi = 0;
  Jet_Mass = 0;
  Jet_HadronConeExclTruthLabelID = 0;
  ForwardJet_Pt = 0;
  ForwardJet_Eta = 0;
  ForwardJet_Phi = 0;
  ForwardJet_Mass = 0;
  LargeRJet_Pt = 0;
  LargeRJet_Eta = 0;
  LargeRJet_Phi = 0;
  LargeRJet_Mass = 0;
  ReclusteredJet_Pt = 0;
  ReclusteredJet_Eta = 0;
  ReclusteredJet_Phi = 0;
  ReclusteredJet_Mass = 0;
  Truth_Boson_Pt = 0;
  Truth_Boson_Eta = 0;
  Truth_Boson_Phi = 0;
  Truth_Boson_Mass = 0;
  // Set object pointer 1 Lep
  Lepton_Pt = 0;
  Lepton_Eta = 0;
  Lepton_Phi = 0;
  Lepton_Mass = 0;
  Lepton_ID = 0;

  // Set branch addresses and branch pointers
  if (!chain) return;
  fChain = chain;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("Event_Number", &Event_Number, &b_Event_Number);
  fChain->SetBranchAddress("Event_Weight", &Event_Weight, &b_Event_Weight);
  fChain->SetBranchAddress("Event_Weights", &Event_Weights, &b_Event_Weights);
  fChain->SetBranchAddress("Jet_Pt", &Jet_Pt, &b_Jet_Pt);
  fChain->SetBranchAddress("Jet_Eta", &Jet_Eta, &b_Jet_Eta);
  fChain->SetBranchAddress("Jet_Phi", &Jet_Phi, &b_Jet_Phi);
  fChain->SetBranchAddress("Jet_Mass", &Jet_Mass, &b_Jet_Mass);
  fChain->SetBranchAddress("Jet_HadronConeExclTruthLabelID",
                           &Jet_HadronConeExclTruthLabelID,
                           &b_Jet_HadronConeExclTruthLabelID);
  fChain->SetBranchAddress("ForwardJet_Pt", &ForwardJet_Pt, &b_ForwardJet_Pt);
  fChain->SetBranchAddress("ForwardJet_Eta", &ForwardJet_Eta,
                           &b_ForwardJet_Eta);
  fChain->SetBranchAddress("ForwardJet_Phi", &ForwardJet_Phi,
                           &b_ForwardJet_Phi);
  fChain->SetBranchAddress("ForwardJet_Mass", &ForwardJet_Mass,
                           &b_ForwardJet_Mass);
  fChain->SetBranchAddress("LargeRJet_Pt", &LargeRJet_Pt, &b_LargeRJet_Pt);
  fChain->SetBranchAddress("LargeRJet_Eta", &LargeRJet_Eta, &b_LargeRJet_Eta);
  fChain->SetBranchAddress("LargeRJet_Phi", &LargeRJet_Phi, &b_LargeRJet_Phi);
  fChain->SetBranchAddress("LargeRJet_Mass", &LargeRJet_Mass, &b_LargeRJet_Mass);
  fChain->SetBranchAddress("Reclustered02TruthJet_Pt", &ReclusteredJet_Pt, &b_ReclusteredJet_Pt);       // DMM: Switch back to 04 if needed
  fChain->SetBranchAddress("Reclustered02TruthJet_Eta", &ReclusteredJet_Eta, &b_ReclusteredJet_Eta);    // DMM: Switch back to 04 if needed
  fChain->SetBranchAddress("Reclustered02TruthJet_Phi", &ReclusteredJet_Phi, &b_ReclusteredJet_Phi);    // DMM: Switch back to 04 if needed
  fChain->SetBranchAddress("Reclustered02TruthJet_Mass", &ReclusteredJet_Mass, &b_ReclusteredJet_Mass); // DMM: Switch back to 04 if needed
  fChain->SetBranchAddress("MET", &MET, &b_MET);
  fChain->SetBranchAddress("MET_Phi", &MET_Phi, &b_MET_Phi);
  fChain->SetBranchAddress("MPT", &MPT, &b_MPT);
  fChain->SetBranchAddress("MPT_Phi", &MPT_Phi, &b_MPT_Phi);
  fChain->SetBranchAddress("MET_Sig", &MET_Sig, &b_MET_Sig);
  fChain->SetBranchAddress("MET_Sig_muInvis", &MET_Sig_muInvis, &b_MET_Sig_muInvis);
  fChain->SetBranchAddress("nSignalJet", &nSignalJet, &b_nSignalJet);
  fChain->SetBranchAddress("nForwardJet", &nForwardJet, &b_nForwardJet);
  fChain->SetBranchAddress("nLargeRJet", &nLargeRJet, &b_nLargeRJet);
  fChain->SetBranchAddress("nReclustered02Jet", &nReclusteredJet, &b_nReclusteredJet); // DMM: Switch back to 04 if needed
  fChain->SetBranchAddress("NSignalTaus", &NSignalTaus, &b_NSignalTaus);
  fChain->SetBranchAddress("Truth_Boson_Pt", &Truth_Boson_Pt,
                           &b_Truth_Boson_Pt);
  fChain->SetBranchAddress("Truth_Boson_Eta", &Truth_Boson_Eta,
                           &b_Truth_Boson_Eta);
  fChain->SetBranchAddress("Truth_Boson_Phi", &Truth_Boson_Phi,
                           &b_Truth_Boson_Phi);
  fChain->SetBranchAddress("Truth_Boson_Mass", &Truth_Boson_Mass,
                           &b_Truth_Boson_Mass);
  // Set 0 Lepton specific branches
  if (AnalysisChannel == 0) {
  }
  // Set 1 Lepton specific branches
  if (AnalysisChannel == 1 || AnalysisChannel == 2) {
    fChain->SetBranchAddress("Lepton_Pt", &Lepton_Pt, &b_Lepton_Pt);
    fChain->SetBranchAddress("Lepton_Eta", &Lepton_Eta, &b_Lepton_Eta);
    fChain->SetBranchAddress("Lepton_Phi", &Lepton_Phi, &b_Lepton_Phi);
    fChain->SetBranchAddress("Lepton_Mass", &Lepton_Mass, &b_Lepton_Mass);
    fChain->SetBranchAddress("Lepton_ID", &Lepton_ID, &b_Lepton_ID);
  }
  // Set 2 Lepton specific branches
  if (AnalysisChannel == 2) {
  }
  Notify();
}

//Initialise weight map
void ReadTruthTree::Init_WeightMap(unsigned int AnalysisChannel) {
  std::cout << "<ReadTruthTree::Init_WeightMap>::   Initialising Event Weight "
               "Map (Variation <-> Weight) "
            << std::endl;
  //Print for diagnostic reasons
  //std::cout << "<ReadTruthTree::Init_WeightMap>::   fChain Truth Tree File: " << loadFile->GetTitle() << std::endl;

  //Storage element for sum of weights histograms
  std::vector<TH1F *> vec_h_SumW;

  //Get the list of files from the TChain/TTree
  TObjArray *fileElements = fChain->GetListOfFiles();
  TIter next(fileElements);
  TChainElement *chEl = 0;
  while ((chEl = (TChainElement *)next())) {
    std::cout << "<ReadTruthTree::Init_WeightMap>::   Chain Element = "
              << chEl->GetTitle() << std::endl;
    //Load the file and open
    TFile loadFile(chEl->GetTitle());

    //Find the sum of weights histogram
    TH1F *h_SumW;
    TString SumWName = "h_Keep_EventCount_";
    //Loop through file keys and find the sum weights histogram
    TIter next(loadFile.GetListOfKeys());
    TKey *key;
    while ((key = (TKey *)next())) {
      TClass *cl = gROOT->GetClass(key->GetClassName());
      if (!cl->InheritsFrom("TH1")) {
        continue;
      }
      TString HistName = key->ReadObj()->GetName();
      if (!HistName.Contains(SumWName)) {
        continue;
      }
      //Save histogram into vector for checks
      std::cout << "<ReadTruthTree::Init_WeightMap>::   Found sum of weights "
                   "histogram : "
                << HistName << std::endl;
      vec_h_SumW.push_back((TH1F *)key->ReadObj());
    }
    vec_h_SumW.back()->SetDirectory(0);
    //Now Close file
    loadFile.Close();
  }

  //Check that all files stored inside TChain contain the same weights
  //TO DO

  // Store the sum of weights and variation names
  std::vector<TString> variationNames;
  for (int i = 1; i <= vec_h_SumW.front()->GetNbinsX(); ++i) {
    if (strlen(vec_h_SumW.front()->GetXaxis()->GetBinLabel(i)) == 0) {
      continue;  //Want to skip variations which don't exist
    }
    TString varName_temp = vec_h_SumW.front()->GetXaxis()->GetBinLabel(i);
    varName_temp = varName_temp.ReplaceAll(" ", "");
    std::cout << "<ReadTruthTree> " << varName_temp << " "
              << vec_h_SumW.front()->GetBinContent(i) << std::endl;
    if (strlen(varName_temp) == 0) {
      continue;  // Want to skip variations which don't exist
    }
    if (!vec_h_SumW.front()->GetBinContent(i)) {
      //Do nothing
    } else {
      std::cout << "New variation name: " << varName_temp << std::endl;
      variationNames.push_back(varName_temp);
    }
  }

  //Must now initialise the first element of the TChain before
  //one can assign a pointer
  if (fChain->GetEntries() > 0) {
    fChain->GetEntry(0);
  }

  if (!Event_Weights->empty()) {
    //Now load the map with the var name and the pointer address to the weight
    for (int varIndex = 0; varIndex < variationNames.size(); varIndex++) {
      if (Event_Weights_Map.find(variationNames.at(varIndex)) ==
          Event_Weights_Map.end()) {
        Event_Weights_Map.insert(std::pair<TString, float *>(
            variationNames.at(varIndex), &(Event_Weights->at(varIndex))));
      }
    }
  } else {
    std::cout << "<ReadTruthTree> WARNING empty event_weights vector"
              << std::endl;
  }
}

Bool_t ReadTruthTree::Notify() {
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void ReadTruthTree::Show(Long64_t entry) {
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t ReadTruthTree::Cut(Long64_t entry) {
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
#endif  // #ifdef ReadTruthTree_cxx
