#ifndef histSvc_H
#define histSvc_H

#include <map>
#include <string>
#include <vector>

#include "TruthFramework_NtupleToHist/Selection.h"
#include "TChain.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "generateHistogram_MonoSWW.h"

class histSvc {
 public:
  struct WeightSyst {
    string name;
    float factor;
  };

  void BookFillHist(TString varName, int nBins, float xmin, float xmax,
                    float value, std::vector<float>* weights, float ExtraWeight,
                    std::vector<Selection> selections = {});
  void BookFillHist(TString varName, int nBins, float xmin, float xmax,
                    float value, std::map<TString, float*>* weights,
                    float ExtraWeight, std::vector<Selection> selections = {});
  void BookFillHist(TString varName, int nBinsX, float xmin, float xmax,
                    float xvalue, int nBinsY, float ymin, float ymax,
                    float yvalue, std::vector<float>* weights,
                    float ExtraWeight, std::vector<Selection> selections = {});
  void BookFillTree(std::vector<float>* weights,float ExtraWeight, float mS_candidate, float MET, float pTV);

  void SetCutBase(bool cutbase);
  void SetAnalysisType(generateHistogram::AnalysisType type);
  void SetRegion(int region);
  void SetProcess(int DSID, std::string sample);
  void SetEventFlav(int flav_1, int flav_2);
  void get_eventFlavour(int& flav_1, int& flav_2);
  void SetNjets(int njets);
  void SetNtags(int ntags);
  void SetDescription(TString description);
  void SetpTV(float ptv);
  void FillCutflow(int bin, float weight, TString name);
  void WriteHists();
  void SetLumi(float lumi);
  void SetXSec(float xsec);
  void SetSumWeights(std::vector<float> sumweights);
  void SetSumWeightsMap(std::map<TString, float> SumWeightsMap);
  void SetWeightNames(std::vector<TString> variationNames);
  void ApplySherpaWeightCut(bool SherpaWeightCut);
  void CutSherpaWeight(std::vector<float>* weights);
  void CutSherpaWeight(std::map<TString, float*>* weights);
  void Reset();
  void SetComment(TString comment) { m_comment = comment; }
  float CorrectionFactor(int variation = 0);
  float CorrectionFactor(TString variation = "nominal");

  std::vector<WeightSyst> m_weightVariations;

  histSvc(TString outputPath, bool nominal);
  ~histSvc();

 private:
  TString GetHistoName(TString variation, TString varName, bool do_ptv=true);
  TString TruthFlavLabel();
  TString m_outputPath;
  bool m_nominal;
  bool m_CutBase;
  bool m_SherpaWeightCut = false;
  int m_DSID;
  int m_flav_1, m_flav_2;
  int m_njets;
  int m_ntags;
  float m_ptv;
  TString m_description;
  TString m_comment;

  float m_Lumi;
  float m_XSec;

  int m_counter;

  //Option a): Default and historical implementation of sum
  //           of weights and
  std::vector<float> m_SumWeights;
  std::vector<TString> m_VariationNames;

  //Option b): Use a map of weights, thereby containing both
  //           the variation name and the sum of weights for
  //           said variation
  std::map<TString, float> m_SumWeightsMap;

  TH1F* m_cutflow_Unweighted =
      new TH1F("cutflow_Unweighted", "cutflow_Unweighted", 12, -0.5, 11.5);
  TH1F* m_cutflow_EventWeight =
      new TH1F("cutflow_EventWeight", "cutflow_EventWeight", 12, -0.5, 11.5);
  TH1F* m_cutflow_FullWeight =
      new TH1F("cutflow_FullWeight", "cutflow_FullWeight", 12, -0.5, 11.5);

  TTree* m_tree = new TTree("minitree","test tree");

  generateHistogram::AnalysisType m_type;
  int m_Region;
  std::string m_sample;
  std::vector<TH1F*> m_histograms;
  std::vector<TH2F*> m_histograms_2d;
  std::map<TString, int> m_histoMap;
  std::map<TString, int> m_histoMap_2d;
};

#endif
