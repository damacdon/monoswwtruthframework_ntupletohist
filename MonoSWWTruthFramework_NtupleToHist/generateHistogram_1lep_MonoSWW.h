#ifndef generateHistogram_1lep_H
#define generateHistogram_1lep_H

#include <map>
#include <string>
#include <vector>

#include "ReadTruthTree_MonoSWW.h"
#include "TChain.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TString.h"

#include "generateHistogram_MonoSWW.h"

class generateHistogram_1lep : public generateHistogram {
 public:
  //Bit flag event selection
  static void EventSelection(ReadTruthTree &readNtuple,
                             EventObjects &PhysicsObjects,
                             unsigned long &eventFlag, bool Debug = false, int Region = 0);

  //Build the objects for the event
  static bool BuildEvent(ReadTruthTree &readNtuple,
                         EventObjects &PhysicsObjects, btagTool &BtagTool,
                         bool Debug = false, int Region = 0);

  //Function for build BDT variables (MVAsystTraining.cxx)
  // static void BuildBDTVars(ReadTruthTree &readNtuple,
                           // EventObjects &PhysicsObjects, bool Debug = false);

  //Select events for histogram plotting
  void Selection1Lepton();

  // Enumerated values for 1-lepton cutflow
  enum OneLeptonCutFlow {
    Input_events,
    Lepton_is_muon,
    Lepton_pt,
    MET_muInv,
    TauVeto,
    MinDPhiMETJets,
    MET_Sig,
    mS_window,
    AnalysisRegion,
    pTVgreaterthan150GeV,
  };

  virtual void FillHistograms() override;

  static float CalculateMtop(const TLorentzVector &lepton,
                             const TLorentzVector &MET,
                             const TLorentzVector &b_jet1,
                             const TLorentzVector &b_jet2);

  static float CalculatedYWH(const TLorentzVector &lepton,
                             const TLorentzVector &MET,
                             const TLorentzVector &b_jet1,
                             const TLorentzVector &b_jet2);
  //  TChain* m_inputChain;
  //  ReadTruthTree* readNtuple;

  void FillCutFlow1lep(const unsigned long int flag, float eventWeight,
                       float btagWeight);

  generateHistogram_1lep();
  ~generateHistogram_1lep();

 private:
};

#endif
