#ifndef generateHistogram_H
#define generateHistogram_H
#include <map>
#include <string>
#include <vector>

#include "ReadTruthTree_MonoSWW.h"
#include "TChain.h"
#include "TH1.h"
#include "TH2.h"
#include "TString.h"
//#include "histSvc.h"
#include "TLorentzVector.h"

//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// EDM
#include "xAODEventInfo/EventInfo.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticle.h"

#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODTruth/TruthMetaData.h"
#include "xAODTruth/TruthMetaDataContainer.h"

#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "METInterface/IMETMaker.h"
#include "METInterface/IMETSignificance.h"
#include "METUtilities/CutsMETMaker.h"
#include "METUtilities/METHelpers.h"
#include "AsgTools/AnaToolHandle.h"

using xAOD::IParticle;
typedef ElementLink<xAOD::IParticleContainer> iplink_t;
typedef DataVector< IParticle > IParticleContainer;

class btagTool;

class histSvc;

class InfoProvider;

//Structure form storing event TLorentzVector objects
struct EventObjects {
  //Leptons
  TLorentzVector LeptonVec1, LeptonTransVec1;
  TLorentzVector LeptonVec2, LeptonTransVec2;
  int Lepton1Flav, Lepton2Flav;
  //MET
  TLorentzVector MET_Vec;
  // Resolved Jets
  TLorentzVector jet1vec, jet2vec, jet3vec, jet4vec;
  // Large-R Jets
  TLorentzVector largeRjet1vec, largeRjet2vec, largeRjet3vec;
  // Reclustered Jets
  TLorentzVector Reclusteredjet1vec, Reclusteredjet2vec, Reclusteredjet3vec;
  // Vector Boson
  TLorentzVector WTransVec;
  TLorentzVector ZVec;
  //FN Dark Higgs candidate
  TLorentzVector DarkHiggs_resolved;
  TLorentzVector DarkHiggs_intermediate;
  TLorentzVector DarkHiggs_merged;
  // Dark Higgs candidate mass
  float mS_candidate;
  // Higgs Boson
  // DMM: This should become obsolete eventually...
  TLorentzVector Higgs;
  // W Boson
  TLorentzVector WBoson;
  // Signal Jets
  std::vector<TLorentzVector> signalJets;
  // all Jets
  std::vector<TLorentzVector> allJets; // FN
  // Signal Jet Flavour
  std::vector<int> signalJetFlavour;
  // all Jet Flavour
  std::vector<int> allJetFlavour;
  // Tagged jet indices
  std::vector<unsigned int> taggedJets;
  //Selected Jets
  TLorentzVector jet1vecSel, jet2vecSel,
    jet3vecSel, jet4vecSel;  // Now set the output of compute_btagging to identify the b-tagged jets. DMM: Currently updated so these just correspond to the 4 highest-pT jets. 
  //b-tag weight
  float btagWeight = -99;
  //Event Weight
  float Event_Weight = -99;
  // njets
  int nJets = -99;
  // n Large-R Jets
  int nLargeRJets = -99.;
  // n Reclustered Jets
  int nReclusteredJets = -99.;
  // min delta phi between MET and one of the jets
  float mindPhi = -99;

  // BDT variables
  // Mass of two leading b-jets
  float mBB = -99;

  // Mass of candidate W boson
  float mW = -99;

  //Transverse V-boson momentum
  float pTV = -99;

  //Missing Transverse Energy
  float MET = -99;

  // Missing Transverse Energy for invisible muon decay
  TLorentzVector MET_muInv_Vec;
  float MET_muInv = -99;

  //Lepton pT
  float pTL = -99;

  // mass of top candidate
  float mTop = -99;

  // minimum dPhi between lepton and b-jet
  float dPhiLBmin = -99;

  // minimum dPhi between lepton and signal jet
  float dPhiLJ1min = -99;

  //dPhi between lepton and b-jet
  float dPhiVBB = -99;

  // dPhi between lepton and leading 2 jets
  float dPhiVJJ = -99;

  //dR between leading signal jets
  float dRJJ = -99;

  // dR between leading b-jets
  float dRBB = -99;

  // dEta between leading signal jets
  float dEtaJJ = -99;

  // dEta between leading b-jets
  float dEtaBB = -99;

  // Transverse mass of W-boson
  float mTW = -99;

  //Leading jet pT
  float pTJ1 = -99;

  // Leading b-jet pT
  float pTB1 = -99;

  // Sub-Leading jet pT
  float pTJ2 = -99;

  // Sub-leading b-jet pT
  float pTB2 = -99;

  //3-jet invariant mass
  float mBBJ = -99;

  // 3-jet invariant mass with no b-tags
  float mJJJ = -99;

  // dY (rapidity) between Higgs and V-boson
  float dYWH = -99;

  // 3rd Jet pT
  float pTJ3 = -99;

  //Distance in eta of BB from Vector boson
  float dEtaVBB = -99;

  // Distance in eta of JJ from Vector boson
  float dEtaVJJ = -99;

  //Di-lepton mass
  float mLL = -99;

  // MET_Sig
  float MET_Sig = -99;
  float MET_Sig_muInvis = -99;

  //Event Number
  float Event_Number = -99;

  // Define event truth flavour
  float EvtFlav = -99;

  //nJets for BDT (Float)
  float numJets = -99;

  // Ht
  float sumjetpt = -99;

  // Desinged for assinging event flavour
  float AssignEventFlavour();
};

class generateHistogram {
 public:
  // Samples
  enum AnalysisType { ZeroLepton, OneLepton, TwoLepton };

  enum AnalysisRegion {
    AllJets,
    OneJet,
    TwoJetSR,
    ThreeJetSR,
    TwoJetCR,
    ThreeJetCR,
    ThreePJet,
    LowPT2jSR,
    LowPT3jSR,
    HighPT2jSR,
    HighPT3jSR
  };

  virtual void GenerateHistogram(AnalysisType type, int DSID);

  virtual void FillHistograms();

  std::vector<AnalysisRegion> GetRegion(AnalysisType type);

  TString GetOutputPath();

  void SetInputPath(TString path);
  void SetOutputPath(TString path);
  void SetNEvents(int events);
  void SetDebug(bool debug);
  void SetNominalOnly(bool nominal);
  void SetLumi(float lumi);
  void SetTotalSumWDSID(bool TotalSumWDSID);
  void ApplySherpaWeightCut(bool SherpaWeightCut);
  void SetRegion(int iRegion);

  //cutflow related functions
  static void updateFlag(unsigned long int &flag,
                         const unsigned long int cutPosition,
                         const unsigned long int passCut = 1);
  unsigned long int bitmask(
      const unsigned long int cut,
      const std::vector<unsigned long int> &excludeCuts = {});
  bool passAllCutsUpTo(const unsigned long int flag,
                       const unsigned long int cut,
                       const std::vector<unsigned long int> &excludeCuts = {});
  bool passSpecificCuts(const unsigned long int flag,
                        const std::vector<unsigned long int> &cuts);

  unsigned int channelNumber(AnalysisType type);
  TH1F *getTotalSumWDSID(TString inputPath, int DSID);

  AnalysisType m_type;

  int m_nEvents = -1;
  int m_DSID = -1;
  int m_Debug = false;
  int m_NominalOnly = false;
  float m_Lumi = 1;
  int m_TotalSumWDSID = false;
  int m_Region = 0;
  int* region = 0;

  std::vector<float> m_SumWeights;
  float m_XSec = 0;
  TChain *m_inputChain;

  //virtual bool BuildEvent(ReadTruthTree *readNtuple) = 0;
  EventObjects m_PhysicsObjects;

  TString m_outputPath = "./";
  TString m_inputPath = "./";
  bool m_SherpaWeightCut = false;

  ReadTruthTree *readNtuple;
  histSvc *m_histFill = NULL;
  btagTool *m_BtagTool = NULL;
  InfoProvider *m_InfoTool = NULL;

  void InitializeInfoTool();
  void InitializeBTagTool(int DSID);
  void InitialiseChain(TString ChainName);
  void InitialiseHistFill();

  generateHistogram();
  ~generateHistogram();

 private:
  void FileLoader(AnalysisType type, int DSID);
  void StoreConfig(AnalysisType type, int DSID);
};

#endif
