#ifndef btagTool_MonoSWW_H
#define btagTool_MonoSWW_H

#include <map>
#include <string>
#include <vector>

#include "TChain.h"
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"
#include "TString.h"

class btagTool {
 public:
  std::vector<unsigned int> compute_btagging(
      std::vector<TLorentzVector>& signalJets,
      std::vector<int>& signalJetFlavour, float& btagWeight);

  void compute_btagweight(
      std::vector<TLorentzVector>& signalJets,
      std::vector<int>& signalJetFlavour, float& btagWeight);

  btagTool(std::string shower);
  ~btagTool();

 private:
  std::string m_Shower;
  void LoadTruthMaps();
  int roll_dice(const std::vector<float>& choices);
  float getTagEff(TLorentzVector jet, int jetFlav);

  TH2D* hTruthMap_b = NULL;
  TH2D* hTruthMap_c = NULL;
  TH2D* hTruthMap_l = NULL;
  TH2D* hTruthMap_t = NULL;
};

#endif
