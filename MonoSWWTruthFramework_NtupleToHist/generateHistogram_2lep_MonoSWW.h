#ifndef generateHistogram_2lep_H
#define generateHistogram_2lep_H

#include <map>
#include <string>
#include <vector>

#include "MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h"
#include "TChain.h"
#include "TH1.h"
#include "TLorentzVector.h"
#include "TString.h"

#include "generateHistogram_MonoSWW.h"

class generateHistogram_2lep : public generateHistogram {
 public:
  //friend class MVASystTraining;//Needed for MVA training
  //Bit flag event selection
  static void EventSelection(ReadTruthTree &readNtuple,
                             EventObjects &PhysicsObjects,
                             unsigned long &eventFlag, bool Debug = false, int Region = 0);

  //Build the objects for the event
  static bool BuildEvent(ReadTruthTree &readNtuple,
                         EventObjects &PhysicsObjects, btagTool &BtagTool,
                         bool Debug = false, int Region = 0);

  //Function for build BDT variables (MVAsystTraining.cxx)
  // static void BuildBDTVars(ReadTruthTree &readNtuple,
                           // EventObjects &PhysicsObjects, bool Debug = false);
  //Select events for histogram plotting
  void Selection2Lepton();

  enum TwoLeptonCutFlow {
    Input_events,
    Two_signal_lepton,
    TauVeto,
    ptllGt200,
    Z_mass_selection,
    MinDPhiMETJets,
    MET_Sig,
    mS_window,
    AnalysisRegion,
  };

  virtual void FillHistograms() override;

  void FillCutFlow2lep(const unsigned long int flag, float eventWeight,
                       float btagWeight);

  generateHistogram_2lep();
  ~generateHistogram_2lep();

 private:
};

#endif
