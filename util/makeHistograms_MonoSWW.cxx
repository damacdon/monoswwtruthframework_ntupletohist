#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_0lep_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_1lep_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_2lep_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h"
#include <vector>
#include <iostream>
#include <ctime>

// This macro takes ntuples produced using the VHbb truth analysis framework as
// inputs (with a very loose event selection)
// The macro will then apply:
// * The event selection of your chosen analysis
// * Muon in jet correction (being added)
// * Truth b-jet tagging (considering all jets)
// *

int main(int argc, char* argv[]) {

  clock_t begin = std::clock();

  if (argc > 1) {
    if (strcmp(argv[1], "--help") == 0) {
      std::cout << "./NtupleToHist <sample to run> <output directory>"
                << std::endl;
      std::cout << "Within makeHistograms.cxx, specify input path, number of "
                   "events, luminosity and other options" << std::endl;
      exit(0);
    }
  }

  // Default analysis
  unsigned int analysisChannel = 0;
  if (argc > 1) analysisChannel = (std::atoi(argv[1]));
  TString inputPath;
  if (argc > 2) {
    inputPath = argv[2];
  } else {
    // Default input paths
    // Andy
    // inputPath = "/unix/atlascxaods/TruthTuples";
    // inputPath =
    // "/unix/atlas3/abell/VHbbTruthAnalysis_forGit/VHbbTruthFramework/WorkArea/run/user.abell.mc15_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v01_EXT0/user.abell.12499872.EXT0._000001.ntuple.root";
    // inputPath =
    // "/unix/atlascxaods/TruthTuples/user.abell.mc15_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v01_EXT0/user.abell.12499872.EXT0._000001.ntuple.root");
    // inputPath =
    // "/unix/atlas3/abell/user.abell.mc15_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.evgen.EVNT.e5340.VHbb.BETA_EXT0";
    // inputPath =
    // "/eos/user/a/abell/TruthTuples_BETA_02/user.abell.mc15_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v01.04_EXT0/user.abell.12565397.EXT0._000001.ntuple.root";
    // inputPath = "/unix/atlas3/abell/TruthTuples_v05/";
    // inputPath =
    // "/unix/atlas3/abell/TruthTuples_v05/group.phys-higgs.mc15_13TeV.345056.PowhegPythia8EvtGen_vvbb_VpT.evgen.EVNT.e5706.VHbb.BETA_v05.NOSMEAR_ZvvH_SYST_EXT0/group.phys-higgs.12749544.EXT0._000001.ntuple.root";
    // inputPath =
    // "/unix/atlas3/abell/TruthTuples_SIMONA/group.phys-higgs.mc15_13TeV.364162.Sherpa_MAXHTPTV140_280_CVetoBVeto.evgen.EVNT.e5340.VHbb.BETA_v01.SMEAR_SIMONA_EXT0/group.phys-higgs.12702005.EXT0._000217.ntuple.root";

    // Luca
    // inputPath =
    // "/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056/group.phys-higgs/group.phys-higgs.12749544.EXT0._000001.ntuple.root";
    // inputPath =
    // "/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/group.phys-higgs.mc15_13TeV.410501.PowhegPythia8EvtGen_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v01.SMEAR_LUCA_EXT0/group.phys-higgs.12636418.EXT0._000001.ntuple.root";
    // inputPath = "/home/danikam1/data/MonoSWW/TruthLevel_ntuples/mc16_13TeV.310773.MadGraphPy8EG_A14NNP23LO_monoSww_all_zp1000_dm200_dh160.merge.EVNT.e7040_e5984/0_Lep.root";
    // inputPath = "/home/danikam1/data/MonoSWW/TruthLevel_ntuples/user.damacdon.mc16_13TeV.364160.Sherpa_Wmunu_MAXHTPTV70_140_CFilterBVeto.merge.EVNT.e5340_e5984.monoS_sigs_0_Lep";
    inputPath="/afs/cern.ch/work/f/fnapolit/private/";
  }
  // Default DSID
  // unsigned int DSID = 410501;
  // unsigned int DSID = 345056;
  // unsigned int DSID =   310773;
  // unsigned int DSID = 361518;
  unsigned int DSID = 366017;
  bool applySherpa = false;
  if (argc > 3) DSID = (std::atoi(argv[3]));
  TString outputPath = "./";
  if (argc > 4) outputPath = argv[4];
  if (argc > 5) applySherpa = (std::atoi(argv[5]) == 1);

  generateHistogram* config;

  switch (analysisChannel) {
    case(0) :
      config = new generateHistogram_0lep();
      break;
    case(1) :
      config = new generateHistogram_1lep();
      break;
    case(2) :
      config = new generateHistogram_2lep();
      break;
  }

  config->SetInputPath(inputPath);
  config->SetOutputPath(outputPath);  // Set the output path for histograms
  config->SetNEvents(-1);   // Number of events to run over, -1 = all events
  //config->SetNEvents(100000);   // Number of events to run over, -1 = all events
  config->SetDebug(false);  // Run in debug mode
  config->SetNominalOnly(false);  // Run with weight variations (i.e Sherpa)
  config->SetLumi(139.0);         // Set the luminosity to normalise MC to, in fb
  config->SetTotalSumWDSID(false);  // Set true only if you run on a batch each
                                    // root file separately, otherwise keep it
                                    // false. Default value is false.
  config->ApplySherpaWeightCut(applySherpa);  // Set true only if you want to
                                              // set the weight of events with
                                              // abs(weight) > 100 to +/- 1
  // config->WriteMVATree(false);
  for(int iRegion = 0; iRegion < 3; iRegion++) {
    config->SetRegion(iRegion);                       // Set the analysis region. 0: merged. 1: intermediate. 2: resolved
    std::cout << "-------> Analysis Region: " << iRegion << std::endl;
    switch (analysisChannel) {
      case(0) :
	config->GenerateHistogram(generateHistogram::ZeroLepton,
                                DSID);  // DSID you want to plot
	break;
      case(1) :
	config->GenerateHistogram(generateHistogram::OneLepton, DSID);
	break;
      case(2) :
	config->GenerateHistogram(generateHistogram::TwoLepton, DSID);
	break;
    }
  }

  clock_t end = clock();
  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

  std::cout << "Total running time: " << elapsed_secs << std::endl;
}
