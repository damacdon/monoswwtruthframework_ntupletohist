# Getting started - download the code

First, make a directory for the ntuple reading code
Within this directory, create build, source and run folders

```bash
mkdir build source run

cd source
```

Checkout the git repository

```bash
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/damacdon/monoswwtruthframework_ntupletohist.git
```

## Time to compile the code

```bash
cd ../build

setupATLAS

lsetup asetup

asetup AnalysisBase,21.2.15,here

cp CMakeLists.txt ../source

cmake ../source

cmake --build .
```

Add the executable

```bash
source `eval echo \\${${AtlasProject}_PLATFORM}`/setup.sh
```

## Everytime you log in

```bash
source source/monoswwtruthframework_ntupletohist/scripts/setup.sh
```


# Running the NtupleToHist code 

## To run over a single root file:

```bash
NtupleToHist_MonoSWW CHANNEL /path/to/ntuple.root DSID /path/to/output/directory APPLY_SHERPA 
```

will run over the input root ntuple `/path/to/ntuple.root` and produce a histrogram and minitree for each MonoSWW analysis category in the output directory `/path/to/output/directory`.

where:
* `CHANNEL` represents the number of leptons in the final state, so can be 0, 1, or 2.
* DSID is the dataset ID (eg. 366031). 
* APPLY_SHERPA is a boolean variable (0 or 1). Set to 0 (false) by default. If set to 1 (true), it will set any weights of Sherpa-generated events above 100 to 1. 

## To run over multiple root files in the same directory:

```bash
NtupleToHist_MonoSWW CHANNEL /path/to/directory/above/directory/containing/root/files DSID /path/to/output/directory APPLY_SHERPA 
```

Eg. If the root files are located in directory `/data/user.damacdon.mc16_13TeV.366030.Sh_PTV100_140_MJJ500_1000_CVetoBVeto.merge.EVNT.e7033_e5984.monoS_all_0_Lep`, the executable could be run as:

```bash
NtupleToHist_MonoSWW 0 /data 366030 output_dir [APPLY_SHERPA]
```

Note that the name of the directory containing the root files must include the DSID for this to work. 

## To run over all data:

If you have multiple ntuple containers (eg. `user.damacdon.mc16_13TeV.366030.Sh_PTV100_140_MJJ500_1000_CVetoBVeto.merge.EVNT.e7033_e5984.monoS_all_0_Lep`) stored under the same directory, the script `run_all_ntuples.py` can be used to produce a histogram file for each container, and output the histogram file to a sub-directory according to the structure in the input json config file:

```bash
cd source/monoswwtruthframework_ntupletohist/scripts/run_ntuples
python run_all_ntuples.py -i /path/to/all/data -o /path/to/top/level/output -c /path/to/json/config
```

where the input path should point to the directory containing the the containers, and the `-c` config argument is optional (defaults to the config file `data/inputConfig_config.json`).

# Adding new samples 

For each new sample (as by the DSID), the txt file `data/SampleInformation_13TeV.txt` must also be updated to tell the code which Parton Shower was used in the generation.

# Calculating V+jets and signal systematics

The script `compareVariations_monoS.py` in the `scripts/signal_variations` directory is used to:

* Perform bin-based reweighting of MET proxy variables (MET in the signal region, MET (&mu; invisible) in the 1-lepton control region, and dilepton p<sub>T</sub> in the 2-lepton control region) to match the distributions of MET proxy variables in the Madgraph-Pythia8 generated W+jets and Z+jets backgrounds to the equivalent distributions for Sherpa-generated samples. 
* Evaluate the two-sided shape and acceptance modeling systematics associated with scale, PDF, and 2-point (i.e. generator) variations for the Z+jets and W+jets backgrounds, and the scale and PDF systematics for signals.

The script takes two required input arguments, namely:
* the path (-d/--hists_dir) to the directory containing the ROOT hists and minitrees that it runs over, and 
* The name (-n/--sample_name) of the sample to consider. Can be either a dsid or the name of a combined sample (eg. 363643 or Zjets).

There are many other optional arguments - run `compareVariations_monoS.py` without any input arguments to see a printout describing all possible arguments. Run `compareVariations_monoS.py` without any input arguments to see the full printout of command-line options:

```bash
./compareVariations_monoS.py
```

## Set up the environment

The python scripts used to evaluate the systematics use Python's root_numpy module. 

If working on a machine with cvmfs (eg. lxplus), source the environment variables needed to use root_numpy as follows (after setting up `AnalysisBase,21.2.15` as described above):

```bash
export LCGENV_PATH=/cvmfs/sft.cern.ch/lcg/releases
/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p LCG_95 --ignore Grid x86_64-slc6-gcc49-opt root_numpy > lcgenv.sh
source lcgenv.sh
```

If working in a docker container started from the [AnalysisBase,21.2.15 image](https://hub.docker.com/layers/atlas/analysisbase/21.2.15/images/sha256-24ec6cb11b01b5c880c4063d5ca958aa5db07c24c6da06c33722ce7f27fff13d), install root_numpy as follows:

```bash
pip install --user root_numpy
pip install --upgrade --force-reinstall --user numpy
```

## Combining V+jets ntuples and minitrees

Before using `compareVariations_monoS.py` to estimate the systematics, the bash script `combine_Vjets.sh` needs to be run to hadd together the Z+jets and W+jets ntuples and minitrees that were produced by NtupleToHist for individual dsids into ntuples and minitrees for combined final states (eg. Zjets and Wjets, and their constituent final-states Zee, Zmumu, Wenu, etc.):

```bash
./combine_Vjets.sh [path/to/directory/containing/ntuples/and/minitrees/produced/by/NtupleToHist] [directory/to/output/hadded/samples/to]
```

## Evaluating signal systematics

The scale systematics for a given dsid `DSID` located in the directory `INPUT_DIR` can be evaluated as follows:

```bash
./hadd_signal.sh [path/to/directory/containing/hists/and/minitrees/produced/by/NtupleToHist] [DSID]
./compareVariations_monoS.py -d [path/to/directory/containing/hists/and/minitrees/produced/by/NtupleToHist] -n [DSID] -m [METBIN]
```

where METBIN specifies the index for the MET bin to use (0: 200-300, 1: 300-500, 2: 500-inf, 3: inclusive)

This will produce plots visualizing the scale and PDF variations in the `all_plots` directory, and text and yaml files listing the symmetrized systematics (reported as percents) in the `output_files` directory.

To run over all signal DSIDS:

```bash
./compareVariations_signals.sh [path/to/directory/containing/hists/and/minitrees/produced/by/NtupleToHist]
```

To combine all yaml files containing signal systematics, run:

```bash
python combine_all_yml_signal.py
```

To combine the yaml files for a given signal (eg. `ww_zp1000_dm200_dh160`) in a form that can be read in for the recast framework (basically just replacing the signal name in the yaml file with `recast`), run:

```bash
python combine_all_yml_recast.py [signal name]
```

eg. 

```bash
python combine_all_yml_recast.py ww_zp1000_dm200_dh160
```

## Evaluating V+jets systematics

If applying bin-based MET proxy reweighting to the Madgraph-Pythia8 samples (recommended), do so for each sample `SAMPLE_NAME` (Zjets and Wjets) as follows:

```bash
./compareVariations_monoS.py -d [INPUT_DIR] -n [SAMPLE_NAME] -m [METBIN] --reweight
```

This will produce reweighted Madgraph-Pythia8 ROOT hists in the `INPUT_DIR` directory (ending in '_reweighted.root'). Now, the `-r/--use_reweighted_mgpy` option can be used in subsequent calls to `compareVariations_monoS.py` to use the reweighted Madgraph-Pythia8 hists. The V+jets samples (eg. Zjets) can now be evaluated with bin-based reweighting as follows:

```bash
./compareVariations_monoS.py -d [INPUT_DIR] -n [SAMPLE_NAME] -m [METBIN] --use_reweighted_mgpy
```

This will produce plots in the `all_plots` directory visualizing the variations, and text and yaml files listing the scale, PDF, and 2-point systematics in the `output_files` directory. To combine all the yaml files produced containing V+jets systematics, run:

```bash
python combine_all_yml_Vjets.py
```