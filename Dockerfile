# Specify the image from which you are working
FROM atlas/analysisbase:21.2.15

USER atlas

# Download the root_numpy and numpy python modules needed by code
RUN source ~/release_setup.sh && \
    pip install --user root_numpy && \
    pip install --upgrade --force-reinstall --user numpy

# Put the current repo (the one in which this Dockerfile resides) in the directory specified here
# Note that this directory is created on the fly and does not need to reside in the repo already
ADD . /jdm/monoS_truth/source/monoswwtruthframework_ntupletohist
WORKDIR /jdm/monoS_truth/build

# Execute this comment in the directory specified just above
RUN source ~/release_setup.sh && \
    sudo chown -R atlas /jdm && \
    mkdir -p /jdm/monoS_truth/run && \
    cp ../source/monoswwtruthframework_ntupletohist/CMakeLists_source.txt ../source/CMakeLists.txt && \
    cmake ../source && \
    cmake --build .


