#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/histSvc_MonoSWW.h"
#include "TruthFramework_NtupleToHist/InfoProvider.h"
#include "MonoSWWTruthFramework_NtupleToHist/btagTool_MonoSWW.h"
#include <PathResolver/PathResolver.h>

#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TObjArray.h>
#include <TRegexp.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <vector>
#include "TChain.h"
#include "TLorentzVector.h"
#include "TRandom3.h"

generateHistogram::generateHistogram()
    : m_nEvents(-1),
      m_DSID(-1),
      m_Debug(false),
      m_NominalOnly(false),
      m_Lumi(1000),
      m_XSec(0),
      m_inputChain(NULL),
      m_outputPath("./"),
      m_inputPath("./") {
  m_SumWeights.clear();
}

generateHistogram::~generateHistogram() {}

void generateHistogram::SetInputPath(TString path) { m_inputPath = path; }

TString generateHistogram::GetOutputPath() { return m_outputPath; }

void generateHistogram::SetOutputPath(TString path) {
  m_outputPath = path;

  std::cout << "m_outputPath " << m_outputPath << std::endl;
}

void generateHistogram::SetNEvents(int events) { m_nEvents = events; }

void generateHistogram::SetDebug(bool debug) { m_Debug = debug; }

void generateHistogram::SetNominalOnly(bool nominal) {
  m_NominalOnly = nominal;
}

void generateHistogram::SetLumi(float lumi) {
  m_Lumi = lumi * 1000;  //Convert fb-1 to pb-1
}

void generateHistogram::SetTotalSumWDSID(bool TotalSumWDSID) {
  m_TotalSumWDSID = TotalSumWDSID;
}

void generateHistogram::ApplySherpaWeightCut(bool SherpaWeightCut) {
  m_SherpaWeightCut = SherpaWeightCut;
}

// void generateHistogram::WriteMVATree(bool WriteMVATree) {
//   m_WriteMVATree = WriteMVATree;
// }

void generateHistogram::SetRegion(int iRegion) { m_Region = iRegion; }

unsigned int generateHistogram::channelNumber(AnalysisType type) {
  unsigned int ChannelNumber = 999;
  if (type == ZeroLepton) ChannelNumber = 0;
  if (type == OneLepton) ChannelNumber = 1;
  if (type == TwoLepton) ChannelNumber = 2;
  if (ChannelNumber == 999) {
    std::cout << "Channel number not set up correctly!" << std::endl;
  }
  return ChannelNumber;
}

void generateHistogram::InitializeInfoTool() {
  //Initialise the information tool data member in class
  m_InfoTool = new InfoProvider(
      std::string(gSystem->Getenv("WorkDir_DIR")) +
      "/data/MonoSWWTruthFramework_NtupleToHist/SampleInformation_13TeV.txt");
}

void generateHistogram::InitializeBTagTool(int DSID) {
  //Initialise the b-tagging tool data member in class
  m_BtagTool = new btagTool(m_InfoTool->GetShowerName(DSID));
}

void generateHistogram::InitialiseChain(TString ChainName) {
  //Initialise the TChain with specified TChains
  m_inputChain = m_inputChain = new TChain(ChainName);
}

void generateHistogram::InitialiseHistFill() {
  //Initialise the histogram gille data member
  m_histFill = new histSvc(m_outputPath, m_NominalOnly);
}

void generateHistogram::GenerateHistogram(AnalysisType type, int DSID) {
  //Initialise the Information Tool
  InitializeInfoTool();

  // Configure store
  StoreConfig(type, DSID);

  //Initialise the b-tagging tool
  InitializeBTagTool(DSID);

  //Initialise the BDT tree used for the re-weighting
  // m_tree = new MVATree(type, m_outputPath, DSID);

  // Initialise the Histogram re-weighting
  InitialiseHistFill();

  // Load File (ntuple info)
  FileLoader(type, DSID);

  //Check input chain entries
  std::cout << m_inputChain->GetEntries() << std::endl;

  // Set histogram fill analysis type
  m_histFill->SetAnalysisType(type);

  // Set the analysis region
  m_histFill->SetRegion(m_Region);

  //Set Sherpa Weight cut off
  m_histFill->ApplySherpaWeightCut(m_SherpaWeightCut);

  //Set the process for the histogram plotter
  m_histFill->SetProcess(DSID, m_InfoTool->GetSampleName(DSID));

  // Set Ntuple reader data member with the relevant TChain and the channel
  // number
  readNtuple = new ReadTruthTree(m_inputChain, channelNumber(type));

  // Call and run the histogram fill function
  FillHistograms();

  // Write the histograms to file
  m_histFill->WriteHists();

  //Write MVA to file
  // if (m_WriteMVATree) m_tree->WriteTree();

  // Delete heap allocated objects
  // delete m_tree;
  delete m_histFill;
  delete readNtuple;
  delete m_inputChain;
}

void generateHistogram::StoreConfig(AnalysisType type, int DSID) {
  m_type = type;
  m_DSID = DSID;
  //  m_variation = variation;
  //  std::cout << m_region.size() << std::endl;
  //  std::cout << m_variable.size() << std::endl;
}

TH1F* generateHistogram::getTotalSumWDSID(TString inputPath, int DSID) {
  TH1F* h_SumW = nullptr;

  TString SumWName = "h_Keep_EventCount_";
  SumWName += DSID;
  SumWName += "_";     //Remove it for old samples
  SumWName += m_type;  //Remove it for old samples
  SumWName += "Lep";   //Remove it for old samples
  if (m_SherpaWeightCut)
    SumWName =
        SumWName.ReplaceAll("h_Keep_EventCount_", "h_Keep_SumWeight_Sherpa_");

  //Strip the path of the input file
  TObjArray* tx = inputPath.Tokenize("/");
  TString fileName = ((TObjString*)tx->Last())->String();
  TString filePath = inputPath.ReplaceAll(fileName, "");
  if (m_Debug) std::cout << filePath << std::endl;

  // Loop over the root files in the directory
  TSystemDirectory dir("Directory", filePath);
  TList* files = dir.GetListOfFiles();
  TIter next(files);

  TSystemFile* tFile;
  TString fileInDir;
  while ((tFile = (TSystemFile*)next())) {
    fileInDir = tFile->GetName();
    if (fileInDir.EndsWith("root")) {
      TFile* rootFileInDir = TFile::Open(filePath + fileInDir);
      if (h_SumW == nullptr) {
        h_SumW = (TH1F*)rootFileInDir->Get(SumWName);
        //Next line essential to decouple the histogram from root file
        h_SumW->SetDirectory(0);
      } else {
        h_SumW->Add((TH1F*)rootFileInDir->Get(SumWName));
        if (m_Debug)
          std::cout << "Nominal SumW file " << fileInDir << ": "
                    << h_SumW->GetBinContent(1) << std::endl;
      }
      rootFileInDir->Close();
      delete rootFileInDir;
    }
  }

  if (m_Debug)
    std::cout << "Total Nominal SumW: " << h_SumW->GetBinContent(1)
              << std::endl;
  //Return the total sum weight histogram
  return h_SumW;
}

void generateHistogram::FileLoader(AnalysisType type, int DSID) {
  //TODO Add file loading based on process (i.e. if Whf, load all W+jets samples)
  std::vector<TString> variationNames;

  TString ChainName = "Tree_";
  ChainName += type;
  ChainName += "Lep";

  // //This will need to go in for the next version of the ntuples (should be OK
  // for now)
  // if(m_type == AnalysisType::ZeroLepton) ChainName += "_0Lep";
  // else if(m_type == AnalysisType::OneLepton) ChainName += "_1Lep";
  // else if(m_type == AnalysisType::TwoLepton) ChainName += "_2Lep";

  TString AnalysisName1 = "";
  AnalysisName1 +=type;
  AnalysisName1 += "_Lep";
  TString AnalysisName2 = "";
  AnalysisName2 +=type;
  AnalysisName2 += "Lep";
  TString AnalysisName3 = "";
  AnalysisName3 +=type;
  AnalysisName3 += "_lep";
  TString AnalysisName4 = "";
  AnalysisName4 +=type;
  AnalysisName4 += "lep";

  m_inputChain = new TChain(ChainName);

  TH1F* h_SumW;
  TH1F* h_XSec;

  TString SumWName = "h_Keep_EventCount_";
  SumWName += DSID;
  SumWName += "_";    //Remove it for old samples
  SumWName += type;   //Remove it for old samples
  SumWName += "Lep";  //Remove it for old samples
  if (m_SherpaWeightCut)
    SumWName =
        SumWName.ReplaceAll("h_Keep_EventCount_", "h_Keep_SumWeight_Sherpa_");

  TString XSecName = "h_Keep_CrossSection_";
  XSecName += DSID;
  XSecName += "_";    //Remove it for old samples
  XSecName += type;   //Remove it for old samples
  XSecName += "Lep";  //Remove it for old samples

  TString DSIDStr = "";
  DSIDStr += DSID;
  m_SumWeights.clear();
  m_XSec = 0;

  if (m_inputPath.Contains(".root")) {
    std::cout << "Not so fancy!" << std::endl;
    m_inputChain->Add(m_inputPath);
    TFile* loadFile = TFile::Open(m_inputPath);

    h_SumW = (TH1F*)loadFile->Get(SumWName);

    if (m_TotalSumWDSID) {
      h_SumW = getTotalSumWDSID(m_inputPath, DSID);
    }
    h_XSec = (TH1F*)loadFile->Get(XSecName);
    if (h_XSec == NULL){
      std::cout<<"generateHistogram::FileLoader trying to access non existent histogram, exiting"<< std::endl;
      exit(0);
    }
    for (int i = 1; i <= h_SumW->GetNbinsX(); ++i) {
      if (strlen(h_SumW->GetXaxis()->GetBinLabel(i)) == 0)
        continue;  //Want to skip variations which don't exist
      m_SumWeights.push_back(h_SumW->GetBinContent(i));
      TString varName_temp = h_SumW->GetXaxis()->GetBinLabel(i);
      variationNames.push_back((varName_temp).ReplaceAll(" ", ""));
    }
    m_XSec =
        h_XSec->GetBinContent(1) /
        h_XSec->GetEntries();  //Need to divide by entries due to grid merging
    loadFile->Close();
    delete loadFile;
  } else {
    std::cout << "Input is a directory: going fancy " << std::endl;
    DIR* dir;
    dirent* pdir;
    dir = opendir(m_inputPath);  // open current directory
    while ((pdir = readdir(dir))) {
      // std::cout << pdir->d_name << std::endl;
      TString foldName = pdir->d_name;
      if (!foldName.Contains(DSIDStr) || 
	  ( !foldName.Contains(AnalysisName1) && 
	  !foldName.Contains(AnalysisName2) && 
	  !foldName.Contains(AnalysisName3) && 
	  !foldName.Contains(AnalysisName4) ) ) continue;
      if (m_Debug) std::cout << pdir->d_name << std::endl;
      DIR* dir2;
      dirent* pdir2;
      dir2 = opendir((m_inputPath + "/" + foldName));  // open current directory
      while ((pdir2 = readdir(dir2))) {
        TString fName = pdir2->d_name;
	std::cout << "dir2 name: " << pdir2->d_name << std::endl;
        if (!fName.Contains(".root")) continue;
        if ( !fName.Contains(AnalysisName1) and  
             !fName.Contains(AnalysisName2) and
             !fName.Contains(AnalysisName3) and
             !fName.Contains(AnalysisName4)
              ) {
          std::cout << "generateHistogram::FileLoader WARNING detected input with suspicious analysis name, skipping it"<< std::endl;
          continue; // FN skipping file if ends with wrong lepton region tag
          }
        TString temp = m_inputPath + "/" + foldName + "/" + fName;
        m_inputChain->Add((m_inputPath + "/" + foldName + "/" + fName));
        if (m_Debug) std::cout << fName << std::endl;
        TFile* loadFile =
            TFile::Open(m_inputPath + "/" + foldName + "/" + fName);
        h_SumW = (TH1F*)loadFile->Get(SumWName);
        if (m_XSec == 0) {
          h_XSec = (TH1F*)loadFile->Get(XSecName);
          if (h_XSec == NULL){
            std::cout<<"generateHistogram::FileLoader trying to access non existent histogram, exiting"<< std::endl;
            exit(0);
          }
          m_XSec =
              h_XSec->GetBinContent(1) /
              h_XSec
                  ->GetEntries();  //Need to divide by entries due to grid merging
        }
	//std::cout << "size of m_SumWeights" << m_SumWeights.size() << std::endl;
        if (m_SumWeights.size() == 0) {
          for (int i = 1; i <= h_SumW->GetNbinsX(); ++i) {
            if (strlen(h_SumW->GetXaxis()->GetBinLabel(i)) == 0)
              continue;  //Want to skip variations which don't exist
            m_SumWeights.push_back(h_SumW->GetBinContent(i));
            TString varName_temp = h_SumW->GetXaxis()->GetBinLabel(i);
            variationNames.push_back((varName_temp).ReplaceAll(" ", ""));
            //	    std::cout << variationNames.back() << std::endl;
          }
        } else {
	  int iVar=0;
          for (int i = 1; i <= h_SumW->GetNbinsX(); ++i) {
            if (strlen(h_SumW->GetXaxis()->GetBinLabel(i)) == 0)
              continue;  //Want to skip variations which don't exist
	    else iVar++;
            m_SumWeights.at(iVar - 1) += h_SumW->GetBinContent(i);
          }
        }
        loadFile->Close();
        delete loadFile;
      }
      if (m_inputChain->GetEntries() == 0){
      std::cout << "generateHistogram::FileLoader ERROR m_inputChain has no events" << std::endl;
      exit(0); 
      }
    }
  }


  if (DSID == 410441) m_XSec = 831.76 * 1.0 * 0.43842;
  if (DSID == 410442) m_XSec = 831.76 * 1.0 * 0.105;
  if (DSID == 410470) m_XSec = 831.76 * 1.0 * 0.543;
  if (DSID == 410472) m_XSec = 831.76 * 1.0 * 0.105;
  if (DSID == 410480) m_XSec = 831.76 * 1.0 * 0.43842;
  if (DSID == 410482) m_XSec = 831.76 * 1.0 * 0.105;
  if (DSID == 410557) m_XSec = 831.76 * 1.0 * 0.43842;
  if (DSID == 410558) m_XSec = 831.76 * 1.0 * 0.105;

  std::cout << "m_SumWeights " << m_SumWeights.at(0) << std::endl;
  std::cout << "m_XSec " << m_XSec << std::endl;

  for (unsigned int r = 0; r < m_SumWeights.size(); r++) {
    std::cout << variationNames.at(r) << " " << m_SumWeights.at(r) << std::endl;
  }

  m_histFill->SetLumi(m_Lumi);
  m_histFill->SetXSec(m_XSec);
  m_histFill->SetSumWeights(m_SumWeights);
  m_histFill->SetWeightNames(variationNames);
}

unsigned long int generateHistogram::bitmask(
    const unsigned long int cut,
    const std::vector<unsigned long int>& excludeCuts) {
  if (m_Debug)
    std::cout << "<generateHistogram::bitmask>::   Perform bitmasking"
              << std::endl;

  unsigned long int mask = 0;
  unsigned long int bit = 0;

  const unsigned long int offBit = 0;
  const unsigned long int onBit = 1;

  if (m_Debug)
    std::cout << "<generateHistogram::bitmask>::   Running exclude check"
              << std::endl;
  if (m_Debug)
    std::cout << "<generateHistogram::bitmask>::       ExcludeCuts.size() : "
              << excludeCuts.size() << std::endl;
  if (m_Debug)
    std::cout << "<generateHistogram::bitmask>::       cut : " << cut
              << std::endl;
  for (unsigned long int i = 0; i < cut + 1; ++i) {
    if (excludeCuts.size() > 0 &&
        std::find(excludeCuts.begin(), excludeCuts.end(), i) !=
            excludeCuts.end()) {
      // if a cut should be excluded set the corresponding bit to 0
      mask = mask | offBit << bit++;
    } else {
      // otherwise set the bit to 1
      mask = mask | onBit << bit++;
    }
  }

  if (m_Debug)
    std::cout << "<generateHistogram::bitmask>::   Return exclude" << std::endl;
  return mask;
}

// function to compare a bit flag against a bit mask
// Usage: given a flag, check if it passes all cuts up to and including "cut"
// excluding the cuts in "excludedCuts"
bool generateHistogram::passAllCutsUpTo(
    const unsigned long int flag, const unsigned long int cut,
    const std::vector<unsigned long int>& excludeCuts) {
  // Get the bitmask: we want to check all cuts up to "cut" excluding the cuts listed in excludeCuts
  unsigned long int mask = bitmask(cut, excludeCuts);
  // Check if the flag matches the bit mask
  return (flag & mask) == mask;
}

// a function to check specifig cuts
bool generateHistogram::passSpecificCuts(
    const unsigned long int flag, const std::vector<unsigned long int>& cuts) {
  unsigned long int mask = 0;
  const unsigned long int onBit = 1;

  // Make the bit mask
  for (auto cut : cuts) mask = mask | (onBit << cut);
  // Check if the flag matches the bit mask
  return (flag & mask) == mask;
}

void generateHistogram::updateFlag(unsigned long int& flag,
                                   const unsigned long int cutPosition,
                                   const unsigned long int passCut) {
  // Put bit passCut (true or false) at position cutPosition
  flag = flag | passCut << cutPosition;
}

void generateHistogram::FillHistograms() {
  //Dummy function to be overwritten
}

//Assign event flavour function
float EventObjects::AssignEventFlavour() {
  //std::cout << "<AssignEventFlavour>::   signalJetFlavour.size() = " << signalJetFlavour.size() << std::endl;

  //Assign event flavour
  if (signalJetFlavour.size() > 0) {
    //std::cout << "<AssignEventFlavour>::   signalJetFlavour 1 = " << signalJetFlavour.at(taggedJets.front()) << std::endl;
    //std::cout << "<AssignEventFlavour>::   signalJetFlavour 2 = " << signalJetFlavour.at(taggedJets.back()) << std::endl;

    if (signalJetFlavour.at(taggedJets.front()) +
            signalJetFlavour.at(taggedJets.back()) ==
        10) {
      return 0.0;
    } else if (signalJetFlavour.at(taggedJets.front()) +
                   signalJetFlavour.at(taggedJets.back()) ==
               9) {
      return 1.0;
    } else if (signalJetFlavour.at(taggedJets.front()) +
                   signalJetFlavour.at(taggedJets.back()) ==
               5) {
      return 2.0;
    } else if (signalJetFlavour.at(taggedJets.front()) +
                   signalJetFlavour.at(taggedJets.back()) ==
               8) {
      return 3.0;
    } else if (signalJetFlavour.at(taggedJets.front()) +
                   signalJetFlavour.at(taggedJets.back()) ==
               4) {
      return 4.0;
    } else if (signalJetFlavour.at(taggedJets.front()) +
                   signalJetFlavour.at(taggedJets.back()) ==
               0) {
      return 5.0;
    }
  }

  //Otherwise return a default value
  return -99.0;
}
