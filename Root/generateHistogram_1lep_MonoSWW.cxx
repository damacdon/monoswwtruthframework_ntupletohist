#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_1lep_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/btagTool_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/histSvc_MonoSWW.h"

#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <fstream>
#include <iostream>
#include <vector>

generateHistogram_1lep::generateHistogram_1lep() {}

generateHistogram_1lep::~generateHistogram_1lep() {}

void generateHistogram_1lep::FillHistograms() {
  Selection1Lepton();  //Needed to steer the code to run the 1-lepton selection
}

//Construct the necessary TLorentzVectors for the 1-leptn event
bool generateHistogram_1lep::BuildEvent(ReadTruthTree &readNtuple,
                                        EventObjects &PhysicsObjects,
                                        btagTool &BtagTool, bool Debug, int Region) {

  //FN: building the alljets for the b-tag veto
  for (unsigned int i = 0 ; i < readNtuple.Jet_Phi->size(); i++ ){
    if (Debug)
      std::cout << "<generateHistogram_1lep::BuildEvent()>::    "<< i+1 << " Jets";
    TLorentzVector tmp;
    tmp.SetPtEtaPhiM(
		     readNtuple.Jet_Pt->at(i), readNtuple.Jet_Eta->at(i),
		     readNtuple.Jet_Phi->at(i), readNtuple.Jet_Mass->at(i));
    PhysicsObjects.allJets.push_back(tmp);
  }

  //Check for either 2 Antikt4 jets or 1 large-R jet
  if (readNtuple.nSignalJet < 2) {
    return false;
  }

  //Build lepton side of the event
  if (static_cast<int>(readNtuple.Lepton_ID->size()) < 1) {
    std::cout << "<BuildEvent()>::   Leptons in event < 1, creation of lepton "
                 "TLorentzVectors:: impossible. Invalid event."
              << std::endl;
    return false;
  }

  if (static_cast<int>(readNtuple.Lepton_ID->size()) != 1) {
    std::cout << "<BuildEvent()>::   Number of leptons in event is not 1. "
                 "Something went wrong, exiting."
              << std::endl;
    exit(0);
  }
  //else std::cout << "<BuildEvent()>::   Number of leptons in event is correctly 1." << std::endl;  
      
  PhysicsObjects.LeptonVec1.SetPtEtaPhiM(
      readNtuple.Lepton_Pt->at(0), readNtuple.Lepton_Eta->at(0),
      readNtuple.Lepton_Phi->at(0), readNtuple.Lepton_Mass->at(0));
  PhysicsObjects.LeptonTransVec1.SetPtEtaPhiM(
      readNtuple.Lepton_Pt->at(0),
      0.0,  //eta for missing energy not defined by construction
      readNtuple.Lepton_Phi->at(0), readNtuple.Lepton_Mass->at(0));

  //------------------------ Build up to 4 leading resolved jets ------------------------// 

  //Build two leading jets
  if (readNtuple.nSignalJet < 2) {
    //std::cout << "<BuildEvent()>::   Number of signal jets in event < 2, creation of jet TLorentzVector:: impossible. Invalid event." << std::endl;
    return false;
  }
  PhysicsObjects.jet1vec.SetPtEtaPhiM(
      readNtuple.Jet_Pt->at(0), readNtuple.Jet_Eta->at(0),
      readNtuple.Jet_Phi->at(0), readNtuple.Jet_Mass->at(0));
  PhysicsObjects.jet2vec.SetPtEtaPhiM(
      readNtuple.Jet_Pt->at(1), readNtuple.Jet_Eta->at(1),
      readNtuple.Jet_Phi->at(1), readNtuple.Jet_Mass->at(1));

  // Build the 3rd resolved jet
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved Jets 3"
              << std::endl;
  if (readNtuple.nSignalJet >= 3) {
    PhysicsObjects.jet3vec.SetPtEtaPhiM(
					readNtuple.Jet_Pt->at(2), readNtuple.Jet_Eta->at(2),
					readNtuple.Jet_Phi->at(2), readNtuple.Jet_Mass->at(2));
  }
  // Build the 4th resolved jet
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved Jets 4"
              << std::endl;
  if (readNtuple.nSignalJet == 4) {
    PhysicsObjects.jet4vec.SetPtEtaPhiM(
					readNtuple.Jet_Pt->at(3), readNtuple.Jet_Eta->at(3),
					readNtuple.Jet_Phi->at(3), readNtuple.Jet_Mass->at(3));
  }

  //-------------------------------------------------------------------------------------// 

  //------------------------ Build up to 3 leading large-R jets ------------------------//                                                                                           
  // Build the two leading large-R jets
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Large-R 1+2 Jets"
              << std::endl;
  }
  if (readNtuple.nLargeRJet >= 1) {
    PhysicsObjects.largeRjet1vec.SetPtEtaPhiM(
					      readNtuple.LargeRJet_Pt->at(0), readNtuple.LargeRJet_Eta->at(0),
					      readNtuple.LargeRJet_Phi->at(0), readNtuple.LargeRJet_Mass->at(0));
  }
  // Now the sub-leading large-R jet                                                                                                                                                
  if (readNtuple.nLargeRJet >= 2) {
    PhysicsObjects.largeRjet2vec.SetPtEtaPhiM(
					      readNtuple.LargeRJet_Pt->at(1), readNtuple.LargeRJet_Eta->at(1),
					      readNtuple.LargeRJet_Phi->at(1), readNtuple.LargeRJet_Mass->at(1));
  }

  // Build the 3rd large-R jet
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Large-R Jets 3"
              << std::endl;
  }
  if (readNtuple.nLargeRJet == 3) {
    PhysicsObjects.largeRjet3vec.SetPtEtaPhiM(
					      readNtuple.LargeRJet_Pt->at(2), readNtuple.LargeRJet_Eta->at(2),
					      readNtuple.LargeRJet_Phi->at(2), readNtuple.LargeRJet_Mass->at(2));
  }
  else {
    PhysicsObjects.largeRjet3vec.SetPtEtaPhiM(0., 0., 0., 0.);
  }
  //-------------------------------------------------------------------------------------//  

  //----------------------- Build up to 3 leading reclustered jets ----------------------//
                                                                                                                                                                                    
  // Build the two leading Reclustered jets
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Reclustered 1+2 Jets"
              << std::endl;
  }
  if (readNtuple.nReclusteredJet >= 1) {
    PhysicsObjects.Reclusteredjet1vec.SetPtEtaPhiM(
					   readNtuple.ReclusteredJet_Pt->at(0), readNtuple.ReclusteredJet_Eta->at(0),
					   readNtuple.ReclusteredJet_Phi->at(0), readNtuple.ReclusteredJet_Mass->at(0));
  }
  // Now the sub-leading Reclustered jet
  if (readNtuple.nReclusteredJet >= 2) {
    PhysicsObjects.Reclusteredjet2vec.SetPtEtaPhiM(
					   readNtuple.ReclusteredJet_Pt->at(1), readNtuple.ReclusteredJet_Eta->at(1),
					   readNtuple.ReclusteredJet_Phi->at(1), readNtuple.ReclusteredJet_Mass->at(1));
  }

  // Build the 3rd Reclustered jet
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Reclustered Jets 3"
              << std::endl;
  }
  if (readNtuple.nReclusteredJet == 3) {
    PhysicsObjects.Reclusteredjet3vec.SetPtEtaPhiM(
					   readNtuple.ReclusteredJet_Pt->at(2), readNtuple.ReclusteredJet_Eta->at(2),
					   readNtuple.ReclusteredJet_Phi->at(2), readNtuple.ReclusteredJet_Mass->at(2));
  }
  else {
    PhysicsObjects.Reclusteredjet3vec.SetPtEtaPhiM(0., 0., 0., 0.);
  }
  //-------------------------------------------------------------------------------------// 

  //Missing Transverse Energy construction
  PhysicsObjects.MET_Vec.SetPtEtaPhiM(readNtuple.MET, 0.0, readNtuple.MET_Phi,
                                      0.0);

  //Define the number of jets
  PhysicsObjects.nJets = readNtuple.nSignalJet + readNtuple.nForwardJet;
  PhysicsObjects.numJets = PhysicsObjects.nJets;
  PhysicsObjects.nLargeRJets = readNtuple.nLargeRJet;
  PhysicsObjects.nReclusteredJets = readNtuple.nReclusteredJet;

  //Assign to vector for ease of manipulation with b-tagging tool
  PhysicsObjects.signalJets = {PhysicsObjects.jet1vec, PhysicsObjects.jet2vec,
                               PhysicsObjects.jet3vec};

  //Jet flavour and b-tagging identification
  int jet1flav = readNtuple.Jet_HadronConeExclTruthLabelID->at(0);
  int jet2flav = readNtuple.Jet_HadronConeExclTruthLabelID->at(1);
  int jet3flav = (readNtuple.nSignalJet == 3)
                     ? readNtuple.Jet_HadronConeExclTruthLabelID->at(2)
                     : -999;
  PhysicsObjects.btagWeight = 1;
  PhysicsObjects.signalJetFlavour = {jet1flav, jet2flav, jet3flav};

  // FN: considering all the  jet in the event for the b-tag, not only the 3 leading 
  std::vector<int> allJetflav;
  for (unsigned int i ; i < readNtuple.Jet_HadronConeExclTruthLabelID->size(); i++ ){
    allJetflav.push_back( readNtuple.Jet_HadronConeExclTruthLabelID->at(i) );
  }
  PhysicsObjects.allJetFlavour = allJetflav;

  if (Debug)
    std::cout << "<generateHistogram_1lep::BuildEvent()>::   b-tagged Jets"
              << std::endl;
  // FN we do not need anymore to know what are the b-tagged jets: just get the btagweight                                                                               
  // btag weight will be small for actual b's                                                                                                                            
  // and hight for LQ.                                                                                                                                                   
  // this takes into account the presence of b's which are not tagged                                                                                                    
  // and the absence of (little) LQ mistagged for b's                                                                                                                    

  // PhysicsObjects.taggedJets = BtagTool.compute_btagging(
  //     PhysicsObjects.signalJets, PhysicsObjects.signalJetFlavour,
  //     PhysicsObjects
  //         .btagWeight);  //Run the b-tagging. This retuns a vector of the vector entries of the 2 b-tagged jets 
  // // std::cout << "generateHistogram_0lep: " << PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(0)) << " " << PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(1)) << std::endl;

  // std::cout << "btagWeight before " << PhysicsObjects.btagWeight << std::endl;                                                                                                    
  BtagTool.compute_btagweight(PhysicsObjects.allJets, PhysicsObjects.allJetFlavour,PhysicsObjects.btagWeight);
  // std::cout << "btagWeight after " << PhysicsObjects.btagWeight << std::endl;   

  //Run the b-tagging. This retuns a vector of the vector entries of the 2 b-tagged jets
  PhysicsObjects.taggedJets = BtagTool.compute_btagging(
      PhysicsObjects.signalJets, PhysicsObjects.signalJetFlavour,
      PhysicsObjects.btagWeight);

  // DMM: For now, the selected signal jets are just the 4 jets with the highest pT
  PhysicsObjects.jet1vecSel = PhysicsObjects.jet1vec;
  PhysicsObjects.jet2vecSel = PhysicsObjects.jet2vec;
  PhysicsObjects.jet3vecSel = PhysicsObjects.jet3vec;
  PhysicsObjects.jet4vecSel = PhysicsObjects.jet4vec;

  //Set the event weight. So far it is constructed using:   Event_Weight = readNtuple->Event_Weight * PhysicsObjects.btagWeight;
  PhysicsObjects.Event_Weight =
      readNtuple.Event_Weight * PhysicsObjects.btagWeight;

  // FN: Dark Higgs                                                                                                                                                                
  // constructed for the three different categories                                                                                                                                
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Dark Higgs Candidate"
              << std::endl;
  PhysicsObjects.DarkHiggs_merged = PhysicsObjects.Reclusteredjet1vec;

  PhysicsObjects.DarkHiggs_intermediate = PhysicsObjects.Reclusteredjet1vec; // FN for the moment                                                                                    
  PhysicsObjects.DarkHiggs_resolved = PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel +  PhysicsObjects.jet3vecSel  +  PhysicsObjects.jet4vecSel;

  // FN each region has a different mass reconstruction
  PhysicsObjects.mS_candidate = 0;
  if (Region == 2)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_resolved.M();
  else if (Region == 1)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_intermediate.M();
  else if (Region == 0)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_merged.M();

  //Transverse vector of the W-boson
  PhysicsObjects.WTransVec =
      PhysicsObjects.MET_Vec + PhysicsObjects.LeptonTransVec1;

  // Missing Transverse Energy assuming the muon decays invisibly
  PhysicsObjects.MET_muInv_Vec = PhysicsObjects.MET_Vec + PhysicsObjects.LeptonVec1;
  PhysicsObjects.MET_muInv = PhysicsObjects.MET_muInv_Vec.Pt();
  
  //Return ok for BuildEvent()
  return true;

}  //BuildEvent()

// //Function for building input variables to BDT. Move to generateHistogram::
// void generateHistogram_1lep::BuildBDTVars(ReadTruthTree &readNtuple,
//                                           EventObjects &PhysicsObjects,
//                                           bool Debug) {
//   //Build BDT input Variables
//   //W candidate mass 
//   //PhysicsObjects.mW = PhysicsObjects.Higgs.M();

//   //Tranverse pT of Vector boson
//   PhysicsObjects.pTV = PhysicsObjects.WTransVec.Pt();

//   //Missing Transverse Energy
//   PhysicsObjects.MET = PhysicsObjects.MET_Vec.Pt();

//   // Missing Transverse Energy assuming the muon decays invisibly
//   PhysicsObjects.MET_muInv_Vec = PhysicsObjects.MET_Vec + PhysicsObjects.LeptonVec1;
//   PhysicsObjects.MET_muInv = PhysicsObjects.MET_muInv_Vec.Pt();

//   //Lepton pT
//   PhysicsObjects.pTL = PhysicsObjects.LeptonVec1.Pt();

//   //mass of top candidate
//   PhysicsObjects.mTop =
//       CalculateMtop(PhysicsObjects.LeptonVec1, PhysicsObjects.MET_Vec,
//                     PhysicsObjects.jet1vecSel, PhysicsObjects.jet2vecSel);

//   //minimum dPhi between lepton and leading signal jet
//   PhysicsObjects.dPhiLJ1min = std::min(
//       fabs(PhysicsObjects.LeptonVec1.DeltaPhi(PhysicsObjects.jet1vecSel)),
//       fabs(PhysicsObjects.LeptonVec1.DeltaPhi(PhysicsObjects.jet2vecSel)));

//   //dPhi between lepton and b-jet
//   //PhysicsObjects.dPhiVLJ1 =
//   //    fabs(PhysicsObjects.WTransVec.DeltaPhi(PhysicsObjects.));

//   //dR between leading signal jets
//   PhysicsObjects.dRJJ =
//       PhysicsObjects.jet1vecSel.DeltaR(PhysicsObjects.jet2vecSel);

//   // Transverse mass of W-boson
//   //PhysicsObjects.mTW = PhysicsObjects.WTransVec.M();

//   //Leading signal jet pT
//   PhysicsObjects.pTJ1 = PhysicsObjects.jet1vecSel.Pt();

//   // Sub-Leading signal jet pT
//   PhysicsObjects.pTJ2 = PhysicsObjects.jet2vecSel.Pt();

//   //3-jet invariant mass
//   PhysicsObjects.mJJJ = (PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel + PhysicsObjects.jet3vecSel).M();

//   //dY (rapidity) between Higgs and V-boson
//   //PhysicsObjects.dYWH =
//   //    CalculatedYWH(PhysicsObjects.LeptonVec1, PhysicsObjects.MET_Vec,
//   //                  PhysicsObjects.jet1vecSel, PhysicsObjects.jet2vecSel);

//   // 3rd Jet pT
//   PhysicsObjects.pTJ3 = PhysicsObjects.jet3vecSel.Pt();

//   // Event Number
//   PhysicsObjects.Event_Number = readNtuple.Event_Number;

//   // Define event truth flavour
//   PhysicsObjects.EvtFlav = PhysicsObjects.AssignEventFlavour();
// }

//Bit Flag Event Selection
void generateHistogram_1lep::EventSelection(ReadTruthTree &readNtuple,
                                            EventObjects &PhysicsObjects,
                                            unsigned long &eventFlag,
                                            bool Debug, int Region) {
  //Cut 1:   Number of input events
  if (Debug) {
    std::cout << "<EventSelection>::   Check number of input events"
              << std::endl;
  };

  updateFlag(eventFlag, OneLeptonCutFlow::Input_events, 1);

  // Cut 2:  Check that the lepton is a muon
  if (Debug) {
    std::cout << "<EventSelection>::  Check that the lepton is a muon"
              << std::endl;
  };
  bool passed_cut = false;
  if (std::abs(readNtuple.Lepton_ID->at(0)) == 13)
    passed_cut = true;

  updateFlag(eventFlag, OneLeptonCutFlow::Lepton_is_muon, passed_cut);

  //Cut 3:   Check that muons pass the pT requirement
  if (Debug) {
    std::cout << "<EventSelection>::  Check if muons pass pT requirement"
              << std::endl;
  };
  passed_cut = false;
  if (readNtuple.Lepton_Pt->at(0) > 25)
    passed_cut = true;
  updateFlag(eventFlag, OneLeptonCutFlow::Lepton_pt, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 4: Lower bound on METmuInvis
  passed_cut = false;
  //std::cout << PhysicsObjects.MET_muInv << std::endl;
  if (PhysicsObjects.MET_muInv > 200.)
    passed_cut = true;
  updateFlag(eventFlag, OneLeptonCutFlow::MET_muInv, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 5: Tau veto
  if (Debug) {
    std::cout << "<EventSelection>::   Tau veto" << std::endl;
  };
  passed_cut = false;
  if (readNtuple.NSignalTaus == 0) passed_cut = true;
  updateFlag(eventFlag, OneLeptonCutFlow::TauVeto, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 6:   MinDPhiMETJetsResolved
  if (Debug) {
    std::cout << "<EventSelection>::   MinDPhiMETJetsResolved" << std::endl;
  }
  passed_cut = false;
  double mindPhi1 = 1000, mindPhi2 = 1000,
    mindPhi3 = 1000;  //What is this!!!!! I have lost all hope in humanity
 
  if (readNtuple.nSignalJet >= 1)
    mindPhi1 = fabs(readNtuple.Jet_Phi->at(0) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet >= 2)
    mindPhi2 = fabs(readNtuple.Jet_Phi->at(1) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet == 3)
    mindPhi3 = fabs(readNtuple.Jet_Phi->at(2) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet == 2 && readNtuple.nForwardJet >= 1)
    mindPhi3 = fabs(readNtuple.ForwardJet_Phi->at(0) - readNtuple.MET_Phi);
  double mindPhi = mindPhi1;
  if (mindPhi2 < mindPhi) mindPhi = mindPhi2;
  if (mindPhi3 < mindPhi) mindPhi = mindPhi3;
  if (mindPhi >= 0.34906585039)
    passed_cut = true;
  PhysicsObjects.mindPhi = mindPhi;
  updateFlag(eventFlag, OneLeptonCutFlow::MinDPhiMETJets, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 7: MET_Sig
  if (Debug) {
    std::cout << "<EventSelection>::   MET Significance" << std::endl;
  };
  passed_cut = false;
  if (readNtuple.MET_Sig_muInvis > 15.) passed_cut = true;
  updateFlag(eventFlag, OneLeptonCutFlow::MET_Sig, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 8: m_S window
  if (Debug) {
    std::cout << "<EventSelection>::   m_S Window" << std::endl;
  };
  passed_cut = false;
  if (PhysicsObjects.mS_candidate > 100. && PhysicsObjects.mS_candidate < 400.)
    passed_cut = true;
  updateFlag(eventFlag, OneLeptonCutFlow::mS_window, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Region Selection
  bool passed_merged = false;
  bool passed_intermediate = false;
  bool passed_resolved = false;

  // Calculate the number of Reclustered jets passing the (pseudo) TARJet08ScalarCandidateReader and TAR+comb scalar candidate requirements
  int nTARJet08ScalarCandidateReader = 0;
  int nTARCombScalarBoson = 0;
  for(int iReclJet=0; iReclJet < readNtuple.ReclusteredJet_Pt->size(); iReclJet++){
    double ReclJet_Pt = readNtuple.ReclusteredJet_Pt->at(iReclJet);
    double ReclJet_Mass = readNtuple.ReclusteredJet_Mass->at(iReclJet);

    // Check if it passes the TAR+comb scalar candidate requirement                                                                                                                                                                   
    if( (ReclJet_Mass > 100.) && (ReclJet_Mass < 400.) ) {
      nTARCombScalarBoson++;

      // Additional cut on pt for TARJet08ScalarCandidateReader requirement                                                                                                                                                           
      if(ReclJet_Pt > 300.) nTARJet08ScalarCandidateReader++;
    }
  }

  // Calculate the invariant mass of the leading 4 signal jets (i.e. the resolved dark higgs mass)
  float m_4j = -1.;
  if(readNtuple.nSignalJet > 3){
    m_4j = PhysicsObjects.DarkHiggs_resolved.M();
  }

  // Apply the recycling strategy to classify the event into the merged, intermediate, or resolved region
  if ( (nTARJet08ScalarCandidateReader > 0) && (PhysicsObjects.MET_muInv > 300.) ) {
    passed_merged = true;
  }

  if ( (passed_merged == false) && (nTARCombScalarBoson == 1) ) {
    passed_intermediate = true;
  }

  if ( (passed_merged == false) && (passed_intermediate == false) && (m_4j > 100.) && (m_4j < 400.) ) {
    passed_resolved = true;
  }

  if (Region == 0) {
    if (Debug) {
      std::cout << "<EventSelection>::   Merged Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_merged << std::endl;
    }
    updateFlag(eventFlag, OneLeptonCutFlow::AnalysisRegion, passed_merged);
  }
  else if (Region == 1) {
    if (Debug) {
      std::cout << "<EventSelection>::   Intermediate Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_intermediate << std::endl;
    }
    updateFlag(eventFlag, OneLeptonCutFlow::AnalysisRegion, passed_intermediate);
  }
  else if (Region == 2) {
    if (Debug) {
      std::cout << "<EventSelection>::   Resolved Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_resolved << std::endl;
    }
    updateFlag(eventFlag, OneLeptonCutFlow::AnalysisRegion, passed_resolved);
  }

}  //EventSelection()

void generateHistogram_1lep::Selection1Lepton() {
  int Region = m_Region;    // Get the analysis region

  // Reading in the number of events, and if set to run on a subset
  int nEvents = (m_nEvents == -1) ? m_inputChain->GetEntries() : m_nEvents;

  //Loop through events
  int inspectionInterval = (nEvents / 10 > 0) ? nEvents / 10 : 1;
  
  for (int i = 0; i < nEvents; ++i) {
    //Reset all m_histFill descriptions and physics objects
    m_PhysicsObjects = {};
    m_histFill->Reset();
    // Load the event
    readNtuple->GetEntry(i);
    //Tell user % of completion
    if (i % inspectionInterval == 0)
      std::cout << " Running on event " << i << " of " << nEvents << std::endl;

    //Build the event objects
    if (!BuildEvent((*readNtuple), m_PhysicsObjects, (*m_BtagTool), false, Region)) continue;

    // Run 1-lepton event selection
    unsigned long eventFlag = 0;
    EventSelection((*readNtuple), m_PhysicsObjects, eventFlag, false, Region);

    //Set/Assign region info to histogram filling service
    // DMM: Commenting out number of b-tags in region info
    // m_histFill->SetNtags(2);  //Currently, b-tagging only allows for 2 b-tags

    // FN does not matter as the SetEventFlav function is hacked, see comment below.  
    m_histFill->SetEventFlav(
       0,0);
    // m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(0)),                                                                                                  
    // m_PhysicsObjects.signalJetFlavour.at(                                                                                                                                     
    //     m_PhysicsObjects.taggedJets.at(1)));                                                                                                                                  

    // FN: reverting back and keeping it here in case we change plan (separate fit for V+jets flavour composition)
    // // FN: in the VHbb, there are two b-tags which define the event flavour
    // // in our case we use the number of LQ, c, b to define the EF.
    // // b's have priority on c's which have priority on LQ's
    // // e.g. if we have b,b,c,LQ -> EF is bb etc.  


    //Fill cut flow
    FillCutFlow1lep(eventFlag, readNtuple->Event_Weight,
                    m_PhysicsObjects.btagWeight);

    //Final veto on event selection
    if (!passAllCutsUpTo(eventFlag, OneLeptonCutFlow::AnalysisRegion))
      continue;

    float MET = m_PhysicsObjects.MET_Vec.Pt(); 
    float MET_muInv = m_PhysicsObjects.MET_muInv_Vec.Pt();
    float MET_Sig = readNtuple->MET_Sig;                                                                                                                                            
    float pTV = MET_muInv;      // DMM: Setting pTV to MET_muInv
    float mS_candidate = m_PhysicsObjects.mS_candidate;
    
    /*
    // Loading BDT input variables and possible plotting variables
    // TLorentzVector DiJet = jet1vecSel + jet2vecSel 
    
    //const float mBB = m_PhysicsObjects.Higgs.M();
    const float mJJ = (m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel).M(); 
    const float Mtop =
        CalculateMtop(m_PhysicsObjects.LeptonVec1, m_PhysicsObjects.MET_Vec,
                      m_PhysicsObjects.jet1vecSel, m_PhysicsObjects.jet2vecSel);

    //2+3 jet variables
    float dRJJ =
        m_PhysicsObjects.jet1vecSel.DeltaR(m_PhysicsObjects.jet2vecSel);
    //float dPhiVBB =
    //    fabs(m_PhysicsObjects.WTransVec.DeltaPhi(m_PhysicsObjects.Higgs));
    float dPhiLJmin = std::min(
        fabs(m_PhysicsObjects.LeptonVec1.DeltaPhi(m_PhysicsObjects.jet1vecSel)),
        fabs(
            m_PhysicsObjects.LeptonVec1.DeltaPhi(m_PhysicsObjects.jet2vecSel)));
    float pTJ1 = m_PhysicsObjects.jet1vecSel.Pt();
    float pTJ2 = m_PhysicsObjects.jet2vecSel.Pt();
    float pTJJ = (m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel).Pt();
    //float mTW = m_PhysicsObjects.WTransVec.M();
    //float dYWH =
    //    CalculatedYWH(m_PhysicsObjects.LeptonVec1, m_PhysicsObjects.MET_Vec,
    //                  m_PhysicsObjects.jet1vecSel, m_PhysicsObjects.jet2vecSel);

    // 3-jet only variables
    bool twoJet = m_PhysicsObjects.nJets == 2;
    float mJJJ =
        (twoJet) ? 0
                 : (m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel + m_PhysicsObjects.jet3vecSel).M();
    float pTJ3 = (twoJet) ? 0 : m_PhysicsObjects.jet3vecSel.Pt();
    */
    // m_tree->EventWeight =
    //     readNtuple->Event_Weights->at(0) * m_PhysicsObjects.btagWeight *
    //     m_histFill->CorrectionFactor(
    //         0);  //Want to get the fully corrected event weight, have to use CorrectionFactor from histSvc for the time being...
    // m_tree->nJ = m_PhysicsObjects.nJets;
    // m_tree->EventNumberMod2 = readNtuple->Event_Number % 2;
    // m_tree->FlavB1 =
    //     m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(0));
    // m_tree->FlavB2 =
    //     m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(1));

    // m_tree->mJJ = mJJ * 1e3;  // Needed in MeV for the xml files
    // m_tree->dRJJ = dRJJ;
    // //m_tree->dPhiVBB = dPhiVBB;
    // m_tree->dPhiLJmin = dPhiLJmin;
    // m_tree->pTV = pTV * 1e3;
    // m_tree->pTJ1 = pTJ1 * 1e3;
    // m_tree->pTJ2 = pTJ2 * 1e3;
    // //m_tree->mTW = mTW * 1e3;
    // //m_tree->Mtop = Mtop * 1e3;
    // //m_tree->dYWH = dYWH;
    // m_tree->MET = MET * 1e3;
    // m_tree->MET_muInv = MET_muInv * 1e3;

    // if (!twoJet) {
    //   m_tree->mJJJ = mJJJ * 1e3;
    //   m_tree->pTJ3 = pTJ3 * 1e3;
    // }

    //Set the region description
    m_histFill->SetDescription("CR1L");

    m_histFill->SetNjets(readNtuple->nSignalJet + readNtuple->nForwardJet);

    m_histFill->SetpTV(pTV);
    // float BDT = 0;

    //Compute the BDT value
    /*
    if (readNtuple->Event_Number % 2 == 0) {
      if (twoJet)
        BDT = m_tree->reader_2jet_even->EvaluateMVA("BDT_1L_2jet_even");
      else
        BDT = m_tree->reader_3jet_even->EvaluateMVA("BDT_1L_3jet_even");
    } else {
      if (twoJet)
        BDT = m_tree->reader_2jet_odd->EvaluateMVA("BDT_1L_2jet_odd");
      else
        BDT = m_tree->reader_3jet_odd->EvaluateMVA("BDT_1L_3jet_odd");
    }
    */

    TLorentzVector jet1vecSel_MeV, jet2vecSel_MeV;
    jet1vecSel_MeV.SetPtEtaPhiM(m_PhysicsObjects.jet1vecSel.Pt() * 1e3,
                                m_PhysicsObjects.jet1vecSel.Eta(),
                                m_PhysicsObjects.jet1vecSel.Phi(),
                                m_PhysicsObjects.jet1vecSel.M() * 1e3);
    jet2vecSel_MeV.SetPtEtaPhiM(m_PhysicsObjects.jet2vecSel.Pt() * 1e3,
                                m_PhysicsObjects.jet2vecSel.Eta(),
                                m_PhysicsObjects.jet2vecSel.Phi(),
                                m_PhysicsObjects.jet2vecSel.M() * 1e3);

    //Fill the histograms
    if (m_Debug) std::cout << "Filling histograms" << std::endl;
    m_histFill->SetCutBase(false);  //Fill MVA selection
    // m_histFill->BookFillHist(
    //     "BDT",                      //Name of output variable
    //     1000,                       //Nbins
    //     -1,                         //xmin
    //     1,                          //xmaxx
    //     BDT,                        //variable to plot
    //     readNtuple->Event_Weights,  //Fill with vector of alternative weights
    //     m_PhysicsObjects
    //         .btagWeight);  //Any correction to the overall event weight

    //m_histFill->BookFillHist("MET_Sig_orig", 100, 0, 50, MET_Sig_orig, readNtuple->Event_Weights,
    //                         m_PhysicsObjects.btagWeight);
    //m_histFill->BookFillHist("MET_Sig_new", 100, 0, 50, MET_Sig_new, readNtuple->Event_Weights,
    //                         m_PhysicsObjects.btagWeight);
    
    m_histFill->BookFillHist("mS_candidate", 80, 0, 400, mS_candidate, readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("MET", 1000, 0, 1000, MET,                                                                                                                             
                             readNtuple->Event_Weights,                                                                                                                             
                             m_PhysicsObjects.btagWeight);
   
    m_histFill->BookFillHist("MET_muInv", 400, 0, 2000, MET_muInv,                                                                                                                 
                             readNtuple->Event_Weights,                                                                                                                             
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillTree(readNtuple->Event_Weights,m_PhysicsObjects.btagWeight,mS_candidate,MET, MET_muInv);
    /*
    m_histFill->BookFillHist("pTV", 1000, 0, 1000, pTV,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("mJJ", 1000, 0, 1000, mJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("Mtop", 1000, 0, 1000, Mtop,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dRJJ", 100, 0, 6, dRJJ, readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("pTJ1", 1000, 0, 1000, pTJ1,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("pTJ2", 1000, 0, 1000, pTJ2,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("pTJJ", 1000, 0, 1500, pTJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    //    m_histFill->BookFillHist("mTW", 1000, 0, 1000, mTW,
    //                         readNtuple->Event_Weights,
    //                         m_PhysicsObjects.btagWeight);
    //m_histFill->BookFillHist("dPhiVBB", 315, 0, 3.15, dPhiVBB,
    //                         readNtuple->Event_Weights,
    //                         m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dPhiLJmin", 350, 0, 3.50, dPhiLJmin,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    //m_histFill->BookFillHist("dYWH", 600, 0, 6.00, dYWH,
    //                         readNtuple->Event_Weights,
    //                         m_PhysicsObjects.btagWeight);
    if (!twoJet) {
      m_histFill->BookFillHist("pTJ3", 1000, 0, 1000, pTJ3,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("mJJJ", 1000, 0, 1000, mJJJ,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
    }

    m_histFill->BookFillHist("pTLep", 500, 0, 500, readNtuple->Lepton_Pt->at(0),
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist(
        "etaLep", 130, -6.5, 6.50, readNtuple->Lepton_Eta->at(0),
        readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist(
        "phiLep", 130, -6.5, 6.50, readNtuple->Lepton_Phi->at(0),
        readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    
    m_histFill->BookFillHist("pTVMJJ", 50, 0, 500, pTV, 50, 0, 500, mJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    */
    // m_histFill->BookFillHist("pTVBDT", 50, 0, 500, pTV, 20, -1, 1, BDT,
    //                          readNtuple->Event_Weights,
    //                          m_PhysicsObjects.btagWeight);
    // m_histFill->BookFillHist("mJJBDT", 50, 0, 500, mJJ, 20, -1, 1, BDT,
    //                          readNtuple->Event_Weights,
    //                          m_PhysicsObjects.btagWeight);
    // m_histFill->BookFillHist("MtopBDT", 50, 0, 500, Mtop, 200, -1, 1, BDT,
    //                          readNtuple->Event_Weights,
    //                          m_PhysicsObjects.btagWeight);

    // if (m_WriteMVATree) m_tree->FillTree();
  }
}


float generateHistogram_1lep::CalculateMtop(const TLorentzVector &lepton,
                                            const TLorentzVector &MET,
                                            const TLorentzVector &b_jet1,
                                            const TLorentzVector &b_jet2) {
  //  bool NeuPz_im;
  float min_mtop, NeuPz_1, NeuPz_2;
  int METShift = 1;
  TLorentzVector neu_tlv_1, neu_tlv_2;
  float mw = 80.385;
  double tmp =
      mw * mw + 2. * lepton.Px() * MET.Px() + 2. * lepton.Py() * MET.Py();
  double METcorr = MET.Pt();
  if (tmp * tmp - pow(2. * lepton.Pt() * MET.Pt(), 2) < 0) {
    //    NeuPz_im = true;
    if (METShift > 0) {
      METcorr = 0.5 * mw * mw /
                (lepton.Pt() - lepton.Px() * cos(MET.Phi()) -
                 lepton.Py() * sin(MET.Phi()));
      double newtmp = mw * mw + 2. * lepton.Px() * METcorr * cos(MET.Phi()) +
                      2. * lepton.Py() * METcorr * sin(MET.Phi());
      NeuPz_1 = lepton.Pz() * newtmp / 2. / lepton.Pt() / lepton.Pt();
      NeuPz_2 = NeuPz_1;
    } else {
      NeuPz_1 = (lepton.Pz() * tmp) / 2. / lepton.Pt() / lepton.Pt();
      NeuPz_2 = NeuPz_1;
    }
  } else {
    NeuPz_1 =
        (lepton.Pz() * tmp +
         lepton.E() * sqrt(tmp * tmp - pow(2. * lepton.Pt() * MET.Pt(), 2))) /
        2. / lepton.Pt() / lepton.Pt();
    NeuPz_2 =
        (lepton.Pz() * tmp -
         lepton.E() * sqrt(tmp * tmp - pow(2. * lepton.Pt() * MET.Pt(), 2))) /
        2. / lepton.Pt() / lepton.Pt();
  }

  neu_tlv_1.SetPxPyPzE(METcorr * cos(MET.Phi()), METcorr * sin(MET.Phi()),
                       NeuPz_1, sqrt(pow(METcorr, 2) + pow(NeuPz_1, 2)));
  neu_tlv_2.SetPxPyPzE(METcorr * cos(MET.Phi()), METcorr * sin(MET.Phi()),
                       NeuPz_2, sqrt(pow(METcorr, 2) + pow(NeuPz_2, 2)));

  float mtop11 = (b_jet1 + lepton + neu_tlv_1).M();
  float mtop12 = (b_jet1 + lepton + neu_tlv_2).M();
  float mtop21 = (b_jet2 + lepton + neu_tlv_1).M();
  float mtop22 = (b_jet2 + lepton + neu_tlv_2).M();
  min_mtop = mtop11;
  if (min_mtop > mtop12) min_mtop = mtop12;
  if (min_mtop > mtop21) min_mtop = mtop21;
  if (min_mtop > mtop22) min_mtop = mtop22;
  return min_mtop;
}

float generateHistogram_1lep::CalculatedYWH(const TLorentzVector &lepton,
                                            const TLorentzVector &MET,
                                            const TLorentzVector &b_jet1,
                                            const TLorentzVector &b_jet2) {
  float NeuPz_1, NeuPz_2;
  float mw = 80.385;
  TLorentzVector neu_tlv, Wz1, Wz2, Hz;
  double tmp =
      mw * mw + 2. * lepton.Px() * MET.Px() + 2. * lepton.Py() * MET.Py();

  if (tmp * tmp - pow(2. * lepton.Pt() * MET.Pt(), 2) < 0) {
    NeuPz_1 = (lepton.Pz() * tmp) / 2. / lepton.Pt() / lepton.Pt();
    NeuPz_2 = NeuPz_1;
  } else {
    NeuPz_1 =
        (lepton.Pz() * tmp +
         lepton.E() * sqrt(tmp * tmp - pow(2. * lepton.Pt() * MET.Pt(), 2))) /
        2. / lepton.Pt() / lepton.Pt();
    NeuPz_2 =
        (lepton.Pz() * tmp -
         lepton.E() * sqrt(tmp * tmp - pow(2. * lepton.Pt() * MET.Pt(), 2))) /
        2. / lepton.Pt() / lepton.Pt();
  }

  Wz1.SetPxPyPzE(0, 0, lepton.Pz() + NeuPz_1,
                 std::sqrt(mw * mw + std::pow(lepton.Pz() + NeuPz_1, 2)));
  Wz2.SetPxPyPzE(0, 0, lepton.Pz() + NeuPz_2,
                 std::sqrt(mw * mw + std::pow(lepton.Pz() + NeuPz_2, 2)));
  Hz.SetPxPyPzE(0, 0, b_jet1.Pz() + b_jet2.Pz(),
                std::sqrt((b_jet1 + b_jet2).M2() +
                          std::pow(b_jet1.Pz() + b_jet2.Pz(), 2)));

  float dBeta1 = std::fabs(Wz1.Beta() - Hz.Beta());
  float dBeta2 = std::fabs(Wz1.Beta() - Hz.Beta());

  if (dBeta1 < dBeta2)
    neu_tlv.SetPxPyPzE(
        MET.Px(), MET.Py(), NeuPz_1,
        sqrt(pow(MET.Px(), 2) + pow(MET.Py(), 2) + pow(NeuPz_1, 2)));
  else
    neu_tlv.SetPxPyPzE(
        MET.Px(), MET.Py(), NeuPz_2,
        sqrt(pow(MET.Px(), 2) + pow(MET.Py(), 2) + pow(NeuPz_2, 2)));

  float dYWH =
      std::fabs((lepton + neu_tlv).Rapidity() - (b_jet1 + b_jet2).Rapidity());

  return dYWH;
}

void generateHistogram_1lep::FillCutFlow1lep(const unsigned long int flag,
                                             float eventWeight,
                                             float btagWeight) {
  // Cuts to exclude from cutflow
  std::vector<unsigned long int> excludeCuts = {};

  // Loop over cuts
  for (unsigned long int i = OneLeptonCutFlow::Input_events;
       i <= OneLeptonCutFlow::AnalysisRegion; ++i) {
    if (std::find(excludeCuts.begin(), excludeCuts.end(), i) !=
        excludeCuts.end())
      continue;

    if (!(passAllCutsUpTo(flag, i, {}))) continue;

    float fullweight = (i >= OneLeptonCutFlow::Input_events)
                           ? eventWeight * btagWeight
                           : eventWeight;

    m_histFill->FillCutflow(i, 1, "Unweighted");
    m_histFill->FillCutflow(i, eventWeight, "EventWeight");
    m_histFill->FillCutflow(i, fullweight, "FullWeight");
  }
}
