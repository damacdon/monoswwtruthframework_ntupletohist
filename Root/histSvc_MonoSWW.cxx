#include "MonoSWWTruthFramework_NtupleToHist/histSvc_MonoSWW.h"
#include "TruthFramework_NtupleToHist/Selection.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_MonoSWW.h"


#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TKey.h>
#include <TList.h>
#include <TRegexp.h>
#include <sys/stat.h>
#include <fstream>
#include <iostream>
#include <vector>
#include "TChain.h"
#include "TLorentzVector.h"
#include "TRandom3.h"

histSvc::histSvc(TString outputPath, bool nominal)
    : m_outputPath("./"),
      m_DSID(-1),
      m_flav_1(-1),
      m_flav_2(-1),
      m_njets(-1),
      m_ntags(-1),
      m_ptv(-1),
      m_description(""),
      m_Lumi(-1),
      m_XSec(-1),
      m_counter(0),
      m_CutBase(false) {
  m_nominal = nominal;
  m_outputPath = outputPath;
  m_histograms.clear();
  m_histoMap.clear();
  m_histoMap_2d.clear();
  m_SumWeights.clear();
  m_SumWeightsMap.clear();
}

histSvc::~histSvc() {}

void histSvc::SetCutBase(bool cutbase) { m_CutBase = cutbase; }

void histSvc::SetAnalysisType(generateHistogram::AnalysisType type) {
  m_type = type;
}

void histSvc::SetRegion(int region) {
  m_Region = region;
}

void histSvc::SetProcess(int DSID, std::string sample) {
  m_DSID = DSID;
  m_sample = sample;
}

void histSvc::SetEventFlav(int flav_1, int flav_2) {
  m_flav_1 = flav_1;
  m_flav_2 = flav_2;
}

void histSvc::get_eventFlavour(int& flav_1, int& flav_2) {
  flav_1 = m_flav_1;
  flav_2 = m_flav_2;
}

void histSvc::SetNjets(int njets) { m_njets = njets; }

void histSvc::SetpTV(float ptv) { m_ptv = ptv; }

void histSvc::SetNtags(int ntags) { m_ntags = ntags; }

void histSvc::SetDescription(TString description) {
  m_description = description;
}

void histSvc::SetLumi(float lumi) { m_Lumi = lumi; }

void histSvc::SetXSec(float xsec) { m_XSec = xsec; }

void histSvc::SetSumWeights(std::vector<float> sumweights) {
  m_SumWeights = sumweights;
}

void histSvc::SetSumWeightsMap(std::map<TString, float> SumWeightsMap) {
  m_SumWeightsMap = SumWeightsMap;
}

void histSvc::SetWeightNames(std::vector<TString> variationNames) {
  m_VariationNames = variationNames;
}

void histSvc::ApplySherpaWeightCut(bool SherpaWeightCut) {
  if (SherpaWeightCut)
    std::cout
        << "Have set config to reduce weights of highly weighted Sherpa events"
        << std::endl;
  m_SherpaWeightCut = SherpaWeightCut;
}

void histSvc::Reset() {
  m_ntags = -1;
  m_flav_1 = -1;
  m_flav_2 = -1;
  m_ptv = -1;
  m_description = "";
  m_CutBase = false;
  m_weightVariations.clear();
}

float histSvc::CorrectionFactor(int variation) {
  return ((m_Lumi * m_XSec) / m_SumWeights.at(variation));
  // Following PMG prescription https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgSystematicUncertaintyRecipes#On_the_fly_variations_using_Sher
}

float histSvc::CorrectionFactor(TString variation) {
  return ((m_Lumi * m_XSec) / m_SumWeightsMap[variation]);
  // Following PMG prescription https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgSystematicUncertaintyRecipes#On_the_fly_variations_using_Sher
}

void histSvc::CutSherpaWeight(std::vector<float>* weights) {
  for (unsigned int w = 0; w < weights->size(); w++) {
    if (weights->at(w) > 100)
      weights->at(w) = 1;
    else if (weights->at(w) < -100)
      weights->at(w) = -1;
  }
}

void histSvc::CutSherpaWeight(std::map<TString, float*>* weights) {
  //Iterator of map
  std::map<TString, float*>::iterator Iter = weights->begin();
  std::map<TString, float*>::iterator IterEnd = weights->end();
  for (; Iter != IterEnd; Iter++) {
    if ((*Iter->second) > 100) {
      (*Iter->second) = 1;
    } else if ((*Iter->second) < -100) {
      (*Iter->second) = -1;
    }
  }
}

void histSvc::BookFillHist(TString varName, int nBins, float xmin, float xmax,
                           float value, std::vector<float>* weights,
                           float ExtraWeight,
                           std::vector<Selection> selections) {
  unsigned int plotsToFill = (m_nominal) ? 1 : m_VariationNames.size();

  if (m_SherpaWeightCut) CutSherpaWeight(weights);

  //Create histograms if they do not exist
  TString histName = GetHistoName("Nominal", varName);
  auto t = m_histoMap.find(histName);  //Why auto!!!!

  if (t == m_histoMap.end()) {
    //Histograms without selections
    for (unsigned int i = 0; i < plotsToFill; ++i) {
      histName = GetHistoName(m_VariationNames.at(i), varName);
      //Store histogram position in m_histograms:
      m_histoMap[histName] = m_histograms.size();
      m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
      m_histograms.back()->GetXaxis()->SetTitle(varName);
      m_histograms.back()->Sumw2();
    }
    // DMM: pTV-inclusive histograms
    histName = GetHistoName("Nominal", varName, false);
    auto t_inclusive = m_histoMap.find(histName);
    if(t_inclusive == m_histoMap.end()){
      for (unsigned int i = 0; i < plotsToFill; ++i) {
	histName = GetHistoName(m_VariationNames.at(i), varName, false);
	//Store histogram position in m_histograms:                                                                                                                                                                                                                       
	m_histoMap[histName] = m_histograms.size();
	m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
	m_histograms.back()->GetXaxis()->SetTitle(varName);
	m_histograms.back()->Sumw2();
      }
    }
    //Histograms with selections
    for (unsigned int i = 0; i < plotsToFill; ++i) {
      for (unsigned int j = 0; j < selections.size(); ++j) {
        histName = GetHistoName(
            m_VariationNames.at(i) + selections.at(j).SelectionName(), varName);
        //Store histogram position in m_histograms:
        m_histoMap[histName] = m_histograms.size();
        m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
        m_histograms.back()->GetXaxis()->SetTitle(varName);
        m_histograms.back()->Sumw2();
      }
    }
    for (unsigned int i = 0; i < m_weightVariations.size(); ++i) {
      histName = GetHistoName(m_weightVariations.at(i).name, varName);
      //Store histogram position in m_histograms:
      //std::cout << histName << std::endl;
      m_histoMap[histName] = m_histograms.size();
      m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
      m_histograms.back()->GetXaxis()->SetTitle(varName);
      m_histograms.back()->Sumw2();
    }
    // Hard coding the RadHi/RadLo variations
    if (m_DSID == 410470 || m_DSID == 410471 || m_DSID == 410472) {
      histName = GetHistoName("RadLo", varName);
      m_histoMap[histName] = m_histograms.size();
      m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
      m_histograms.back()->GetXaxis()->SetTitle(varName);
      m_histograms.back()->Sumw2();
    } else if (m_DSID == 410480 || m_DSID == 410481 || m_DSID == 410482) {
      histName = GetHistoName("RadHi", varName);
      m_histoMap[histName] = m_histograms.size();
      m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
      m_histograms.back()->GetXaxis()->SetTitle(varName);
      m_histograms.back()->Sumw2();
    }
  }

  //Fill no selection.
  for (unsigned int i = 0; i < plotsToFill; ++i) {
    m_histograms.at(m_histoMap[GetHistoName(m_VariationNames.at(i), varName)])
        ->Fill(value,
               weights->at(i) * ExtraWeight *
                   CorrectionFactor(0));  //Request nominal 'i' originally
  }

  // DMM: Fill no selection with inclusive pTV
  for (unsigned int i = 0; i < plotsToFill; ++i) {
    m_histograms.at(m_histoMap[GetHistoName(m_VariationNames.at(i), varName, false)])
      ->Fill(value,
	     weights->at(i) * ExtraWeight *
	     CorrectionFactor(0));  //Request nominal 'i' originally                                                                                                                                                                                              
  }

  //Fill vector of selections. They are at the end of
  for (unsigned int i = 0; i < plotsToFill; ++i) {
    for (unsigned int j = 0; j < selections.size(); ++j) {
      if (selections.at(j).SelectionOutput() == true) {
        m_histograms
            .at(m_histoMap[GetHistoName(
                m_VariationNames.at(i) + selections.at(j).SelectionName(),
                varName)])
            ->Fill(value,
                   weights->at(i) * ExtraWeight *
                       CorrectionFactor(0));  //Request nominal 'i' originally
      }
    }
  }

  for (unsigned int i = 0; i < m_weightVariations.size(); ++i) {
    m_histograms
        .at(m_histoMap[GetHistoName(m_weightVariations.at(i).name, varName)])
        ->Fill(value, weights->at(0) * ExtraWeight * CorrectionFactor(0) *
                          m_weightVariations.at(i).factor);
  }

  // Hard coding the RadHi/RadLo variations
  if (m_DSID == 410470 || m_DSID == 410471 || m_DSID == 410472) {
    float weight = 1 / weights->at(0);
    float sumOfWeights = m_SumWeights.at(0) * m_SumWeights.at(0);
    for (unsigned int i = 0; i < plotsToFill; ++i) {
      if (m_VariationNames.at(i) == "Var3cDown" ||
          m_VariationNames.at(i) == "muR=2.0,muF=2.0") {
        weight *= weights->at(i);
        sumOfWeights /= m_SumWeights.at(i);
      }
    }
    m_histograms.at(m_histoMap[GetHistoName("RadLo", varName)])
        ->Fill(value,
               weight * ExtraWeight * CorrectionFactor(0) * sumOfWeights);
  } else if (m_DSID == 410480 || m_DSID == 410481 || m_DSID == 410482) {
    float weight = 1 / weights->at(0);
    float sumOfWeights = m_SumWeights.at(0) * m_SumWeights.at(0);
    for (unsigned int i = 0; i < plotsToFill; ++i) {
      if (m_VariationNames.at(i) == "Var3cUp" ||
          m_VariationNames.at(i) == "muR=0.5,muF=0.5") {
        weight *= weights->at(i);
        sumOfWeights /= m_SumWeights.at(i);
      }
    }
    m_histograms.at(m_histoMap[GetHistoName("RadHi", varName)])
        ->Fill(value,
               weight * ExtraWeight * CorrectionFactor(0) * sumOfWeights);
  }
}

void histSvc::BookFillHist(TString varName, int nBins, float xmin, float xmax,
                           float value, std::map<TString, float*>* weights,
                           float ExtraWeight,
                           std::vector<Selection> selections) {
  TString histName = GetHistoName("Nominal", varName);
  //unsigned int plotsToFill = (m_nominal) ? 1 : m_VariationNames.size(); //Change to map

  if (m_SherpaWeightCut) CutSherpaWeight(weights);  //Change to map

  //Locally scoped iterator over weights map
  std::map<TString, float*>::iterator Iter = weights->begin();
  std::map<TString, float*>::iterator IterEnd = weights->end();

  //========================================================
  //           Instantiated Hists if non-existant
  //========================================================
  //Create histograms if they do not exist
  auto t = m_histoMap.find(histName);  //Why auto!!!!!
  if (t == m_histoMap.end()) {
    //Histograms without selections

    if (!m_nominal) {
      for (; Iter != IterEnd; Iter++) {
        histName = GetHistoName(Iter->first, varName);
        // Store histogram position in m_histograms:
        m_histoMap[histName] = m_histograms.size();
        m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
        m_histograms.back()->GetXaxis()->SetTitle(varName);
        m_histograms.back()->Sumw2();
      }
      // DMM: inclusive pTV
      histName = GetHistoName("Nominal", varName, false);
      auto t_inclusive = m_histoMap.find(histName);
      if (t_inclusive == m_histoMap.end()) {
	for (; Iter != IterEnd; Iter++) {
	  histName = GetHistoName(Iter->first, varName, false);
	  // Store histogram position in m_histograms:                                                                                                                                                                                                                    
	  m_histoMap[histName] = m_histograms.size();
	  m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
	  m_histograms.back()->GetXaxis()->SetTitle(varName);
	  m_histograms.back()->Sumw2();
	}
      }
      //Histograms with selections
      for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
        for (unsigned int j = 0; j < selections.size(); ++j) {
          histName = GetHistoName(
              Iter->first + selections.at(j).SelectionName(), varName);
          //Store histogram position in m_histograms:
          m_histoMap[histName] = m_histograms.size();
          m_histograms.push_back(
              new TH1F(histName, histName, nBins, xmin, xmax));
          m_histograms.back()->GetXaxis()->SetTitle(varName);
          m_histograms.back()->Sumw2();
        }
      }
      // Hard coding the RadHi/RadLo variations
      if (m_DSID == 410470 || m_DSID == 410471 || m_DSID == 410472) {
        histName = GetHistoName("RadLo", varName);
        m_histoMap[histName] = m_histograms.size();
        m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
        m_histograms.back()->GetXaxis()->SetTitle(varName);
        m_histograms.back()->Sumw2();
      } else if (m_DSID == 410480 || m_DSID == 410481 || m_DSID == 410482) {
        histName = GetHistoName("RadHi", varName);
        m_histoMap[histName] = m_histograms.size();
        m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
        m_histograms.back()->GetXaxis()->SetTitle(varName);
        m_histograms.back()->Sumw2();
      }

    } else {  //If nominal set to false end
      //Load just nominal
      histName = GetHistoName("Nominal", varName);
      // Store histogram position in m_histograms:
      // std::cout << histName << std::endl;
      m_histoMap[histName] = m_histograms.size();
      m_histograms.push_back(new TH1F(histName, histName, nBins, xmin, xmax));
      m_histograms.back()->GetXaxis()->SetTitle(varName);
      m_histograms.back()->Sumw2();
    }
  }

  //========================================================
  //           Now fill instantiated Hists
  //========================================================
  if (!m_nominal) {
    //-----------------------------------------------------------------------
    //Fill nominal weight but with the variation weight
    for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
      float weight = (*Iter->second);
      m_histograms.at(m_histoMap[GetHistoName(Iter->first, varName)])
          ->Fill(value, weight * ExtraWeight * CorrectionFactor("Nominal"));
    }

    ///                      WHICH ONE OF THESE TWO ARE RIGHT !!!!!!!!!!!

    //Fill nominal weight but the correction factor is the ratio of sum of weights between variation & nominal
    //   Reweight factor = var(Sum of weights) / nominal(sum of weights)
    for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
      float nominal_weight = (*weights->at("Nominal"));
      float factor = (*Iter->second) / nominal_weight;
      m_histograms.at(m_histoMap[GetHistoName(Iter->first, varName)])
          ->Fill(value, nominal_weight * ExtraWeight *
                            CorrectionFactor("Nominal") * factor);
    }
    //-----------------------------------------------------------------------

    // DMM: inclusive pTV
    for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
      float nominal_weight = (*weights->at("Nominal"));
      float factor = (*Iter->second) / nominal_weight;
      m_histograms.at(m_histoMap[GetHistoName(Iter->first, varName, false)])
	->Fill(value, nominal_weight * ExtraWeight *
	       CorrectionFactor("Nominal") * factor);
    }

    //Fill vector of selections. They are at the end of
    for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
      for (unsigned int j = 0; j < selections.size(); ++j) {
        if (selections.at(j).SelectionOutput() == true) {
          float weight = (*Iter->second);
          m_histograms
              .at(m_histoMap[GetHistoName(
                  Iter->first + selections.at(j).SelectionName(),
                  varName)])  //Change to map
              ->Fill(value, weight * ExtraWeight * CorrectionFactor("Nominal"));
        }
      }
    }

    // Hard coding the RadHi/RadLo variations
    if (m_DSID == 410470 || m_DSID == 410471 || m_DSID == 410472) {
      float weight = 1 / (*weights->at("Nominal"));
      float sumOfWeights =
          m_SumWeightsMap["Nominal"] * m_SumWeightsMap["Nominal"];
      for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
        if (Iter->first == "Var3cDown" || Iter->first == "muR=2.0,muF=2.0") {
          float this_weight = (*Iter->second);
          weight *= this_weight;
          sumOfWeights /= m_SumWeightsMap.at(Iter->first);
        }
      }
      m_histograms.at(m_histoMap[GetHistoName("RadLo", varName)])
          ->Fill(value, weight * ExtraWeight * CorrectionFactor("Nominal") *
                            sumOfWeights);
    } else if (m_DSID == 410480 || m_DSID == 410481 || m_DSID == 410482) {
      float weight = 1 / (*weights->at("Nominal"));
      float sumOfWeights = m_SumWeightsMap.at("Nominal");
      m_SumWeightsMap.at("Nominal");
      for (Iter = weights->begin(); Iter != IterEnd; Iter++) {
        if (Iter->first == "Var3cUp" || Iter->first == "muR=0.5,muF=0.5") {
          float this_weight = (*Iter->second);
          weight *= this_weight;
          sumOfWeights /= m_SumWeightsMap.at(Iter->first);
        }
      }
      m_histograms.at(m_histoMap[GetHistoName("RadHi", varName)])
          ->Fill(value, weight * ExtraWeight * CorrectionFactor("Nominal") *
                            sumOfWeights);
    }

  } else {  //Else just fill nominal
    float weight = (*weights->at("Nominal"));
    m_histograms.at(m_histoMap[GetHistoName("Nominal", varName)])
        ->Fill(value, weight * ExtraWeight * CorrectionFactor("Nominal"));
  }
}

void histSvc::BookFillHist(TString varName, int nBinsX, float xmin, float xmax,
                           float xvalue, int nBinsY, float ymin, float ymax,
                           float yvalue, std::vector<float>* weights,
                           float ExtraWeight,
                           std::vector<Selection> selections) {
  TString histName = GetHistoName("Nominal", varName);
  unsigned int plotsToFill = (m_nominal) ? 1 : m_VariationNames.size();

  // Create histograms if they do not exist
  auto t = m_histoMap_2d.find(histName);
  if (t == m_histoMap_2d.end()) {
    //Histograms without selections
    for (unsigned int i = 0; i < plotsToFill; ++i) {
      histName = GetHistoName(m_VariationNames.at(i), varName);
      m_histoMap_2d[histName] = m_histograms_2d.size();
      m_histograms_2d.push_back(
          new TH2F(histName, histName, nBinsX, xmin, xmax, nBinsY, ymin, ymax));
      m_histograms_2d.back()->Sumw2();
    }
    // DMM: inclusive pTV
    histName = GetHistoName("Nominal", varName, false);
    auto t_inclusive = m_histoMap_2d.find(histName);
    if (t_inclusive == m_histoMap_2d.end()) {
      for (unsigned int i = 0; i < plotsToFill; ++i) {
	histName = GetHistoName(m_VariationNames.at(i), varName, false);
	m_histoMap_2d[histName] = m_histograms_2d.size();
	m_histograms_2d.push_back(
	    new TH2F(histName, histName, nBinsX, xmin, xmax, nBinsY, ymin, ymax));
	m_histograms_2d.back()->Sumw2();
      }
    }
    //Histograms with selections
    for (unsigned int i = 0; i < plotsToFill; ++i) {
      for (unsigned int j = 0; j < selections.size(); ++j) {
        histName = GetHistoName(
            m_VariationNames.at(i) + selections.at(j).SelectionName(), varName);
        //Store histogram position in m_histograms:
        m_histoMap_2d[histName] = m_histograms_2d.size();
        m_histograms_2d.push_back(new TH2F(histName, histName, nBinsX, xmin,
                                           xmax, nBinsY, ymin, ymax));
        m_histograms_2d.back()->Sumw2();
      }
    }
    for (unsigned int i = 0; i < m_weightVariations.size(); ++i) {
      histName = GetHistoName(m_weightVariations.at(i).name, varName);
      m_histoMap_2d[histName] = m_histograms_2d.size();
      m_histograms_2d.push_back(
          new TH2F(histName, histName, nBinsX, xmin, xmax, nBinsY, ymin, ymax));
      m_histograms_2d.back()->Sumw2();
    }
  }

  //Fill no selection.
  for (unsigned int i = 0; i < plotsToFill; ++i) {
    m_histograms_2d
        .at(m_histoMap_2d[GetHistoName(m_VariationNames.at(i), varName)])
        ->Fill(xvalue, yvalue,
               weights->at(i) * ExtraWeight *
                   CorrectionFactor(0));  //Request nominal 'i' originally
  }

  // DMM: Fill inclusive pTV
  for (unsigned int i = 0; i < plotsToFill; ++i) {
    m_histograms_2d
      .at(m_histoMap_2d[GetHistoName(m_VariationNames.at(i), varName, false)])
      ->Fill(xvalue, yvalue,
	     weights->at(i) * ExtraWeight *
	     CorrectionFactor(0));  //Request nominal 'i' originally                                                                                                                                                                                              
  }

  //Fill vector of selections. They are at the end of
  for (unsigned int i = 0; i < plotsToFill; ++i) {
    for (unsigned int j = 0; j < selections.size(); ++j) {
      if (selections.at(j).SelectionOutput() == true) {
        m_histograms_2d
            .at(m_histoMap_2d[GetHistoName(
                m_VariationNames.at(i) + selections.at(j).SelectionName(),
                varName)])
            ->Fill(xvalue, yvalue,
                   weights->at(i) * ExtraWeight *
                       CorrectionFactor(0));  //Request nominal 'i' originally
      }
    }
  }
  //Fill no selection.
  for (unsigned int i = 0; i < m_weightVariations.size(); ++i) {
    m_histograms_2d
        .at(m_histoMap_2d[GetHistoName(m_weightVariations.at(i).name, varName)])
        ->Fill(xvalue, yvalue,
               weights->at(0) * ExtraWeight * CorrectionFactor(0) *
                   m_weightVariations.at(i).factor);
  }
}

void histSvc::BookFillTree(std::vector<float>* weights,float ExtraWeight, float mS_candidate, float MET, float pTV){

    float total_weight = ExtraWeight*weights->at(0)*CorrectionFactor(0);

    if (m_counter == 0) {
    m_tree->Branch("total_weight",&total_weight,"total_weight/F");
    m_tree->Branch("mS_candidate",&mS_candidate,"mS_candidate/F");
    m_tree->Branch("MET",&MET,"MET/F");
    m_tree->Branch("pTV",&pTV,"pTV/F");
    }
    m_counter++;

    m_tree->Fill();
}

void histSvc::FillCutflow(int bin, float weight, TString name) {
  if (name == "Unweighted")
    m_cutflow_Unweighted->Fill(bin, weight);
  else if (name == "EventWeight")
    m_cutflow_EventWeight->Fill(bin, weight);
  else if (name == "FullWeight")
    m_cutflow_FullWeight->Fill(bin, weight);
}

void histSvc::WriteHists() {
  system("mkdir -p " + m_outputPath);

  TString outputFileName = m_outputPath;
  TString outputFileName_tree = m_outputPath;
  outputFileName += "/output_";
  outputFileName_tree += "/output_";
  if (m_type == generateHistogram::ZeroLepton) outputFileName += "0Lep_";
  if (m_type == generateHistogram::OneLepton) outputFileName += "1Lep_";
  if (m_type == generateHistogram::TwoLepton) outputFileName += "2Lep_";

  if (m_type == generateHistogram::ZeroLepton) outputFileName_tree += "0Lep_";
  if (m_type == generateHistogram::OneLepton) outputFileName_tree += "1Lep_";
  if (m_type == generateHistogram::TwoLepton) outputFileName_tree += "2Lep_";

  if(m_Region == 0) outputFileName += "Merged_";
  if(m_Region == 1) outputFileName += "Intermediate_";
  if(m_Region == 2) outputFileName += "Resolved_";

  if(m_Region == 0) outputFileName_tree += "Merged_";
  if(m_Region == 1) outputFileName_tree += "Intermediate_";
  if(m_Region == 2) outputFileName_tree += "Resolved_";

  outputFileName += m_DSID;
  outputFileName_tree += m_DSID;
  outputFileName_tree += "_tree";
  outputFileName += ".root";
  outputFileName_tree += ".root";
  TFile* f = new TFile(outputFileName, "RECREATE");
  TFile* f1 = new TFile(outputFileName_tree, "RECREATE");
  TDirectory* cutflow = f->mkdir("Cutflow");
  TDirectory* syst = f->mkdir("WeightVariations");

  TString histName;

  f->cd();
  for (unsigned int i = 0; i < m_histograms.size(); ++i) {
    histName = m_histograms.at(i)->GetName();
    cout << histName << endl;
    if (histName.Contains("Nominal"))
      f->cd();
    else if (!m_nominal)
      syst->cd();
    else
      continue;
    m_histograms.at(i)->Write();
    delete m_histograms.at(i);
  }

  for (unsigned int i = 0; i < m_histograms_2d.size(); ++i) {
    histName = m_histograms_2d.at(i)->GetName();
    if (histName.Contains("Nominal"))
      f->cd();
    else if (!m_nominal)
      syst->cd();
    else
      continue;
    m_histograms_2d.at(i)->Write();
    delete m_histograms_2d.at(i);
  }

  f->cd();

  cutflow->cd();

  // Set the analysis region cut name
  std::string RegionCut_str;
  if(m_Region == 0) RegionCut_str = "Merged Region";
  if(m_Region == 1) RegionCut_str = "Intermediate Region";
  if(m_Region == 2) RegionCut_str = "Resolved Region";

  if (m_type == generateHistogram::ZeroLepton) {
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(1, "Input events");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(2, "MET > 200 GeV");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(3, "Tau Veto");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(4, "MinDPhiMETJets");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(5, "MET_Sig > 15");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(6, "mS_window");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(7, RegionCut_str.c_str());

    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(1, "Input events");
    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(2, "MET > 200 GeV");
    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(3, "Tau Veto");
    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(4, "MinDPhiMETJets");
    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(5, "MET_Sig > 15");
    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(6, "mS_window");
    m_cutflow_EventWeight->GetXaxis()->SetBinLabel(7, RegionCut_str.c_str());

    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(1, "Input events");
    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(2, "MET > 200 GeV");
    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(3, "Tau Veto");
    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(4, "MinDPhiMETJets");
    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(5, "MET_Sig > 15");
    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(6, "mS_window");
    m_cutflow_FullWeight->GetXaxis()->SetBinLabel(7, RegionCut_str.c_str());
  }

  if (m_type == generateHistogram::OneLepton) {
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(1, "Input events");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(2, "Lepton is muon");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(3, "Lepton pt > 25 GeV");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(4, "MET (mu inv) > 200 GeV");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(5, "Tau Veto");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(6, "MinDPhiMETJets");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(7, "MET_Sig > 15");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(8, "mS_window");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(9, RegionCut_str.c_str());
  }

  if (m_type == generateHistogram::TwoLepton) {
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(1, "Input events");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(2, "2 signal lepton");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(3, "Tau veto");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(4, "pt_ll > 200 GeV");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(5, "Z mass cut");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(6, "MinDPhiMETJetsResolved");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(7, "MET Significance");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(8, "mS_window");
    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(9, RegionCut_str.c_str());
    //    m_cutflow_Unweighted->GetXaxis()->SetBinLabel(5,"pT1 > 45 GeV");
    // m_cutflow_Unweighted->GetXaxis()->SetBinLabel(6,"pTV > 150 GeV");
  }

  m_cutflow_Unweighted->Write();
  m_cutflow_EventWeight->Write();
  m_cutflow_FullWeight->Write();
  
  f1->cd();

  m_tree->Write();

  delete m_tree;

  delete m_cutflow_Unweighted;
  delete m_cutflow_EventWeight;
  delete m_cutflow_FullWeight;

  cutflow->Close();
  delete cutflow;
  syst->Close();
  delete syst;
  f->Close();
  delete f;
  f1->Close();
  delete f1;
}

TString histSvc::TruthFlavLabel() {
  // Note that taus are currently being treated as light!
  // std::cout << "Inside TruthFlavLabel" << std::endl;
  // std::cout << m_flav_1 << " " << m_flav_2 << std::endl;
  // std::cout << m_flav_1+ m_flav_2 << std::endl;
  // FN: for our MonoSWW probably we don't want to split V+jets in flavor bins.
  // NB this change will make the job w/o touching the other generateHistogram files.
  return "all";
  //
  
  if (m_flav_1 + m_flav_2 == 30)
    return "ll";
  else if (m_flav_1 + m_flav_2 == 20)
    return "bl";
  else if (m_flav_1 + m_flav_2 == 19)
    return "cl";
  else if (m_flav_1 + m_flav_2 == 15)
    return "ll";
  else if (m_flav_1 + m_flav_2 == 10)
    return "bb";
  else if (m_flav_1 + m_flav_2 == 9)
    return "bc";
  else if (m_flav_1 + m_flav_2 == 5)
    return "bl";
  else if (m_flav_1 + m_flav_2 == 8)
    return "cc";
  else if (m_flav_1 + m_flav_2 == 4)
    return "cl";
  else if (m_flav_1 + m_flav_2 == 0)
    return "ll";
  else {
    std::cout << "Could not find truth flavour matching for "
              << m_flav_1 + m_flav_2 << " - exiting" << std::endl;
    exit(0);
  }
}

TString histSvc::GetHistoName(TString variation, TString varName, bool do_ptv) {
  TString histogramName = "hist_";

  if (m_type == generateHistogram::ZeroLepton)
    histogramName += "0Lep";
  else if (m_type == generateHistogram::OneLepton)
    histogramName += "1Lep";
  else if (m_type == generateHistogram::TwoLepton)
    histogramName += "2Lep";

  histogramName += "_";

  histogramName += m_sample;

  if (m_sample == "ttbar" || m_sample == "W" || m_sample == "Z" ||
      m_sample ==
          "stopWt") {  //Add the sample name here if you want to include the jet flavour label
    histogramName += TruthFlavLabel();
  }

  // DMM: Commenting out the info about number of b-tags and resolved jets
  /*
  if (m_ntags == 1)
    histogramName += "1tag";
  else if (m_ntags == 2)
    histogramName += "2tag";
  else if (m_ntags < 0)
    histogramName += "0ptag";

  if (m_njets == 2)
    histogramName += "2jet";
  else if (m_njets == 3)
    histogramName += "3jet";
  else if (m_njets > 3)
    histogramName += "4pjet";
  else if (m_njets < 0)
    histogramName += "0pjet";
  histogramName += "_";
  */

  // DMM: Updated with XAMPP MET binning
  histogramName += "_";
  if(do_ptv) {
    if (!m_CutBase) {
      if (m_ptv > 200 && m_ptv <= 300)
	histogramName += "200_300ptv";
      if (m_ptv > 300 && m_ptv <= 500)
	histogramName += "300_500ptv";
      if (m_ptv > 500)
	histogramName += "500ptv";
    } else if (m_CutBase) {
      if (m_ptv > 200 && m_ptv <= 300)
	histogramName += "200_300ptv";
      if (m_ptv > 300 && m_ptv <= 500)
	histogramName += "300_500ptv";
      if (m_ptv > 500)
	histogramName += "500ptv";
    }
  }
  else histogramName += "allptv";

  histogramName += "_";

  histogramName += m_description;
  histogramName += "_";

  histogramName += varName;
  histogramName += "_";

  histogramName += variation;

  //  histogramName.ReplaceAll(" ","");

  return histogramName;
}
