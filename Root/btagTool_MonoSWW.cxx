#include "MonoSWWTruthFramework_NtupleToHist/btagTool_MonoSWW.h"
#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TRegexp.h>
#include <TSystem.h>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <vector>
#include "TChain.h"
#include "TLorentzVector.h"
#include "TRandom3.h"

btagTool::btagTool(std::string shower)
    : hTruthMap_b(NULL),
      hTruthMap_c(NULL),
      hTruthMap_l(NULL),
      hTruthMap_t(NULL) {
  m_Shower = shower;
}

btagTool::~btagTool() {
  delete hTruthMap_b;
  delete hTruthMap_c;
  delete hTruthMap_l;
  delete hTruthMap_t;
}

void btagTool::compute_btagweight(std::vector<TLorentzVector>& signalJets, std::vector<int>& signalJetFlavour,
    float& btagWeight ){
  if (hTruthMap_b == NULL) LoadTruthMaps();

  int nTags = 0;  // setting the desired number of tags // 

  std::vector<float> btagEff;

  // std::cout << "debug 1 "<< std::endl;

  for (unsigned int i = 0; i < signalJets.size(); ++i) {
  // std::cout << "debug 1 "<<i << std::endl;
  // std::cout << signalJets.at(i).Pt()<< " " << signalJets.at(i).Eta()<< std::endl;
    btagEff.push_back(getTagEff(
        signalJets.at(i),
        signalJetFlavour.at(
            i)));  //this is the real work-horse of the pseudo b-tagging efficiency tool
  }
  // std::cout << "debug 2 "<< std::endl;

  float weight = 0;
  if (nTags == 0) {
    float temp_weight = 1;
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      // std::cout << "debug 2 "<<i << std::endl;
      temp_weight *= (1 - btagEff.at(i));
    }
    weight = temp_weight;
    
  }
  // std::cout << "debug 3 "<< std::endl;

  btagWeight = weight;

  return ;

}


std::vector<unsigned int> btagTool::compute_btagging(
    std::vector<TLorentzVector>& signalJets, std::vector<int>& signalJetFlavour,
    float& btagWeight) {
  if (hTruthMap_b == NULL) LoadTruthMaps();

  int nTags = 0;  // setting the desired number of tags

  std::vector<float> btagEff;

  for (unsigned int i = 0; i < signalJets.size(); ++i) {
    btagEff.push_back(getTagEff(
        signalJets.at(i),
        signalJetFlavour.at(
            i)));  //this is the real work-horse of the pseudo b-tagging efficiency tool
  }

  std::vector<float> choices;
  std::vector<std::vector<unsigned int> > combinations;
  float weight = 0;
  if (nTags == 0) {
    float temp_weight = 1;
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      temp_weight *= (1 - btagEff.at(i));
    }
    weight += temp_weight;
    
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      float temp_weight = 0;
      for (unsigned int j = i + 1; j < signalJets.size(); ++j) {
        // temp_weight = btagEff.at(i) * btagEff.at(j);
        temp_weight = ( 1 - btagEff.at(i)) * ( 1 - btagEff.at(j) ); // FN none of them is b-tagged
        for (unsigned int k = 0; k < signalJets.size(); ++k) {
          if (k == i || k == j) continue;
          temp_weight *= (1 - btagEff.at(k));
        }
        // weight += temp_weight;
        choices.push_back(temp_weight);
        combinations.push_back({i, j});
      }
    }
    
  }
  bool debug_dev = false;

  int choice = roll_dice(choices);
   if (debug_dev) std::cout << ">>>>>>>>>>>>>>>>>>>>>" << std::endl;
   if (debug_dev)std::cout << "flav 1 " << signalJetFlavour.at(0) << " flav 2 " << signalJetFlavour.at(1) << " flav 3 " << signalJetFlavour.at(2) << std::endl;
   if (debug_dev)std::cout << "btagEff.at(0) "  << btagEff.at(0) << " btagEff.at(1) " << btagEff.at(1) << " btagEff.at(2) " << btagEff.at(2) << std::endl;
   if (debug_dev)std::cout << "choices.at(choice) " << choices.at(choice) << std::endl;
   if (debug_dev)std::cout << "weight " << weight << std::endl;
  btagWeight = weight;

if (debug_dev)
   for(int i=0; i<combinations.size(); i++){
     std::cout << combinations.at(i).at(0) << " " << combinations.at(i).at(1) << std::endl;
     std::cout << signalJetFlavour.at(combinations.at(i).at(0)) << " " << signalJetFlavour.at(combinations.at(i).at(1)) << std::endl;
   }

   if (debug_dev)std::cout << "The chosen flavour is" << std::endl;
   if (debug_dev)std::cout << signalJetFlavour.at(combinations.at(choice).at(0)) << " " << signalJetFlavour.at(combinations.at(choice).at(1)) << std::endl;
  return combinations.at(
      choice);  // returns the two elements which are b-tagged
}

float btagTool::getTagEff(TLorentzVector jet, int jetFlav) {
  float jetpT = jet.Pt();
  float jetEta = fabs(jet.Eta());

  if (jetFlav == 5) {
    if (jetpT > hTruthMap_b->GetYaxis()->GetBinCenter(hTruthMap_b->GetNbinsY()))
      jetpT = hTruthMap_b->GetYaxis()->GetBinCenter(hTruthMap_b->GetNbinsY());
    while (hTruthMap_b->GetBinContent(
               hTruthMap_b->GetXaxis()->FindBin(fabs(jetEta)),
               hTruthMap_b->GetYaxis()->FindBin(jetpT)) == 0) {
      jetEta = hTruthMap_b->GetXaxis()->GetBinCenter(
          hTruthMap_b->GetXaxis()->FindBin(fabs(jetEta)) -
          1);  //Sometimes empty bin at high eta, in this case move one bin down
    }
    return hTruthMap_b->GetBinContent(
        hTruthMap_b->GetXaxis()->FindBin(fabs(jetEta)),
        hTruthMap_b->GetYaxis()->FindBin(jetpT));
  } else if (jetFlav == 4) {
    if (jetpT > hTruthMap_c->GetYaxis()->GetBinCenter(hTruthMap_c->GetNbinsY()))
      jetpT = hTruthMap_c->GetYaxis()->GetBinCenter(hTruthMap_c->GetNbinsY());
    while (hTruthMap_c->GetBinContent(
               hTruthMap_c->GetXaxis()->FindBin(fabs(jetEta)),
               hTruthMap_c->GetYaxis()->FindBin(jetpT)) == 0) {
      jetEta = hTruthMap_c->GetXaxis()->GetBinCenter(
          hTruthMap_c->GetXaxis()->FindBin(fabs(jetEta)) -
          1);  //Sometimes empty bin at high eta, in this case move one bin down
    }
    return hTruthMap_c->GetBinContent(
        hTruthMap_c->GetXaxis()->FindBin(fabs(jetEta)),
        hTruthMap_c->GetYaxis()->FindBin(jetpT));
  } else if (jetFlav == 15) {
    if (jetpT > hTruthMap_t->GetYaxis()->GetBinCenter(hTruthMap_t->GetNbinsY()))
      jetpT = hTruthMap_t->GetYaxis()->GetBinCenter(hTruthMap_t->GetNbinsY());
    while (hTruthMap_t->GetBinContent(
               hTruthMap_t->GetXaxis()->FindBin(fabs(jetEta)),
               hTruthMap_t->GetYaxis()->FindBin(jetpT)) == 0) {
      jetEta = hTruthMap_t->GetXaxis()->GetBinCenter(
          hTruthMap_t->GetXaxis()->FindBin(fabs(jetEta)) -
          1);  //Sometimes empty bin at high eta, in this case move one bin down
    }
    return hTruthMap_t->GetBinContent(
        hTruthMap_t->GetXaxis()->FindBin(fabs(jetEta)),
        hTruthMap_t->GetYaxis()->FindBin(jetpT));
  } else if (jetFlav == 0) {
    if (jetpT > hTruthMap_l->GetYaxis()->GetBinCenter(hTruthMap_l->GetNbinsY()))
      jetpT = hTruthMap_l->GetYaxis()->GetBinCenter(hTruthMap_l->GetNbinsY());
    while (hTruthMap_l->GetBinContent(
               hTruthMap_l->GetXaxis()->FindBin(fabs(jetEta)),
               hTruthMap_l->GetYaxis()->FindBin(jetpT)) == 0) {
      jetEta = hTruthMap_l->GetXaxis()->GetBinCenter(
          hTruthMap_l->GetXaxis()->FindBin(fabs(jetEta)) -
          1);  //Sometimes empty bin at high eta, in this case move one bin down
    }
    return hTruthMap_l->GetBinContent(
        hTruthMap_l->GetXaxis()->FindBin(fabs(jetEta)),
        hTruthMap_l->GetYaxis()->FindBin(jetpT));
  } else if (jetFlav == -999)
    return 0.0;
  else {
    std::cout << "Jet flavour not found - exiting" << std::endl;
    exit(0);
  }
}

int btagTool::roll_dice(const std::vector<float>& choices) {
  static TRandom3 rand;
  double total = 0;
  rand.SetSeed(123456);
  //  rand.SetSeed(time(NULL));
  for (auto choice : choices) total += choice;
  double dice = rand.Rndm(), choice = total * dice, tat = 0;
    // std::cout<<"total: "<<total<<", choice: "<<choice<<", dice: "<<dice<<std::endl;
  for (unsigned int i = 0; i < choices.size(); i++) {
    tat += choices.at(i);
    // std::cout << i << " tat " << tat << std::endl;
    if (choice < tat) return i;
  }
  return choices.size() - 1;
}

void btagTool::LoadTruthMaps() {
  std::cout << "Loading b-tagging truth maps" << std::endl;

  std::string m_WorkDir_DIR = gSystem->Getenv("WorkDir_DIR");

  TFile* truthMapFile = TFile::Open(
      (TString)(m_WorkDir_DIR +
                "/data/TruthFramework_NtupleToHist/outMaps_r21.root"),
      "READ");

  if (m_Shower == "Sherpa") {
    std::cout << "Loaded Sherpa truth maps" << std::endl;
    hTruthMap_b = (TH2D*)truthMapFile->Get("77_B_Sherpa");
    hTruthMap_c = (TH2D*)truthMapFile->Get("77_C_Sherpa");
    hTruthMap_l = (TH2D*)truthMapFile->Get("77_Light_Sherpa");
    hTruthMap_t = (TH2D*)truthMapFile->Get("77_T_Sherpa");
    hTruthMap_b->SetDirectory(0);
    hTruthMap_c->SetDirectory(0);
    hTruthMap_l->SetDirectory(0);
    hTruthMap_t->SetDirectory(0);
  } else if (m_Shower == "Pythia8") {
    std::cout << "Loaded Pythia8 truth maps" << std::endl;
    hTruthMap_b = (TH2D*)truthMapFile->Get("77_B_Pythia8EvtGen");
    hTruthMap_c = (TH2D*)truthMapFile->Get("77_C_Pythia8EvtGen");
    hTruthMap_l = (TH2D*)truthMapFile->Get("77_Light_Pythia8EvtGen");
    hTruthMap_t = (TH2D*)truthMapFile->Get("77_T_Pythia8EvtGen");
    hTruthMap_b->SetDirectory(0);
    hTruthMap_c->SetDirectory(0);
    hTruthMap_l->SetDirectory(0);
    hTruthMap_t->SetDirectory(0);
  } else if (m_Shower == "Pythia6") {
    std::cout << "Loaded Pythia6 truth maps" << std::endl;
    hTruthMap_b = (TH2D*)truthMapFile->Get("77_B_Pythia6");
    hTruthMap_c = (TH2D*)truthMapFile->Get("77_C_Pythia6");
    hTruthMap_l = (TH2D*)truthMapFile->Get("77_Light_Pythia6");
    hTruthMap_t = (TH2D*)truthMapFile->Get("77_T_Pythia6");
    hTruthMap_b->SetDirectory(0);
    hTruthMap_c->SetDirectory(0);
    hTruthMap_l->SetDirectory(0);
    hTruthMap_t->SetDirectory(0);
  } else if (m_Shower == "Herwig7") {
    std::cout << "Loaded Herwig7 truth maps" << std::endl;
    hTruthMap_b = (TH2D*)truthMapFile->Get("77_B_Herwig7");
    hTruthMap_c = (TH2D*)truthMapFile->Get("77_C_Herwig7");
    hTruthMap_l = (TH2D*)truthMapFile->Get("77_Light_Herwig7");
    hTruthMap_t = (TH2D*)truthMapFile->Get("77_T_Herwig7");
    hTruthMap_b->SetDirectory(0);
    hTruthMap_c->SetDirectory(0);
    hTruthMap_l->SetDirectory(0);
    hTruthMap_t->SetDirectory(0);
  } else {
    std::cout << "LoadTruthMaps did not find shower: " << m_Shower << std::endl;
  }
}
