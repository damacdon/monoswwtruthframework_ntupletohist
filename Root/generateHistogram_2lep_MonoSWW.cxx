#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_2lep_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_MonoSWW.h"

#include "MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/btagTool_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/histSvc_MonoSWW.h"

#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <fstream>
#include <iostream>
#include <vector>

generateHistogram_2lep::generateHistogram_2lep() {}

generateHistogram_2lep::~generateHistogram_2lep() {}

void generateHistogram_2lep::FillHistograms() {
  Selection2Lepton();  //Needed to steer the code to run the 1-lepton selection
}

bool generateHistogram_2lep::BuildEvent(ReadTruthTree &readNtuple,
                                        EventObjects &PhysicsObjects,
                                        btagTool &BtagTool, bool Debug, int Region) {
  //std::cout << " Building Events " << std::endl;

  //FN: building the alljets for the b-tag veto
  for (unsigned int i = 0 ; i < readNtuple.Jet_Phi->size(); i++ ){
    if (Debug)
      std::cout << "<generateHistogram_0lep::BuildEvent()>::    "<< i+1 << " Jets";
    TLorentzVector tmp;
    tmp.SetPtEtaPhiM(
		     readNtuple.Jet_Pt->at(i), readNtuple.Jet_Eta->at(i),
		     readNtuple.Jet_Phi->at(i), readNtuple.Jet_Mass->at(i));
    PhysicsObjects.allJets.push_back(tmp);
  }

  //Check for either 2 AntiKt4 jets or 1 large-R jet
  if (readNtuple.nSignalJet < 2) {
    return false;
  }

  //Build lepton side of eventOA
  if (static_cast<int>(readNtuple.Lepton_ID->size()) < 1) {
    //std::cout << "<BuildEvent()>::   Leptons in event < 1, creation of lepton TLorentzVectors:: impossible. Invalid event." << std::endl;
    return false;
  }

  if (static_cast<int>(readNtuple.Lepton_ID->size()) != 2) {
    std::cout << "<BuildEvent()>::   Number of leptons in event is not 2. "
               "Something went wrong, exiting."
              << std::endl;
    exit(0);
  }
  //else std::cout << "<BuildEvent()>::   Number of leptons in event is correctly 2." << std::endl;
  
  PhysicsObjects.LeptonVec1.SetPtEtaPhiM(
      readNtuple.Lepton_Pt->at(0), readNtuple.Lepton_Eta->at(0),
      readNtuple.Lepton_Phi->at(0), readNtuple.Lepton_Mass->at(0));
  PhysicsObjects.LeptonTransVec1.SetPtEtaPhiM(
      readNtuple.Lepton_Pt->at(0),
      0.0,  //eta for missing energy not defined by construction
      readNtuple.Lepton_Phi->at(0), readNtuple.Lepton_Mass->at(0));

  PhysicsObjects.LeptonVec2.SetPtEtaPhiM(
      readNtuple.Lepton_Pt->at(1), readNtuple.Lepton_Eta->at(1),
      readNtuple.Lepton_Phi->at(1), readNtuple.Lepton_Mass->at(1));
  PhysicsObjects.LeptonTransVec2.SetPtEtaPhiM(
      readNtuple.Lepton_Pt->at(1),
      0.0,  //eta for missing energy not defined by construction
      readNtuple.Lepton_Phi->at(1), readNtuple.Lepton_Mass->at(1));

  PhysicsObjects.Lepton1Flav = readNtuple.Lepton_ID->at(0);
  PhysicsObjects.Lepton2Flav = readNtuple.Lepton_ID->at(1);

  //------------------------ Build up to 4 leading resolved jets ------------------------// 

  //Build two leading jets  
  if (readNtuple.nSignalJet < 2) {
    //std::cout << "<BuildEvent()>::   Number of signal jets in event < 2, creation of jet TLorentzVector:: impossible. Invalid event." << std::endl;
    return false;
  }
  PhysicsObjects.jet1vec.SetPtEtaPhiM(
      readNtuple.Jet_Pt->at(0), readNtuple.Jet_Eta->at(0),
      readNtuple.Jet_Phi->at(0), readNtuple.Jet_Mass->at(0));
  PhysicsObjects.jet2vec.SetPtEtaPhiM(
      readNtuple.Jet_Pt->at(1), readNtuple.Jet_Eta->at(1),
      readNtuple.Jet_Phi->at(1), readNtuple.Jet_Mass->at(1));

  // Build the 3rd resolved jet
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved Jets 3"
              << std::endl;
  if (readNtuple.nSignalJet >= 3) {
    PhysicsObjects.jet3vec.SetPtEtaPhiM(
                                        readNtuple.Jet_Pt->at(2), readNtuple.Jet_Eta->at(2),
                                        readNtuple.Jet_Phi->at(2), readNtuple.Jet_Mass->at(2));
  }
  // Build the 4th resolved jet
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved Jets 4"
              << std::endl;
  if (readNtuple.nSignalJet == 4) {
    PhysicsObjects.jet4vec.SetPtEtaPhiM(
                                        readNtuple.Jet_Pt->at(3), readNtuple.Jet_Eta->at(3),
                                        readNtuple.Jet_Phi->at(3), readNtuple.Jet_Mass->at(3));
  }

  //-------------------------------------------------------------------------------------// 

  //------------------------ Build up to 3 leading large-R jets ------------------------//
  // Build the two leading large-R jets
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Large-R 1+2 Jets"
              << std::endl;
  }
  if (readNtuple.nLargeRJet >= 1) {
    PhysicsObjects.largeRjet1vec.SetPtEtaPhiM(
                                              readNtuple.LargeRJet_Pt->at(0), readNtuple.LargeRJet_Eta->at(0),
                                              readNtuple.LargeRJet_Phi->at(0), readNtuple.LargeRJet_Mass->at(0));
  }
  // Now the sub-leading large-R jets
  if (readNtuple.nLargeRJet >= 2) {
    PhysicsObjects.largeRjet2vec.SetPtEtaPhiM(
                                              readNtuple.LargeRJet_Pt->at(1), readNtuple.LargeRJet_Eta->at(1),
                                              readNtuple.LargeRJet_Phi->at(1), readNtuple.LargeRJet_Mass->at(1));
  }

  // Build the 3rd large-R jet                            
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Large-R Jets 3"
              << std::endl;
  }
  if (readNtuple.nLargeRJet == 3) {
    PhysicsObjects.largeRjet3vec.SetPtEtaPhiM(
                                              readNtuple.LargeRJet_Pt->at(2), readNtuple.LargeRJet_Eta->at(2),
                                              readNtuple.LargeRJet_Phi->at(2), readNtuple.LargeRJet_Mass->at(2));
  }
  else {
    PhysicsObjects.largeRjet3vec.SetPtEtaPhiM(0., 0., 0., 0.);
  }
  //-------------------------------------------------------------------------------------//

  //----------------------- Build up to 3 leading reclustered jets ----------------------//
  // Build the two leading Reclustered jets                           
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Reclustered 1+2 Jets"
              << std::endl;
  }
  if (readNtuple.nReclusteredJet >= 1) {
    PhysicsObjects.Reclusteredjet1vec.SetPtEtaPhiM(
                                           readNtuple.ReclusteredJet_Pt->at(0), readNtuple.ReclusteredJet_Eta->at(0),
                                           readNtuple.ReclusteredJet_Phi->at(0), readNtuple.ReclusteredJet_Mass->at(0));
  }
  // Now the sub-leading Reclustered jet
  if (readNtuple.nReclusteredJet >= 2) {
    PhysicsObjects.Reclusteredjet2vec.SetPtEtaPhiM(
                                           readNtuple.ReclusteredJet_Pt->at(1), readNtuple.ReclusteredJet_Eta->at(1),
                                           readNtuple.ReclusteredJet_Phi->at(1), readNtuple.ReclusteredJet_Mass->at(1));
  }

  // Build the 3rd Reclustered jet
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Reclustered Jets 3"
              << std::endl;
  }
  if (readNtuple.nReclusteredJet == 3) {
    PhysicsObjects.Reclusteredjet3vec.SetPtEtaPhiM(
                                           readNtuple.ReclusteredJet_Pt->at(2), readNtuple.ReclusteredJet_Eta->at(2),
                                           readNtuple.ReclusteredJet_Phi->at(2), readNtuple.ReclusteredJet_Mass->at(2));
  }
  else {
    PhysicsObjects.Reclusteredjet3vec.SetPtEtaPhiM(0., 0., 0., 0.);
  }
  //-------------------------------------------------------------------------------------// 

  //Missing Transverse Energy construction
  PhysicsObjects.MET_Vec.SetPtEtaPhiM(readNtuple.MET, 0.0, readNtuple.MET_Phi,
                                      0.0);

  // Z boson construction
  PhysicsObjects.ZVec = PhysicsObjects.LeptonVec1 + PhysicsObjects.LeptonVec2;

  //Define the number of jets
  PhysicsObjects.nJets = readNtuple.nSignalJet + readNtuple.nForwardJet;
  PhysicsObjects.numJets = PhysicsObjects.nJets;
  PhysicsObjects.nLargeRJets = readNtuple.nLargeRJet;
  PhysicsObjects.nReclusteredJets = readNtuple.nReclusteredJet;

  //Assign to vector for ease of manipulation with b-tagging tool
  PhysicsObjects.signalJets = {PhysicsObjects.jet1vec, PhysicsObjects.jet2vec,
                               PhysicsObjects.jet3vec};

  //Jet flavour and b-tagging identification
  int jet1flav = readNtuple.Jet_HadronConeExclTruthLabelID->at(0);
  int jet2flav = readNtuple.Jet_HadronConeExclTruthLabelID->at(1);
  int jet3flav = (readNtuple.nSignalJet == 3)
    ? readNtuple.Jet_HadronConeExclTruthLabelID->at(2)
    : -999;
  PhysicsObjects.btagWeight = 1;
  PhysicsObjects.signalJetFlavour = {jet1flav, jet2flav, jet3flav};

  // FN: considering all the  jet in the event for the b-tag, not only the 3 leading 
  std::vector<int> allJetflav;
  for (unsigned int i ; i < readNtuple.Jet_HadronConeExclTruthLabelID->size(); i++ ){
    allJetflav.push_back( readNtuple.Jet_HadronConeExclTruthLabelID->at(i) );
  }
  PhysicsObjects.allJetFlavour = allJetflav;

  //Assign to vector for ease of manipulation with b-tagging tool
   if (Debug)
     std::cout << "<generateHistogram_0lep::BuildEvent()>::   b-tagged Jets"
	       << std::endl;
   // FN we do not need anymore to know what are the b-tagged jets: just get the btagweight                                                                               
   // btag weight will be small for actual b's                                                                                                                            
   // and hight for LQ.                                                                                                                                                   
   // this takes into account the presence of b's which are not tagged                                                                                                    
   // and the absence of (little) LQ mistagged for b's                                                                                                                    

   // PhysicsObjects.taggedJets = BtagTool.compute_btagging(
   //     PhysicsObjects.signalJets, PhysicsObjects.signalJetFlavour,
   //     PhysicsObjects
   //         .btagWeight);  //Run the b-tagging. This retuns a vector of the vector entries of the 2 b-tagged jets
   // // std::cout << "generateHistogram_0lep: " << PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(0)) << " " << PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(1)) << std::endl;                                                                                                                                                     
   // std::cout << "btagWeight before " << PhysicsObjects.btagWeight << std::endl;
   BtagTool.compute_btagweight(PhysicsObjects.allJets, PhysicsObjects.allJetFlavour,PhysicsObjects.btagWeight);
   // std::cout << "btagWeight after " << PhysicsObjects.btagWeight << std::endl; 

  //Run the b-tagging. This retuns a vector of the vector entries of the 2 b-tagged jets
  PhysicsObjects.taggedJets = BtagTool.compute_btagging(
      PhysicsObjects.signalJets, PhysicsObjects.signalJetFlavour,
      PhysicsObjects.btagWeight);

  //PhysicsObjects.signalJetFlavour = {PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(0)), PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(1))};

  // DMM: For now, the selected signal jets are just the 4 jets with the highest pT                                  
  PhysicsObjects.jet1vecSel = PhysicsObjects.jet1vec;
  PhysicsObjects.jet2vecSel = PhysicsObjects.jet2vec;
  PhysicsObjects.jet3vecSel = PhysicsObjects.jet3vec;
  PhysicsObjects.jet4vecSel = PhysicsObjects.jet4vec;

  PhysicsObjects.sumjetpt = 0;

  // FN: Dark Higgs                                                                                                                                                                
  // constructed for the three different categories                                                                                                                                
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Dark Higgs Candidate"
              << std::endl;
  PhysicsObjects.DarkHiggs_merged = PhysicsObjects.Reclusteredjet1vec;

  PhysicsObjects.DarkHiggs_intermediate = PhysicsObjects.Reclusteredjet1vec; // FN for the moment

  PhysicsObjects.DarkHiggs_resolved = PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel +  PhysicsObjects.jet3vecSel  +  PhysicsObjects.jet4vecSel;

  // FN each region has a different mass reconstruction
  PhysicsObjects.mS_candidate = 0;
  if (Region == 2)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_resolved.M();
  else if (Region == 1)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_intermediate.M();
  else if (Region == 0)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_merged.M();

  // Return ok for BuildEvent()
  return true;

}  //BuildEvent()

// //Function for building input variables to BDT. Move to generateHistogram::
// void generateHistogram_2lep::BuildBDTVars(ReadTruthTree &readNtuple,
//                                           EventObjects &PhysicsObjects,
//                                           bool Debug) {
//   //Build BDT input Variables
//   //Higgs candidate mass
//   //PhysicsObjects.mBB = PhysicsObjects.Higgs.M();

//   //Di-lepton invariant mass
//   PhysicsObjects.mLL = PhysicsObjects.ZVec.M();

//   //Tranverse pT of Vector boson
//   PhysicsObjects.pTV = PhysicsObjects.ZVec.Pt();

//   //Missing Transverse Energy
//   PhysicsObjects.MET = PhysicsObjects.MET_Vec.Pt();

//   //distance in eta from V and JJ system
//   PhysicsObjects.dEtaVJJ =
//     fabs(PhysicsObjects.ZVec.Eta() - (PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel).Eta());

//   //dPhi between lepton and JJ system
//   PhysicsObjects.dPhiVJJ =
//       fabs(PhysicsObjects.WTransVec.DeltaPhi(PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel));

//   //dR between leading jets
//   PhysicsObjects.dRJJ =
//       PhysicsObjects.jet1vecSel.DeltaR(PhysicsObjects.jet2vecSel);

//   //Leading signal jet pT
//   PhysicsObjects.pTJ1 = PhysicsObjects.jet1vecSel.Pt();

//   // Sub-Leading signal jet pT
//   PhysicsObjects.pTJ2 = PhysicsObjects.jet2vecSel.Pt();

//   //3-jet invariant mass
//   PhysicsObjects.mJJJ = ((PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel) + PhysicsObjects.jet3vecSel).M();

//   //3rd Jet pT
//   PhysicsObjects.pTJ3 = PhysicsObjects.jet3vecSel.Pt();

//   // Event Number
//   PhysicsObjects.Event_Number = readNtuple.Event_Number;

//   // Define event truth flavour
//   PhysicsObjects.EvtFlav = PhysicsObjects.AssignEventFlavour();
// }

void generateHistogram_2lep::EventSelection(ReadTruthTree &readNtuple,
                                            EventObjects &PhysicsObjects,
                                            unsigned long &eventFlag,
                                            bool Debug, int Region) {
  // Cut 1:   Number of input events
  if (Debug) {
    std::cout << "<EventSelection>::   Check number of input events"
              << std::endl;
  };

  updateFlag(eventFlag, TwoLeptonCutFlow::Input_events, 1);

  // Cut 2:   Check that leptons pass the pT requiremented
  if (Debug) {
    std::cout << "<EventSelection>::  Check leptons pass pT requirement"
              << std::endl;
  };
  bool passed_cut = false;
  if( (std::abs(readNtuple.Lepton_ID->at(0))==11 && std::abs(readNtuple.Lepton_ID->at(1))==11) && (readNtuple.Lepton_Pt->at(0) > 27) )
    passed_cut = true;
  if ( (std::abs(readNtuple.Lepton_ID->at(0))==13 && std::abs(readNtuple.Lepton_ID->at(1))==13) && (readNtuple.Lepton_Pt->at(0) > 25) && (readNtuple.Lepton_ID->at(0) == -1*readNtuple.Lepton_ID->at(1)) )
    passed_cut = true;
  updateFlag(eventFlag, TwoLeptonCutFlow::Two_signal_lepton, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 3: Tau veto
  if (Debug) {
    std::cout << "<EventSelection>::   Tau veto" << std::endl;
  };
  passed_cut = false;
  if (readNtuple.NSignalTaus == 0) passed_cut = true;
  updateFlag(eventFlag, TwoLeptonCutFlow::TauVeto, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 4: pt_ll > 200 GeV cut
  if (Debug) {
    std::cout << "<EventSelection>::   pt_ll > 200 GeV" << std::endl;
  };
  passed_cut = false;
  if(PhysicsObjects.ZVec.Pt() > 200) passed_cut = true;
  updateFlag(eventFlag, TwoLeptonCutFlow::ptllGt200, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  //Cut 5:   Z mass window cut
  if (Debug) {
    std::cout << "<EventSelection>:: Z mass cut" << std::endl;
  };
  passed_cut = false;
  if( PhysicsObjects.ZVec.M() > 83 && PhysicsObjects.ZVec.M() < 99) passed_cut = true;
  updateFlag(eventFlag, TwoLeptonCutFlow::Z_mass_selection, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>:: passed_cut " << passed_cut << std::endl;
  };

  // Cut 6:   MinDPhiMETJetsResolved
  if (Debug) {
    std::cout << "<EventSelection>::   MinDPhiMETJetsResolved" << std::endl;
  }
  passed_cut = false;
  double mindPhi1 = 1000, mindPhi2 = 1000,
    mindPhi3 = 1000;  //What is this!!!!! I have lost all hope in humanity

  if (readNtuple.nSignalJet >= 1)
    mindPhi1 = fabs(readNtuple.Jet_Phi->at(0) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet >= 2)
    mindPhi2 = fabs(readNtuple.Jet_Phi->at(1) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet == 3)
    mindPhi3 = fabs(readNtuple.Jet_Phi->at(2) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet == 2 && readNtuple.nForwardJet >= 1)
    mindPhi3 = fabs(readNtuple.ForwardJet_Phi->at(0) - readNtuple.MET_Phi);
  double mindPhi = mindPhi1;
  if (mindPhi2 < mindPhi) mindPhi = mindPhi2;
  if (mindPhi3 < mindPhi) mindPhi = mindPhi3;
  if (mindPhi >= 0.34906585039)
    passed_cut = true;
  PhysicsObjects.mindPhi = mindPhi;
  updateFlag(eventFlag, TwoLeptonCutFlow::MinDPhiMETJets, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 7: MET_Sig
  if (Debug) {
    std::cout << "<EventSelection>::   MET Significance" << std::endl;
  };
  passed_cut = false;
  if (readNtuple.MET_Sig < 15.) passed_cut = true;
  updateFlag(eventFlag, TwoLeptonCutFlow::MET_Sig, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 8: m_S window
  if (Debug) {
    std::cout << "<EventSelection>::   m_S Window" << std::endl;
  };
  passed_cut = false;
  if (PhysicsObjects.mS_candidate > 100. && PhysicsObjects.mS_candidate < 400.)
    passed_cut = true;
  updateFlag(eventFlag, TwoLeptonCutFlow::mS_window, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Region Selection
  bool passed_merged = false;
  bool passed_intermediate = false;
  bool passed_resolved = false;

  // Calculate the number of Reclustered jets passing the (pseudo) TARJet08ScalarCandidateReader and TAR+comb scalar candidate requirements
  int nTARJet08ScalarCandidateReader = 0;
  int nTARCombScalarBoson = 0;
  for(int iReclJet=0; iReclJet < readNtuple.ReclusteredJet_Pt->size(); iReclJet++){
    double ReclJet_Pt = readNtuple.ReclusteredJet_Pt->at(iReclJet);
    double ReclJet_Mass = readNtuple.ReclusteredJet_Mass->at(iReclJet);

    // Check if it passes the TAR+comb scalar candidate requirement                                                                                                                                                                   
    if( (ReclJet_Mass > 100.) && (ReclJet_Mass < 400.) ) {
      nTARCombScalarBoson++;

      // Additional cut on pt for TARJet08ScalarCandidateReader requirement                                                                                                                                                           
      if(ReclJet_Pt > 300.) nTARJet08ScalarCandidateReader++;
    }
  }

  // Calculate the invariant mass of the leading 4 signal jets (i.e. the resolved dark higgs mass)
  float m_4j = -1.;
  if(readNtuple.nSignalJet > 3){
    m_4j = PhysicsObjects.DarkHiggs_resolved.M();
  }

  // Apply the recycling strategy to classify the event into the merged, intermediate, or resolved region
  if ( (nTARJet08ScalarCandidateReader > 0) && (PhysicsObjects.ZVec.Pt() > 300.) ) {
    passed_merged = true;
  }

  if ( (passed_merged == false) && (nTARCombScalarBoson == 1) ) {
    passed_intermediate = true;
  }

  if ( (passed_merged == false) && (passed_intermediate == false) && (m_4j > 100.) && (m_4j < 400.) ) {
    passed_resolved = true;
  }

  if (Region == 0) {
    if (Debug) {
      std::cout << "<EventSelection>::   Merged Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_merged << std::endl;
    }
    updateFlag(eventFlag, TwoLeptonCutFlow::AnalysisRegion, passed_merged);
  }
  else if (Region == 1) {
    if (Debug) {
      std::cout << "<EventSelection>::   Intermediate Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_intermediate << std::endl;
    }
    updateFlag(eventFlag, TwoLeptonCutFlow::AnalysisRegion, passed_intermediate);
  }
  else if (Region == 2) {
    if (Debug) {
      std::cout << "<EventSelection>::   Resolved Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_resolved << std::endl;
    }
    updateFlag(eventFlag, TwoLeptonCutFlow::AnalysisRegion, passed_resolved);
  }

}  //EventSelection()

void generateHistogram_2lep::Selection2Lepton() {
  int Region = m_Region;    // Get the analysis region

  // Reading in the number of events, and if set to run on a subset
  int nEvents = (m_nEvents == -1) ? m_inputChain->GetEntries() : m_nEvents;

  // Loop through events
  int inspectionInterval = (nEvents / 10 > 0) ? nEvents / 10 : 1;

  ofstream f_evWeights;
  TString Region_str;
  if(Region == 0) Region_str = "Merged";
  else if(Region == 1) Region_str = "Intermediate";
  else if (Region == 2) Region_str = "Resolved";

  TString filename = "/home/danikam1/data/MonoSWWTruthFramework_output/evWeights_2Lep_" + Region_str + "_";
  filename += m_DSID;
  filename += ".txt";
  f_evWeights.open(filename);

  for (int i = 0; i < nEvents; ++i) {
    //Reset all m_histFill descriptions
    m_PhysicsObjects = {};
    m_histFill->Reset();
    // Load the event
    readNtuple->GetEntry(i);

    //Tell user % of completion
    if (i % inspectionInterval == 0)
      std::cout << " Running on event " << i << " of " << nEvents << std::endl;

    //Build the event objects
    if (!BuildEvent((*readNtuple), m_PhysicsObjects, (*m_BtagTool), false, Region)) continue;

    // Run 1-lepton event selection
    unsigned long eventFlag = 0;
    EventSelection((*readNtuple), m_PhysicsObjects, eventFlag, false, Region);

    //Set/Assign region info to histogram filling service
    // DMM: Commenting out number of b-tags in region info 
    //m_histFill->SetNtags(2);  //Currently, b-tagging only allows for 2 b-tags

    // FN does not matter as the SetEventFlav function is hacked, see comment below.                                                                                                
    m_histFill->SetEventFlav(
        0,0);
    // m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(0)),                                                                                                  
    // m_PhysicsObjects.signalJetFlavour.at(                                                                                                                                     
    //     m_PhysicsObjects.taggedJets.at(1)));                                                                                                                                  

    // FN: reverting back and keeping it here in case we change plan (separate fit for V+jets flavour composition)
    // // FN: in the VHbb, there are two b-tags which define the event flavour
    // // in our case we use the number of LQ, c, b to define the EF.
    // // b's have priority on c's which have priority on LQ's.
    // // e.g. if we have b,b,c,LQ -> EF is bb etc.    

    //Fill cut flow
    FillCutFlow2lep(eventFlag, readNtuple->Event_Weight,
                    m_PhysicsObjects.btagWeight);

    //Final veto on event selection
    if (!passAllCutsUpTo(eventFlag, TwoLeptonCutFlow::AnalysisRegion))
      continue;

    f_evWeights << readNtuple->Event_Weight*m_PhysicsObjects.btagWeight << std::endl;
    
    // Loading BDT input variables and possible plotting variables
    // TLorentzVector DiJet = jet1vecSel + jet2vecSel;

    bool twoJet = (m_PhysicsObjects.nJets == 2);

    const float pTV = m_PhysicsObjects.ZVec.Pt();
    const float mJJ = (m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel).M();

    float dRJJ =
        m_PhysicsObjects.jet1vecSel.DeltaR(m_PhysicsObjects.jet2vecSel);
    float dPhiVJJ =
        fabs(m_PhysicsObjects.ZVec.DeltaPhi(m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel));
    float dEtaVJJ =
      fabs(m_PhysicsObjects.ZVec.Eta() - (m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel).Eta());
    float pTJ1 = m_PhysicsObjects.jet1vecSel.Pt();
    float pTJ2 = m_PhysicsObjects.jet2vecSel.Pt();
    float mLL = m_PhysicsObjects.ZVec.M();
    float MET = m_PhysicsObjects.MET_Vec.Pt();
    float mS_candidate = m_PhysicsObjects.mS_candidate;
    float mJJJ =
        (twoJet) ? 0
      : (m_PhysicsObjects.jet1vecSel + m_PhysicsObjects.jet2vecSel + m_PhysicsObjects.jet3vecSel).M();
    float pTJ3 = (twoJet) ? 0 : m_PhysicsObjects.jet3vecSel.Pt();

    std::vector<float> ones_vec(readNtuple->Event_Weights->size(), 1.0);
    
    /*
    bool passCutBasedSelection = false;
    if (readNtuple->MET /
            sqrt(m_PhysicsObjects.sumjetpt + m_PhysicsObjects.LeptonVec1.Pt() +
                 m_PhysicsObjects.LeptonVec2.Pt()) <
        3.5) {
      if (m_PhysicsObjects.ZVec.Pt() < 200 && dRBB < 1.8)
        passCutBasedSelection = true;
      if (m_PhysicsObjects.ZVec.Pt() > 200 && dRBB < 1.2)
        passCutBasedSelection = true;
    }
    */
    /*
    bool passZHFCR = false;
    if (readNtuple->MET /
            sqrt(m_PhysicsObjects.sumjetpt + m_PhysicsObjects.LeptonVec1.Pt() +
                 m_PhysicsObjects.LeptonVec2.Pt()) <
        3.5) {
      if (m_PhysicsObjects.Higgs.M() < 140 &&
          m_PhysicsObjects.Higgs.M() > 110) {
        passZHFCR = true;
      }
    }
    */

    // m_tree->EventWeight =
    //     readNtuple->Event_Weights->at(0) *
    //     m_PhysicsObjects
    //         .btagWeight;  //Want to get the fully corrected event weight, have to use CorrectionFactor from histSvc for the time being...
    // m_tree->nJ = m_PhysicsObjects.nJets;
    // m_tree->EventNumberMod2 = readNtuple->Event_Number % 2;
    // m_tree->FlavB1 =
    //     m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(0));
    // m_tree->FlavB2 =
    //     m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(1));

    // m_tree->mJJ = mJJ * 1e3;  // Needed in MeV for the xml files
    // m_tree->dRJJ = dRJJ;
    // m_tree->dPhiVJJ = dPhiVJJ;
    // m_tree->dEtaVJJ = dEtaVJJ;
    // m_tree->pTV = pTV * 1e3;
    // m_tree->pTJ1 = pTJ1 * 1e3;
    // m_tree->pTJ2 = pTJ2 * 1e3;
    // m_tree->mLL = mLL * 1e3;
    // m_tree->MET = MET * 1e3;

    // if (!twoJet) {
    //   m_tree->mJJJ = mJJJ * 1e3;
    //   m_tree->pTJ3 = pTJ3 * 1e3;
    // }

    //Set the region description
    m_histFill->SetDescription("CR2L");

    m_histFill->SetNjets(readNtuple->nSignalJet + readNtuple->nForwardJet);
   
    m_histFill->SetpTV(pTV);
    // float BDT = 0;

    //Compute the BDT value
    /*
    if (readNtuple->Event_Number % 2 == 0) {
      if (twoJet) {
        if (m_PhysicsObjects.ZVec.Pt() < 150)
          BDT =
              m_tree->reader_2jet_lpt_even->EvaluateMVA("BDT_2L_2jet_lpt_even");
        else
          BDT =
              m_tree->reader_2jet_hpt_even->EvaluateMVA("BDT_2L_2jet_hpt_even");
      } else {
        if (m_PhysicsObjects.ZVec.Pt() < 150)
          BDT =
              m_tree->reader_3jet_lpt_even->EvaluateMVA("BDT_2L_3jet_lpt_even");
        else
          BDT =
              m_tree->reader_3jet_hpt_even->EvaluateMVA("BDT_2L_3jet_hpt_even");
      }
    } else {
      if (twoJet) {
        if (m_PhysicsObjects.ZVec.Pt() < 150)
          BDT = m_tree->reader_2jet_lpt_odd->EvaluateMVA("BDT_2L_2jet_lpt_odd");
        else
          BDT = m_tree->reader_2jet_hpt_odd->EvaluateMVA("BDT_2L_2jet_hpt_odd");
      } else {
        if (m_PhysicsObjects.ZVec.Pt() < 150)
          BDT = m_tree->reader_3jet_lpt_odd->EvaluateMVA("BDT_2L_3jet_lpt_odd");
        else
          BDT = m_tree->reader_3jet_hpt_odd->EvaluateMVA("BDT_2L_3jet_hpt_odd");
      }
    }
    */
    
    TLorentzVector jet1vecSel_MeV, jet2vecSel_MeV;
    jet1vecSel_MeV.SetPtEtaPhiM(m_PhysicsObjects.jet1vecSel.Pt() * 1e3,
                                m_PhysicsObjects.jet1vecSel.Eta(),
                                m_PhysicsObjects.jet1vecSel.Phi(),
                                m_PhysicsObjects.jet1vecSel.M() * 1e3);
    jet2vecSel_MeV.SetPtEtaPhiM(m_PhysicsObjects.jet2vecSel.Pt() * 1e3,
                                m_PhysicsObjects.jet2vecSel.Eta(),
                                m_PhysicsObjects.jet2vecSel.Phi(),
                                m_PhysicsObjects.jet2vecSel.M() * 1e3);


    if (m_Debug) std::cout << "Filling histograms" << std::endl;

    // m_histFill->BookFillHist(
    //     "BDT",                      //Name of output variable
    //     1000,                       //Nbins
    //     -1,                         //xmin
    //     1,                          //xmax
    //     BDT,                        //variable to plot
    //     readNtuple->Event_Weights,  //Fill with vector of alternative weights
    //     m_PhysicsObjects
    //         .btagWeight);  //Any correction to the overall event weight

    //m_histFill->BookFillHist("eventWeight", 5000, -6, 6, readNtuple->Event_Weight, &ones_vec, 1.0);
    
    m_histFill->BookFillHist("mS_candidate", 80, 0, 400, mS_candidate, readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("MET", 400, 0, 2000, MET,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    //m_histFill->BookFillHist("mJJ", 100, 0, 500, mJJ, readNtuple->Event_Weights,
    //                         m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("pTV", 400, 0, 2000, pTV,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillTree(readNtuple->Event_Weights,m_PhysicsObjects.btagWeight,mS_candidate,MET, pTV);
    /*
    m_histFill->BookFillHist("dRJJ", 120, 0, 6, dRJJ, readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);

    m_histFill->BookFillHist("pTJ1", 200, 0, 1000, pTJ1,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("pTJ2", 200, 0, 1000, pTJ2,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("mLL", 1000, 0, 1000, mLL,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dPhiVJJ", 315, 0, 3.15, dPhiVJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dEtaVJJ", 120, 0, 6, dEtaVJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    if (!twoJet) {
      m_histFill->BookFillHist("pTJ3", 200, 0, 1000, pTJ3,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("mJJJ", 200, 0, 1000, mJJJ,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
    }

    m_histFill->BookFillHist("pTVMJJ", 50, 0, 500, pTV, 50, 0, 500, mJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    //m_histFill->BookFillHist("pTVBDT", 50, 0, 500, pTV, 20, -1, 1, BDT, readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    //m_histFill->BookFillHist("mBBBDT", 50, 0, 500, mBB, 20, -1, 1, BDT, readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    
    
    if (passCutBasedSelection) {
      m_histFill->BookFillHist("mBBCutBased", 100, 0, 500, mBB,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("pTVCutBased", 400, 0, 2000, pTV,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("dRBBCutBased", 120, 0, 6, dRBB,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
    }

    if (passZHFCR) {
      m_histFill->BookFillHist("mBBZHFCR", 100, 0, 500, mBB,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("pTVZHFCR", 400, 0, 2000, pTV,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("dRBZHFCR", 120, 0, 6, dRBB,
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
    }

    if (readNtuple->Truth_Boson_Pt->size() > 0) {
      m_histFill->BookFillHist("Number_of_Truth_Bosons", 11, -0.5, 10.5,
                               readNtuple->Truth_Boson_Pt->size(),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Pt", 400, 0, 2000,
                               readNtuple->Truth_Boson_Pt->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Eta", 120, -6, 6,
                               readNtuple->Truth_Boson_Eta->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Phi", 315, 0, 3.15,
                               readNtuple->Truth_Boson_Phi->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Mass", 100, 0, 500,
                               readNtuple->Truth_Boson_Mass->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
    }
    */
    // if (m_WriteMVATree) m_tree->FillTree();
  }
  f_evWeights.close();
}

void generateHistogram_2lep::FillCutFlow2lep(const unsigned long int flag,
                                             float eventWeight,
                                             float btagWeight) {
  // Cuts to exclude from cutflow
  std::vector<unsigned long int> excludeCuts = {};

  // Loop over cuts
  for (unsigned long int i = TwoLeptonCutFlow::Input_events;
       i <= TwoLeptonCutFlow::AnalysisRegion; ++i) {
    if (std::find(excludeCuts.begin(), excludeCuts.end(), i) !=
        excludeCuts.end())
      continue;

    if (!(passAllCutsUpTo(flag, i, {}))) continue;

    float fullweight = (i >= TwoLeptonCutFlow::Input_events)
                           ? eventWeight * btagWeight
                           : eventWeight;

    m_histFill->FillCutflow(i, 1, "Unweighted");
    m_histFill->FillCutflow(i, eventWeight, "EventWeight");
    m_histFill->FillCutflow(i, fullweight, "FullWeight");
  }
}
