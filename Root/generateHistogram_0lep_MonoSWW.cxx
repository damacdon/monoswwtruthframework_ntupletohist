#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_0lep_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/generateHistogram_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/ReadTruthTree_MonoSWW.h"
#include "TruthFramework_NtupleToHist/Selection.h"
#include "MonoSWWTruthFramework_NtupleToHist/btagTool_MonoSWW.h"
#include "MonoSWWTruthFramework_NtupleToHist/histSvc_MonoSWW.h"

#include <TClass.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <fstream>
#include <iostream>
#include <vector>

generateHistogram_0lep::generateHistogram_0lep() {}

generateHistogram_0lep::~generateHistogram_0lep() {}

void generateHistogram_0lep::FillHistograms() {
  Selection0Lepton();  //Needed to steer the code to run the 0-lepton selection
}

//Construct the necessary TLorentzVectors for the 1-lepton event
bool generateHistogram_0lep::BuildEvent(ReadTruthTree &readNtuple,
                                        EventObjects &PhysicsObjects,
                                        btagTool &BtagTool, bool Debug, int Region) {

  //FN: building the alljets for the b-tag veto
  for (unsigned int i = 0 ; i < readNtuple.Jet_Phi->size(); i++ ){
    if (Debug) 
      std::cout << "<generateHistogram_0lep::BuildEvent()>::    "<< i+1 << " Jets";
    TLorentzVector tmp;
    tmp.SetPtEtaPhiM(
        readNtuple.Jet_Pt->at(i), readNtuple.Jet_Eta->at(i),
        readNtuple.Jet_Phi->at(i), readNtuple.Jet_Mass->at(i));
    PhysicsObjects.allJets.push_back(tmp);
  }

  //------------------------ Build up to 4 leading resolved jets ------------------------//
  //Build the two leading resolved jets
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved 1+2 Jets"
              << std::endl;
  if (readNtuple.nSignalJet >= 1) {
    PhysicsObjects.jet1vec.SetPtEtaPhiM(
        readNtuple.Jet_Pt->at(0), readNtuple.Jet_Eta->at(0),
        readNtuple.Jet_Phi->at(0), readNtuple.Jet_Mass->at(0));
  }
  //Now the sub-leading resolved jet
  if (readNtuple.nSignalJet >= 2) {
    PhysicsObjects.jet2vec.SetPtEtaPhiM(
        readNtuple.Jet_Pt->at(1), readNtuple.Jet_Eta->at(1),
        readNtuple.Jet_Phi->at(1), readNtuple.Jet_Mass->at(1));
  }

  // Build the 3rd resolved jet
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved Jets 3"
              << std::endl;
  if (readNtuple.nSignalJet >= 3) {
    PhysicsObjects.jet3vec.SetPtEtaPhiM(
        readNtuple.Jet_Pt->at(2), readNtuple.Jet_Eta->at(2),
        readNtuple.Jet_Phi->at(2), readNtuple.Jet_Mass->at(2));
  } 
  // Build the 4th resolved jet
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Resolved Jets 4"
              << std::endl;
  if (readNtuple.nSignalJet == 4) {
    PhysicsObjects.jet4vec.SetPtEtaPhiM(
  readNtuple.Jet_Pt->at(3), readNtuple.Jet_Eta->at(3),
        readNtuple.Jet_Phi->at(3), readNtuple.Jet_Mass->at(3));
  }
  
  //-------------------------------------------------------------------------------------//

  //------------------------ Build up to 3 leading large-R jets ------------------------//  
  // Build the two leading large-R jets 
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Large-R 1+2 Jets"
              << std::endl;
  }
  if (readNtuple.nLargeRJet >= 1) {
        PhysicsObjects.largeRjet1vec.SetPtEtaPhiM(
            readNtuple.LargeRJet_Pt->at(0), readNtuple.LargeRJet_Eta->at(0),
            readNtuple.LargeRJet_Phi->at(0), readNtuple.LargeRJet_Mass->at(0));
  }
  // Now the sub-leading large-R jet                                                                                                                                                
  if (readNtuple.nLargeRJet >= 2) {
    PhysicsObjects.largeRjet2vec.SetPtEtaPhiM(
            readNtuple.LargeRJet_Pt->at(1), readNtuple.LargeRJet_Eta->at(1),
            readNtuple.LargeRJet_Phi->at(1), readNtuple.LargeRJet_Mass->at(1));
   }

  // Build the 3rd large-R jet
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Large-R Jets 3"
              << std::endl;
  }
  if (readNtuple.nLargeRJet == 3) {
    PhysicsObjects.largeRjet3vec.SetPtEtaPhiM(
          readNtuple.LargeRJet_Pt->at(2), readNtuple.LargeRJet_Eta->at(2),
          readNtuple.LargeRJet_Phi->at(2), readNtuple.LargeRJet_Mass->at(2));
  }   
  else {
    PhysicsObjects.largeRjet3vec.SetPtEtaPhiM(0., 0., 0., 0.);
  }
  //-------------------------------------------------------------------------------------// 

  //----------------------- Build up to 3 leading Reclustered jets ----------------------//                                                                                           
  // Build the two leading Reclustered jets
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Reclustered 1+2 Jets"
              << std::endl;
  }
  if (readNtuple.nReclusteredJet >= 1) {
    PhysicsObjects.Reclusteredjet1vec.SetPtEtaPhiM(
                readNtuple.ReclusteredJet_Pt->at(0), readNtuple.ReclusteredJet_Eta->at(0),
                readNtuple.ReclusteredJet_Phi->at(0), readNtuple.ReclusteredJet_Mass->at(0));
  }
  // Now the sub-leading Reclustered jet                                                                                                                                             
  if (readNtuple.nReclusteredJet >= 2) {
    PhysicsObjects.Reclusteredjet2vec.SetPtEtaPhiM(
                readNtuple.ReclusteredJet_Pt->at(1), readNtuple.ReclusteredJet_Eta->at(1),
                readNtuple.ReclusteredJet_Phi->at(1), readNtuple.ReclusteredJet_Mass->at(1));
  }

  // Build the 3rd Reclustered jet
  if (Debug){
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Reclustered Jets 3"
              << std::endl;
  }
  if (readNtuple.nReclusteredJet == 3) {
    PhysicsObjects.Reclusteredjet3vec.SetPtEtaPhiM(
                readNtuple.ReclusteredJet_Pt->at(2), readNtuple.ReclusteredJet_Eta->at(2),
                readNtuple.ReclusteredJet_Phi->at(2), readNtuple.ReclusteredJet_Mass->at(2));
  }
  else {
    PhysicsObjects.Reclusteredjet3vec.SetPtEtaPhiM(0., 0., 0., 0.);
  }
  //-------------------------------------------------------------------------------------//

  //Build the Missing Transverse Energy vector
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   MET" << std::endl;
  PhysicsObjects.MET_Vec.SetPtEtaPhiM(readNtuple.MET, 0.0, readNtuple.MET_Phi,
                                      0.0);
  //Define the number of jets
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   nJets"
              << std::endl;
  PhysicsObjects.nJets = readNtuple.nSignalJet + readNtuple.nForwardJet;
  PhysicsObjects.numJets = PhysicsObjects.nJets;
  PhysicsObjects.nLargeRJets = readNtuple.nLargeRJet;
  PhysicsObjects.nReclusteredJets = readNtuple.nReclusteredJet;

  //Determine event flavour composition
  if (Debug)
    std::cout
        << "<generateHistogram_0lep::BuildEvent()>::   Truth Flavour + b-tag"
        << std::endl;
  int jet1flav = readNtuple.Jet_HadronConeExclTruthLabelID->at(0);
  int jet2flav = readNtuple.Jet_HadronConeExclTruthLabelID->at(1);
  int jet3flav = (readNtuple.nSignalJet == 3)
                     ? readNtuple.Jet_HadronConeExclTruthLabelID->at(2)
                     : -999;
  // FN: considering all the  jet in the event for the b-tag, not only the 3 leading ones
  // 
  std::vector<int> allJetflav;
  for (unsigned int i ; i < readNtuple.Jet_HadronConeExclTruthLabelID->size(); i++ ){
    allJetflav.push_back( readNtuple.Jet_HadronConeExclTruthLabelID->at(i) );
  }
  PhysicsObjects.allJetFlavour = allJetflav;


  //if(readNtuple.nSignalJet == 3){
  //  twoJet = false;
  //}
  //else if( readNtuple.nForwardJet == 1 ){
  //  twoJet = false;
  //}
  PhysicsObjects.btagWeight = 1;
  PhysicsObjects.signalJets = {PhysicsObjects.jet1vec, PhysicsObjects.jet2vec,
                               PhysicsObjects.jet3vec};
  PhysicsObjects.signalJetFlavour = {jet1flav, jet2flav, jet3flav}; // FN to be deleted..

  //Assign to vector for ease of manipulation with b-tagging tool
  // DMM: Problem: by design, this function always returns 2 b-tagged jets...why is it reasonable to assume that there will always be 2 b-jets in the event??? Or is it just the two jets most likely to be b-jets??
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   b-tagged Jets"
              << std::endl;
              // FN we do not need anymore to know what are the b-tagged jets: just get the btagweight
              // btag weight will be small for actual b's 
              // and hight for LQ.
              // this takes into account the presence of b's which are not tagged
              // and the absence of (little) LQ mistagged for b's

  // PhysicsObjects.taggedJets = BtagTool.compute_btagging(
  //     PhysicsObjects.signalJets, PhysicsObjects.signalJetFlavour,
  //     PhysicsObjects
  //         .btagWeight);  //Run the b-tagging. This retuns a vector of the vector entries of the 2 b-tagged jets
  // // std::cout << "generateHistogram_0lep: " << PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(0)) << " " << PhysicsObjects.signalJetFlavour.at(PhysicsObjects.taggedJets.at(1)) << std::endl;
  // std::cout << "btagWeight before " << PhysicsObjects.btagWeight << std::endl;
  BtagTool.compute_btagweight(PhysicsObjects.allJets, PhysicsObjects.allJetFlavour,PhysicsObjects.btagWeight);
  // std::cout << "btagWeight after " << PhysicsObjects.btagWeight << std::endl;



  //Jet selection procedure for 0-lepton analysis
  // DMM: Need to remove the b-tag requirement on the resolved jets
  if (Debug)
    std::cout
        << "<generateHistogram_0lep::BuildEvent()>::   Jet Selection (0-lepton)"
        << std::endl;

  // DMM: For now, the selected signal jets are just the 4 jets with the highest pT
  PhysicsObjects.jet1vecSel = PhysicsObjects.jet1vec;
  PhysicsObjects.jet2vecSel = PhysicsObjects.jet2vec;
  PhysicsObjects.jet3vecSel = PhysicsObjects.jet3vec;
  PhysicsObjects.jet4vecSel = PhysicsObjects.jet4vec;
  
  // W Boson Candidate
  // Constructed from all possible combinations of leading 2-4 signal jets.
  // Select the combo with mass nearest W mass
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   W boson Candidate"
              << std::endl;

  // Go through all available combinations of 2-4 leading resolved jets
  // DMM: Should be able to remove this once the above definition of signalJets includes PhysicsObjects.jet4vec
  PhysicsObjects.signalJets = {PhysicsObjects.jet1vec, PhysicsObjects.jet2vec,
           PhysicsObjects.jet3vec, PhysicsObjects.jet4vec};
  

  int max_signalJets = std::min(readNtuple.nSignalJet, 4);  // Max jets for reco combination is 4
  int min_signalJets = 2;
  double min_massDiff = 999999;                             // Set the minimum mass difference to some unreasonably high value
  double tmp_massDiff = 0;

  if(Debug && max_signalJets < readNtuple.nSignalJet) {
    std::cout << "Only " << readNtuple.nSignalJet << " jets in event, capping maxJets used for reco canidates to that value."; }

  // The for loop below largely reproduces the behaviour of the function MonoSAnalysisHelper::FillRecoCandidates
  // in the file XAMPPmonoS/Root/MonoSAnalysisHelper.cxx in the monoS XAMPP package 
  for (int constituents = min_signalJets; constituents <= max_signalJets; ++constituents) {
    std::vector<bool> v(max_signalJets);
    std::fill(v.begin(), v.begin() + constituents, true);
    // loop over all possible permutations of the 2-4 leading signal jets
    do {
      TLorentzVector WCand;
      for (int i = 0; i < max_signalJets; ++i) {
  if (v[i]) {
    //cout << i;
    // Add up the jets contributing to the candidate W boson
    WCand += PhysicsObjects.signalJets.at(i);
  }
      }
    
      //cout << " W Candidate Mass: " << WCand.M();

      // Select the W boson candidate with mass closest to the W mass (80.4 GeV)
      tmp_massDiff = abs(WCand.M()-80.4);
      if(tmp_massDiff < min_massDiff) {
        PhysicsObjects.WBoson = WCand;
        min_massDiff = tmp_massDiff;
      }
      //cout << endl;
    } while (std::prev_permutation(v.begin(), v.end()));
  }
  //cout << "W mass: " << PhysicsObjects.WBoson.M() << endl;
  //cout << endl;

  //Set the event weight. So far it is constructed using:   Event_Weight = readNtuple.Event_Weight * PhysicsObjects.btagWeight;

    // FN: Dark Higgs
    // constructed for the three different categories
  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Dark Higgs Candidate"
              << std::endl;
  PhysicsObjects.DarkHiggs_merged = PhysicsObjects.Reclusteredjet1vec;

  PhysicsObjects.DarkHiggs_intermediate = PhysicsObjects.Reclusteredjet1vec; // FN for the moment

  PhysicsObjects.DarkHiggs_resolved = PhysicsObjects.jet1vecSel + PhysicsObjects.jet2vecSel +  PhysicsObjects.jet3vecSel  +  PhysicsObjects.jet4vecSel;

  // FN each region has a different mass reconstruction 
  PhysicsObjects.mS_candidate = 0;
  if (Region == 2)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_resolved.M();
  else if (Region == 1)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_intermediate.M();
  else if (Region == 0)
    PhysicsObjects.mS_candidate = PhysicsObjects.DarkHiggs_merged.M();

  if (Debug)
    std::cout << "<generateHistogram_0lep::BuildEvent()>::   Event Weight"
              << std::endl;
  PhysicsObjects.Event_Weight =
    readNtuple.Event_Weight * PhysicsObjects.btagWeight;   

  //Return ok for the BuildEvent()
  return true;
}


//Bit Flag Event Selection
void generateHistogram_0lep::EventSelection(ReadTruthTree &readNtuple,
                                            EventObjects &PhysicsObjects,
                                            unsigned long &eventFlag,
                                            bool Debug, int Region) {

  //std::cout << "<EventSelection>::   ###################" << std::endl;
  //Cut 1:   Number of input events
  if (Debug) {
    std::cout << "<EventSelection>::   Check number of input events"
              << std::endl;
  };
  updateFlag(eventFlag, ZeroLeptonCutFlow::Input_events, 1);

  //Cut 2:   MET > 200 GeV
  if (Debug) {
    std::cout << "<EventSelection>::   MET > 200 GeV" << std::endl;
  };

  bool passed_cut = true;
  if (readNtuple.MET < 200) passed_cut = false;
  updateFlag(eventFlag, ZeroLeptonCutFlow::METgreaterthan200GeV, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 3: Tau veto
  if (Debug) {
    std::cout << "<EventSelection>::   Tau veto" << std::endl;
  };
  passed_cut = false;
  if (readNtuple.NSignalTaus == 0) passed_cut = true;
  updateFlag(eventFlag, ZeroLeptonCutFlow::TauVeto, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  //Cut 4:   MinDPhiMETJetsResolved
  if (Debug) {
    std::cout << "<EventSelection>::   MinDPhiMETJetsResolved" << std::endl;
  };                                                                                                                                                                                 
  passed_cut = false;
  double mindPhi1 = 1000, mindPhi2 = 1000,                                                                                                                                          
         mindPhi3 = 1000;  //What is this!!!!! I have lost all hope in humanity...                                                                                                          
  if (readNtuple.nSignalJet >= 1)
    mindPhi1 = fabs(readNtuple.Jet_Phi->at(0) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet >= 2)
    mindPhi2 = fabs(readNtuple.Jet_Phi->at(1) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet == 3)
    mindPhi3 = fabs(readNtuple.Jet_Phi->at(2) - readNtuple.MET_Phi);
  if (readNtuple.nSignalJet == 2 && readNtuple.nForwardJet >= 1)
    mindPhi3 = fabs(readNtuple.ForwardJet_Phi->at(0) - readNtuple.MET_Phi);
  double mindPhi = mindPhi1;
  if (mindPhi2 < mindPhi) mindPhi = mindPhi2;                                                                                                                                       
  if (mindPhi3 < mindPhi) mindPhi = mindPhi3;
  if (mindPhi >= 0.34906585039)
    passed_cut = true;
  PhysicsObjects.mindPhi = mindPhi;
  updateFlag(eventFlag, ZeroLeptonCutFlow::MinDPhiMETJets, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl; 
  };  

  // Cut 5: MET_Sig
  if (Debug) {
    std::cout << "<EventSelection>::   MET Significance" << std::endl;
  };
  passed_cut = false;
  if (readNtuple.MET_Sig > 15.) passed_cut = true;
  updateFlag(eventFlag, ZeroLeptonCutFlow::MET_Sig, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Cut 6: m_S window
  if (Debug) {
    std::cout << "<EventSelection>::   m_S Window" << std::endl;
  };
  passed_cut = false;
  if (PhysicsObjects.mS_candidate > 100. && PhysicsObjects.mS_candidate < 400.)
    passed_cut = true;
  updateFlag(eventFlag, ZeroLeptonCutFlow::mS_window, passed_cut);
  if (Debug) {
    std::cout << "<EventSelection>::   passed_cut " << passed_cut << std::endl;
  };

  // Region Selection
  bool passed_merged = false;
  bool passed_intermediate = false;
  bool passed_resolved = false;

  // Calculate the number of Reclustered jets passing the (pseudo) TARJet08ScalarCandidateReader and TAR+comb scalar candidate requirements
  int nTARJet08ScalarCandidateReader = 0;
  int nTARCombScalarBoson = 0;
  for(int iReclJet=0; iReclJet < readNtuple.ReclusteredJet_Pt->size(); iReclJet++){
    double ReclJet_Pt = readNtuple.ReclusteredJet_Pt->at(iReclJet);
    double ReclJet_Mass = readNtuple.ReclusteredJet_Mass->at(iReclJet);

      // Check if it passes the TAR+comb scalar candidate requirement
      if( (ReclJet_Mass > 100.) && (ReclJet_Mass < 400.) ) {
	nTARCombScalarBoson++;

	// Additional cut on pt for TARJet08ScalarCandidateReader requirement
	if(ReclJet_Pt > 300.) nTARJet08ScalarCandidateReader++;
      }
  }

  // Calculate the invariant mass of the leading 4 signal jets (i.e. the resolved dark higgs mass)
  float m_4j = -1.;
  if(readNtuple.nSignalJet > 3){
    m_4j = PhysicsObjects.DarkHiggs_resolved.M();
  }

  // Apply the recycling strategy to classify the event into the merged, intermediate, or resolved region				  
  if ( (nTARJet08ScalarCandidateReader > 0) && (readNtuple.MET > 300.) ) {
    passed_merged = true;
  }

  if ( (passed_merged == false) && (nTARCombScalarBoson == 1) ) {
    passed_intermediate = true;
  }

  if ( (passed_merged == false) && (passed_intermediate == false) && (m_4j > 100.) && (m_4j < 400.) ) {
    passed_resolved = true;
  }
  
  if (Region == 0) {
    if (Debug) {
      std::cout << "<EventSelection>::   Merged Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_merged << std::endl;
    }
    updateFlag(eventFlag, ZeroLeptonCutFlow::AnalysisRegion, passed_merged);
  }
  else if (Region == 1) {
    if (Debug) {
      std::cout << "<EventSelection>::   Intermediate Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_intermediate << std::endl;
    }
    updateFlag(eventFlag, ZeroLeptonCutFlow::AnalysisRegion, passed_intermediate);
  }
  else if (Region == 2) {
    if (Debug) {
      std::cout << "<EventSelection>::   Resolved Region" << std::endl;
      std::cout << "<EventSelection>::   passed_cut " << passed_resolved << std::endl;
    } 
    updateFlag(eventFlag, ZeroLeptonCutFlow::AnalysisRegion, passed_resolved);
  }  
}

void generateHistogram_0lep::Selection0Lepton() {
  int Region = m_Region;    // DMM: Get the analysis region
  int nEvents =
      (m_nEvents == -1)
          ? m_inputChain->GetEntries()
          : m_nEvents;  // Reading in the number of events, and if set to run on a subset

  int inspectionLevel = (nEvents / 10 > 0) ? nEvents / 10 : 1;
    
  std::cout << "<Selection0Lepton()>::   Running event processing"
            << std::endl;
  for (int i = 0; i < nEvents; ++i) {
    //Reset all m_histFill descriptions and physics objects
    m_PhysicsObjects = {};
    m_histFill->Reset();
    // Load the event
    readNtuple->GetEntry(i);
    //Tell user % of completion
    if (i % inspectionLevel == 0)
      std::cout << " Running on event " << i << " of " << nEvents << std::endl;
    
    //Build the event objects
    if (!BuildEvent((*readNtuple), m_PhysicsObjects, (*m_BtagTool), false, Region)) continue;

    //Run 0-lepton event selection
    unsigned long eventFlag = 0;
    EventSelection((*readNtuple), m_PhysicsObjects, eventFlag, false, Region);

    //Set/Assign region info to histogram filling service
    // DMM: Commenting out number of b-tags in region info

    //m_histFill->SetNtags(2);  //Currently, b-tagging only allows for 2 b-tags
    // FN does not matter as the SetEventFlav function is hacked, see comment below.
    m_histFill->SetEventFlav(
        0,0);
        // m_PhysicsObjects.signalJetFlavour.at(m_PhysicsObjects.taggedJets.at(0)),
        // m_PhysicsObjects.signalJetFlavour.at(
        //     m_PhysicsObjects.taggedJets.at(1)));

    // FN: reverting back and keeping it here in case we change plan (separate fit for V+jets flavour composition)
    // // FN: in the VHbb, there are two b-tags which define the event flavour
    // // in our case we use the number of LQ, c, b to define the EF.
    // // b's have priority on c's which have priority on LQ's.
    // // e.g. if we have b,b,c,LQ -> EF is bb etc.

    // keeping these for histogramming 
    int nTags_dev = 0;
    for (int i = 0; i < m_PhysicsObjects.signalJetFlavour.size(); i++){
      if (m_PhysicsObjects.signalJetFlavour.at(i) == 5)
        nTags_dev ++;
    }
    int nCTags_dev = 0;
    for (int i = 0; i < m_PhysicsObjects.signalJetFlavour.size(); i++){
      if (m_PhysicsObjects.signalJetFlavour.at(i) == 4)
        nCTags_dev ++;
    }

    // if (nTags_dev == 0 and nCTags_dev == 0)
    //   m_histFill->SetEventFlav(
    //           0,0); // ll
    // else if (nTags_dev == 0 and nCTags_dev == 1)
    //   m_histFill->SetEventFlav(
    //           19,0); // cl
    // else if (nTags_dev == 0 and nCTags_dev == 2)
    //   m_histFill->SetEventFlav(
    //           8,0); // cc
    // else if (nTags_dev == 1 and nCTags_dev == 0)
    //   m_histFill->SetEventFlav(
    //           5,0); // bl
    // else if (nTags_dev == 1 and nCTags_dev == 1)
    //   m_histFill->SetEventFlav(
    //           9,0); // bc
    // else if (nTags_dev == 1 and nCTags_dev == 2)
    //   m_histFill->SetEventFlav(
    //           9,0); // bc
    // else if (nTags_dev == 2 and nCTags_dev == 0)
    //   m_histFill->SetEventFlav(
    //           10,0); // bb
    // else if (nTags_dev == 2 and nCTags_dev == 1)
    //   m_histFill->SetEventFlav(
    //           10,0); // bb
    // else if (nTags_dev == 2 and nCTags_dev == 2)
    //   m_histFill->SetEventFlav(
    //           10,0); // bb
    // else if (nTags_dev > 2) // three or more b's 
    //   m_histFill->SetEventFlav(
    //           10,0); // bb
    // else 
    //   m_histFill->SetEventFlav(
    //           0,0); // ll

    // std::cout << nTags_dev << std::endl;

    //Fill cut flow
    FillCutFlow0lep(eventFlag, readNtuple->Event_Weight,
                    m_PhysicsObjects.btagWeight);

    //Final veto on event selection
    if (!passAllCutsUpTo(
      eventFlag, ZeroLeptonCutFlow::AnalysisRegion))
      continue;

    //Loading BDT input variables and possible plotting variables
    float mW = m_PhysicsObjects.WBoson.M();
    float dRJJ =
        m_PhysicsObjects.jet1vec.DeltaR(m_PhysicsObjects.jet2vec);
    float dEtaJJ = fabs(m_PhysicsObjects.jet1vec.Eta() -
                        m_PhysicsObjects.jet2vec.Eta());
    float pTJ1 = m_PhysicsObjects.jet1vec.Pt();
    float pTJ2 = m_PhysicsObjects.jet2vec.Pt();
    float pTJJ = (m_PhysicsObjects.jet1vec + m_PhysicsObjects.jet2vec).Pt();
    float MET = m_PhysicsObjects.MET_Vec.Pt();
    float mS_candidate = m_PhysicsObjects.mS_candidate;

    float HT = 0;
    for (unsigned int jet_index = 0; jet_index < readNtuple->Jet_Pt->size();
         jet_index++) {
      HT += readNtuple->Jet_Pt->at(jet_index);
    }
    for (unsigned int jet_index = 0;
         jet_index < readNtuple->ForwardJet_Pt->size(); jet_index++) {
      HT += readNtuple->ForwardJet_Pt->at(jet_index);
    }
    bool twoJet = m_PhysicsObjects.nJets == 2;

    std::vector<float> ones_vec(readNtuple->Event_Weights->size(), 1.0);

    // Set the region description
    m_histFill->SetDescription("SR");

    // DMM: Commenting out number of jets in region info for now (not sure that we really care)
    //if (twoJet)
    //  m_histFill->SetNjets(2);
    //else
    //  m_histFill->SetNjets(3);

    m_histFill->SetpTV(MET);

    TLorentzVector jet1vec_MeV, jet2vec_MeV;
    jet1vec_MeV.SetPtEtaPhiM(m_PhysicsObjects.jet1vec.Pt() * 1e3,
                                m_PhysicsObjects.jet1vec.Eta(),
                                m_PhysicsObjects.jet1vec.Phi(),
                                m_PhysicsObjects.jet1vec.M() * 1e3);
    jet2vec_MeV.SetPtEtaPhiM(m_PhysicsObjects.jet2vec.Pt() * 1e3,
                                m_PhysicsObjects.jet2vec.Eta(),
                                m_PhysicsObjects.jet2vec.Phi(),
                                m_PhysicsObjects.jet2vec.M() * 1e3);

    //Fill the histograms. To add Selection define it at the beginning of the loop and add the argument Selections: ... ,btagWeight, Selections);
    m_histFill->SetCutBase(false);  //Filling MVA selection

    m_histFill->BookFillHist("eventWeight", 5000, -6, 6, readNtuple->Event_Weight, &ones_vec, 1.0);

    m_histFill->BookFillHist("mS_candidate", 80, 0, 400, mS_candidate, readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("MET", 400, 0, 2000, MET,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);

    m_histFill->BookFillTree(readNtuple->Event_Weights,m_PhysicsObjects.btagWeight,mS_candidate,MET, MET);


    /*
    m_histFill->BookFillHist("pTJ1", 200, 0, 1000, pTJ1,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("pTJ2", 200, 0, 1000, pTJ2,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
        m_histFill->BookFillHist("pTJJ", 200, 0, 1500, pTJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dRJJ", 120, 0, 6, dRJJ, readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dEtaJJ", 120, 0, 6, dEtaJJ,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);

    m_histFill->BookFillHist("nTags_dev", 6, 0, 6, nTags_dev,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
                             // 1); // set to one to see # of tags in absolute numbers


    m_histFill->BookFillHist(
        "dPhiJJ", 315, 0, 3.15,
        fabs(m_PhysicsObjects.jet1vec.DeltaPhi(m_PhysicsObjects.jet2vec)),
        readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist(
        "SumPtJet", 600, 0, 3000, m_PhysicsObjects.sumjetpt,
        readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("dPhiMETMPT", 315, 0, 3.15,
                             fabs(readNtuple->MET_Phi - readNtuple->MPT_Phi),
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist(
        "dPhiMETW", 18, 0, 180,
        fabs(m_PhysicsObjects.MET_Vec.DeltaPhi(m_PhysicsObjects.WBoson)) * 180 /
            TMath::Pi(),
        readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist(
        "MindPhiMETJet", 315, 0, 3.15, m_PhysicsObjects.mindPhi,
        readNtuple->Event_Weights, m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("MET_Track", 100, 0, 1000, readNtuple->MPT,
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);
    m_histFill->BookFillHist("max_pTV_HT", 100, 0, 1000, std::max(MET, HT),
                             readNtuple->Event_Weights,
                             m_PhysicsObjects.btagWeight);

    if (readNtuple->Truth_Boson_Pt->size() > 0) {
      m_histFill->BookFillHist("Number_of_Truth_Bosons", 11, -0.5, 10.5,
                               readNtuple->Truth_Boson_Pt->size(),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Pt", 400, 0, 2000,
                               readNtuple->Truth_Boson_Pt->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Eta", 120, -6, 6,
                               readNtuple->Truth_Boson_Eta->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Phi", 315, 0, 3.15,
                               readNtuple->Truth_Boson_Phi->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      m_histFill->BookFillHist("Leading_Truth_Boson_Mass", 100, 0, 500,
                               readNtuple->Truth_Boson_Mass->at(0),
                               readNtuple->Event_Weights,
                               m_PhysicsObjects.btagWeight);
      
    }
    */
  }
}

void generateHistogram_0lep::FillCutFlow0lep(const unsigned long int flag,
                                             float eventWeight,
                                             float btagWeight) {
  // Cuts to exclude from cutflow
  std::vector<unsigned long int> excludeCuts = {};

  // Loop over cuts
  for (unsigned long int i = ZeroLeptonCutFlow::Input_events;
       //       i <= ZeroLeptonCutFlow::DPhiMETDijetResolved_greaterthan120; ++i) {
       i <= ZeroLeptonCutFlow::AnalysisRegion; ++i) { 
    if (std::find(excludeCuts.begin(), excludeCuts.end(), i) !=
        excludeCuts.end())
      continue;

    if (!(passAllCutsUpTo(flag, i, {}))) continue;

    // DMM: Where should fullweight start for monoS???
    // float fullweight = (i >= ZeroLeptonCutFlow::LeadJetPt_greaterthan45GeV)
    float fullweight = (i >= ZeroLeptonCutFlow::Input_events) 
                           ? eventWeight * btagWeight
                           : eventWeight;

    m_histFill->FillCutflow(i, 1, "Unweighted");
    m_histFill->FillCutflow(i, eventWeight, "EventWeight");
    m_histFill->FillCutflow(i, fullweight, "FullWeight");
  }
}

